# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 13:53:43 2016

@author: Viet
"""

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

"""
Blocked quadrat variance - data variance at various block sizes
Two-term local quadrat variance (TTLQV): b = block size
    V2(b) = \sum_{i=1}^{n+1-2b} (\sum_{j=i}^{i+b-1} x[j] - \sum_{j=i+b}^{i+2b-1} x[j]) ** 2 / (2 * b * (n+1-2b))
Variance peaks = scales of spatial pattern
"""
def local_quadrat_variance(b):
    pass

def get_ring(delta_r=25., N_r=40):
    delta_r *= 2
    C_r = delta_r ** 2
    ring = [0, delta_r]
    for i in range(1, N_r):
        ring += [C_r / ring[-1] + ring[-1]]
    return np.array(ring)
    
def assuncao_rate(e, b):
    # e = event variables e.g. the number of cancer patients
    # b = population-at-risk e.g. region population
    y = e * 1.0 / b
    e_sum, b_sum = np.sum(e), np.sum(b)
    ebi_b = e_sum * 1.0 / b_sum
    s2 = sum(b * ((y - ebi_b) ** 2)) / b_sum
    ebi_a = s2 - ebi_b / (float(b_sum) / len(e))
    ebi_v = ebi_a + ebi_b / b
    return (y - ebi_b) / np.sqrt(ebi_v) 

"""
Moran's I - degree of correlation between variable values at increasing spatial lags
    -1          negative correlation
    +1          positive correlation
    -1/(n-1)    no correlation
I(d) = ( \sum_{i,j} w[i,j](d) * (x[i] - xmean) * (x[j] - xmean) / \sum_{i,j} w[i,j] ) 
        / ( \sum_{i} (x[i] - xmean) ** 2 / n )
where w[i,j] = 1 if x[i] and x[j] are in same distance class d, 0 otherwise.
"""
def moransi(X, W):
    xmean = np.mean(X)
    X0 = X - xmean
    n = len(X)
    #
    den = np.sum(W * np.outer(X0, X0)) / W.sum()
    nom = np.sum(X0 ** 2) / n
    return den / nom

"""
Geary's C - difference among variable values at nearby locations
    0   positive autocorrelation
    ~2  strong negative autocorrlation
    1   no autocorrelation
C(d) = ( \sum_{i,j} w[i,j](d) * (x[i] - x[j]) ** 2 / (2 * \sum{i, j} w[i,j]) ) 
        / ( \sum_{i} (x[i] - xmean) ** 2 / (n-1) )
where w[i,j] = 1 if x[i] and x[j] are in same distance class d, 0 otherwise.
"""
def gearysc(X, W):
    xmean = np.mean(X)
    n = len(X)
    Xr = np.tile(X, (n, 1))
    #
    den = np.sum(W * ((Xr.T - Xr) ** 2)) / (2 * W.sum())
    nom = np.sum((X - xmean) ** 2) / (n-1)
    return den / nom


def spatial_correlogram(dring, I, pop, adjusted=True):
    D = pop.get_dmatrix()
    nr = len(dring) - 1
    cov = pop.covs[0]  #* pop.covs[1].sum(1)
    if adjusted:
        X = assuncao_rate(I, cov)
    else:
        X = I * 1. / cov
    #
    mis = np.zeros(nr)
    for i in range(nr):
        '@todo: check commeted out option'
        #W = np.array((D >= dring[i]) * (D < dring[i+1]), dtype=int)
        W = np.array((D <= dring[i+1]), dtype=int)          
        mis[i] = moransi(X, W)
    return mis
    
    
