import numpy as np


def discrete_trace(T, S, tau):
    """
        (T, S) = continuous-time multivariate-state trace
            S.shape = [time, n_compartments]
    """
    tt = discrete_T(T, tau)
    ss = discrete_S(T, S, tau, tt)
    return tt, ss


def discrete_T(T, tau):
    nT = len(T) - 1
    assert nT > 0
    return np.append(np.arange(T[0], T[-1], tau), T[-1])


def discrete_S(T, S, tau, tt):
    ss = np.zeros((len(tt), S.shape[1]))
    ss[0, :] = S[0, :]
    ii_old = 0
    for i in xrange(1, len(T) - 1):
        ii = int((T[i] - T[0]) / tau)
        ss[ii_old:ii, :] = np.tile(ss[ii_old, :], (ii-ii_old, 1))
        ss[ii, :] = S[i, :]
        ii_old = ii
    ss[ii_old:, :] = ss[ii_old, :]
    return ss


def aggregate_over_space(S, s0, rupdate):
    """
    Input:
    S[i, :] = (spatial location, type index) of event i
    S.shape = (len(T) - 2, 2)
    Output:
    S[i, :] = (compartment values) after event i
    S.shape = (len(T), n_compartments)
    """
    Sout = np.zeros((len(S) + 2, len(rupdate)))
    Sout[0, :] = s0.sum(0)
    for i in xrange(S.shape[0]):
        Sout[i+1, :] = rupdate[S[i, 1]]
    Sout = Sout.cumsum(0)
    return Sout


def get_discrete_expand_S(T, S, s0, rupdate, tt):
    tau = tt[1] - tt[0]
    S_expand = np.zeros((len(tt), s0.shape[0], s0.shape[1]), dtype=int)
    # print S_expand.shape
    #
    S_expand[0, :, :] = s0
    ii_old = 0
    for i in xrange(S.shape[0]):
        # print i, ii_old,
        ii = int((T[i + 1] - T[0]) / tau) + 1
        # print ii
        S_expand[ii_old:ii+1, :, :] = np.tile(
            S_expand[ii_old, :, :], (ii-ii_old+1, 1, 1))
        S_expand[ii, S[i, 0], :] += rupdate[S[i, 1]]
        ii_old = ii
    S_expand[ii_old:, :, :] = S_expand[ii_old, :, :]
    return S_expand


def aggregate_over_time(S, s0, rupdate):
    Sout = s0
    for i in xrange(S.shape[0]):
        Sout[S[i, 0], :] += rupdate[S[i, 1]]
    return Sout


def compute_ess(samps):
    """
    Calculate effective sample size as in Gelman et al. 2014
    Require multiple chains
    """
    n_samp, n_chain = samps.shape
    # calculate between-chain variance
    B = n_samp * np.var(np.mean(samps, axis=-2), axis=-1, ddof=1)
    # calculate within-chain variance
    W = np.mean(np.var(samps, axis=-2, ddof=1), axis=-1)
    # estimate of maginal posterior variance
    Vhat = W * (n_samp - 1) / n_samp + B / n_samp

    # ESS start
    negative_autocorr = False
    t = 1
    rho = np.ones(n_samp)
    # find point of first negative autocorrelation
    while not negative_autocorr and (t < n_samp):
        variogram = np.mean((samps[t:, :] - samps[:-t, :]) ** 2)
        rho[t] = 1. - variogram / (2. * Vhat)
        negative_autocorr = sum(rho[t - 1:t + 1]) < 0
        t += 1

    if t % 2:
        t -= 1

    ess = min(n_chain * n_samp, int(
        n_chain * n_samp / (1 + 2 * rho[1:t - 1].sum())))

    return ess
