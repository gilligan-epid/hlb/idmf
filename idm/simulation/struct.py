import util
from idm.plotting.plotmaker import make_traceplot, make_fillplot
from idm.plotting.mapmaker import make_heatmap

import os
import numpy as np
import numpy.random as nr
import h5py
import cPickle as pickle
import matplotlib.pyplot as plt
# from pymc.diagnostics import effective_n


class SimOptions(object):
    def __init__(self, **kwargs):
        self.n_iter = kwargs.pop('n_iter', 100)  # no. iters / block
        self.n_block = kwargs.pop('n_block', 5)  # no. blocks
        self.n_samp = self.n_iter * self.n_block
        self.with_single = kwargs.pop('with_single', False)
        self.tau = kwargs.pop('tau', None)
        self.n_comp = kwargs.pop('n_comp', None)
        self.prim_points = kwargs.pop('primary_intros', ([], []))
        self.n_dcomp = kwargs.pop('n_dcomp', 0)
        assert self.n_comp is not None
        self.cout = np.arange(self.n_comp)
        self.numeric = kwargs.pop('numeric', False)
        self.snap_times = kwargs.pop('snap_times', [])  # extra
        self.draw_data = kwargs.pop('draw_data', False)
        self.chunk_no = kwargs.pop('chunk_no', 0)
        self.det_by_data = kwargs.pop('det_by_data', False)
        self.n_skipped_samp = kwargs.pop('n_skipped_samp', 0)
        #
        self.outdir = kwargs.pop('outdir', None)
        assert self.tau is not None


class PopSim(object):
    """
    Simulations of a population model
    """
    def __init__(self, model, opts):
        self.model = model
        self.opts = opts
        # print [(k, self.model.params[k].value) for k in self.model.params.keys()]
        # assert False

    def run(self, iV=None, prefix=''):
        n_dcomp, n_comp = self.opts.n_dcomp, self.opts.n_comp
        draw_data = self.opts.draw_data and self.model.data is not None
        tt = util.discrete_T(self.model.pop.tvec, self.opts.tau)
        n_year = int((tt[-1] - tt[0]) / 365) + 1
        fagg = os.path.join(
            self.opts.outdir, '{}sim_aggregate.h5'.format(prefix))
        if self.opts.chunk_no == 0:
            with h5py.File(fagg, "w") as f:
                f.create_group('by_time')
                f.create_dataset('by_time/T', data=tt)
                f.create_dataset('by_time/S', dtype=np.int32, shape=(
                    self.opts.n_samp, len(tt), self.opts.n_comp), maxshape=(
                        None, len(tt), self.opts.n_comp))
                f.create_dataset('by_time/Sw', dtype=np.float, shape=(
                    self.opts.n_samp, len(tt), self.opts.n_comp), maxshape=(
                        None, len(tt), self.opts.n_comp))
                f.create_dataset('econtrib', dtype=np.float, shape=(
                    self.opts.n_samp, n_year, 3), maxshape=(None, n_year, 3))
                for ci in self.opts.cout:
                    f.create_dataset('by_space_{}'.format(ci), shape=(
                        self.opts.n_samp, self.model.pop.n_pop), maxshape=(
                            None, self.model.pop.n_pop), dtype=np.int32)
                    f.create_dataset('mean_full_{}'.format(ci), dtype=float,
                                     shape=(len(tt), self.model.pop.n_pop))
                # snap times
                for t in self.opts.snap_times:
                    for ci in self.opts.cout:
                        f.create_dataset(
                            'by_space_{}_t{}'.format(ci, t),
                            shape=(self.opts.n_samp, self.model.pop.n_pop),
                            maxshape=(None, self.model.pop.n_pop),
                            dtype=np.int32)
                if draw_data:
                    f.create_dataset('by_time/D', dtype=np.int32, shape=(
                        self.opts.n_samp, len(tt)), maxshape=(None, len(tt)))
                    f.create_dataset('by_time/Dw', dtype=np.float, shape=(
                        self.opts.n_samp, len(tt)), maxshape=(None, len(tt)))
                    f.create_dataset('by_space_dcount', dtype=np.int32, shape=(
                        self.opts.n_samp, self.model.pop.n_pop), maxshape=(
                            None, self.model.pop.n_pop))
                    # snap times
                    for t in self.opts.snap_times:
                        f.create_dataset(
                            'by_space_dcount_t{}'.format(t), dtype=np.int32,
                            shape=(self.opts.n_samp, self.model.pop.n_pop),
                            maxshape=(None, self.model.pop.n_pop))
                else:
                    f.create_dataset('by_time/D', data=h5py.Empty('f'))
                    f.create_dataset('by_time/Dw', data=h5py.Empty('f'))
                    f.create_dataset('by_space_dcount', data=h5py.Empty('f'))
            #
            fsrun = os.path.join(
                self.opts.outdir, '{}sim_single_runs.h5'.format(prefix))
            if self.opts.with_single:
                with h5py.File(fsrun, "w") as f:
                    f.create_group('discrete')
                    f.create_dataset('discrete/T', data=tt)
                    f.create_dataset('discrete/S_low', dtype=np.int32, shape=(
                        len(tt), self.model.pop.n_pop, self.opts.n_comp))
                    f.create_dataset('discrete/S_med', dtype=np.int32, shape=(
                        len(tt), self.model.pop.n_pop, self.opts.n_comp))
                    f.create_dataset('discrete/S_high', dtype=np.int32, shape=(
                        len(tt), self.model.pop.n_pop, self.opts.n_comp))
                    if draw_data:
                        f.create_dataset('discrete/D_low', dtype=np.int32,
                                         shape=(len(tt), self.model.pop.n_pop))
                        f.create_dataset('discrete/D_med', dtype=np.int32,
                                         shape=(len(tt), self.model.pop.n_pop))
                        f.create_dataset('discrete/D_high', dtype=np.int32,
                                         shape=(len(tt), self.model.pop.n_pop))
                    f.create_group('continuous')
                    f.create_group('continuous/low')
                    f.create_dataset(
                        'continuous/low/s0', dtype=np.int8, shape=(
                            self.model.pop.n_pop, self.opts.n_comp))
                    f.create_group('continuous/med')
                    f.create_dataset(
                        'continuous/med/s0', dtype=np.int8, shape=(
                            self.model.pop.n_pop, self.opts.n_comp))
                    f.create_group('continuous/high')
                    f.create_dataset(
                        'continuous/high/s0', dtype=np.int8, shape=(
                            self.model.pop.n_pop, self.opts.n_comp))
                sv_idx = np.sort(nr.choice(
                    self.opts.n_samp, size=min(9, self.opts.n_samp),
                    replace=False))
        else:
            # extend data
            with h5py.File(fagg, "a") as f:
                dset = f['by_time/S']
                dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                dset = f['by_time/Sw']
                dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                dset = f['econtrib']
                dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                for ci in self.opts.cout:
                    dset = f['by_space_{}'.format(ci)]
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                # snap times
                for t in self.opts.snap_times:
                    for ci in self.opts.cout:
                        dset = f['by_space_{}_t{}'.format(ci, t)]
                        dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                if draw_data:
                    dset = f['by_time/D']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    dset = f['by_time/Dw']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    dset = f['by_space_dcount']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    # snap times
                    for t in self.opts.snap_times:
                        dset = f['by_space_dcount_t{}'.format(t)]
                        dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
        #
        max_size = self.model.pop.n_maxhost * self.model.pop.n_pop
        fsize, slow, shigh, smed = 0, max_size, 0, 0
        sims_by_time = np.empty((self.opts.n_iter, len(tt), self.opts.n_comp))
        sims_by_time_wtd = np.empty_like(sims_by_time)
        sims_econtrib = np.empty((self.opts.n_iter, n_year, 3))
        sims_by_space = {ci: [
            np.empty((self.opts.n_iter, self.model.pop.n_pop)) for i in xrange(
                len(self.opts.snap_times) + 1)] for ci in self.opts.cout}
        sims_mean = {ci: np.zeros((len(tt), self.model.pop.n_pop))
                     for ci in self.opts.cout}
        if draw_data:
            D_by_time = np.empty((self.opts.n_iter, len(tt)))
            D_by_time_wtd = np.empty_like(D_by_time)
            dcount_by_space = [
                np.empty((self.opts.n_iter, self.model.pop.n_pop))
                for i in xrange(len(self.opts.snap_times) + 1)]
        ri = 0
        for bi in xrange(self.opts.n_block):
            for i in xrange(self.opts.n_iter):
                T, S, s0, econtrib = self.model.sample(*self.opts.prim_points)
                S_expand = util.get_discrete_expand_S(
                    T, S, s0, self.model.rupdate, tt)
                if not self.opts.numeric:
                    S_expand = S_expand.clip(max=1)
                # print (S[:, 1] == 2).sum(), (S[:, 1] == 3).sum()
                # print S_expand.sum(1)[100, :]
                # assert False
                if draw_data:
                    D, dcounts = self.model.sample_data(
                        S_expand[:, :, -1 - n_dcomp], tt, self.opts.snap_times)
                    D_by_time[i, :] = D.sum(1)
                    D_by_time_wtd[i, :] = (
                        D * self.model.pop.hdens[None, :]).sum(1)
                    dcount_by_space[0][i, :] = dcounts[-1]
                    # snap times
                    for k in xrange(len(self.opts.snap_times)):
                        dcount_by_space[k + 1][i, :] = dcounts[k]
                # aggregation
                sims_econtrib[i, :, :] = econtrib
                if n_dcomp > 0:
                    sims_by_time[i, :, :-n_dcomp] = S_expand[
                        :, :, :-n_dcomp].sum(1)
                    sims_by_time[i, :, -n_dcomp:] = S_expand[
                        :, :, -n_dcomp:].sum(1)
                    sims_by_time_wtd[i, :, :-n_dcomp] = (
                        S_expand[:, :, :-n_dcomp] * self.model.pop.hdens[
                            None, :, None]).sum(1)
                    sims_by_time_wtd[i, :, -n_dcomp:] = (
                        S_expand[:, :, -n_dcomp:] * self.model.pop.hdens[
                            None, :, None]).sum(1)
                else:
                    sims_by_time[i, :, :] = S_expand.sum(1)
                    sims_by_time_wtd[i, :, :] = (
                        S_expand * self.model.pop.hdens[None, :, None]).sum(1)
                ssize = sims_by_time[i, -1, -1 - n_dcomp]
                fsize += ssize
                for ci in self.opts.cout:
                    # k = self.opts.cout[oi]
                    if iV is not None and iV > ci:
                        ii = np.append(np.arange(ci, iV),
                                       np.arange(iV + 1, n_comp - n_dcomp))
                    elif ci < n_comp - n_dcomp:
                        ii = np.arange(ci, n_comp - n_dcomp)
                    else:
                        ii = np.arange(ci, n_comp)
                    sims_by_space[ci][0][i, :] = S_expand[-1, :, ii].sum(0)
                    sims_mean[ci] += S_expand[:, :, ii].sum(2)
                    # snap times
                    for k, t in enumerate(self.opts.snap_times):
                        it = int((t - T[0]) / self.opts.tau) + 1
                        sims_by_space[ci][k + 1][i, :] =\
                            S_expand[it, :, ii].sum(0)
                # random single runs
                si = bi * self.opts.n_iter + i
                if self.opts.chunk_no == 0 and self.opts.with_single\
                   and ri < len(sv_idx) and sv_idx[ri] == si:
                    with h5py.File(fsrun, "a") as f:
                        if ssize < slow:
                            f['discrete/S_low'][:] = S_expand
                            if draw_data:
                                f['discrete/D_low'][:] = D
                            f['continuous/low/s0'][:] = s0
                            if 'continuous/low/T' in f:
                                del f['continuous/low/T']
                                del f['continuous/low/S']
                            f.create_dataset('continuous/low/T', data=T)
                            f.create_dataset('continuous/low/S', data=S)
                            slow = ssize
                        if ssize > shigh:
                            f['discrete/S_high'][:] = S_expand
                            if draw_data:
                                f['discrete/D_high'][:] = D
                            f['continuous/high/s0'][:] = s0
                            if 'continuous/high/T' in f:
                                del f['continuous/high/T']
                                del f['continuous/high/S']
                            f.create_dataset('continuous/high/T', data=T)
                            f.create_dataset('continuous/high/S', data=S)
                            shigh = ssize
                        smean = fsize / (si + 1)
                        if np.abs(smed - smean) > np.abs(ssize - smean):
                            f['discrete/S_med'][:] = S_expand
                            if draw_data:
                                f['discrete/D_med'][:] = D
                            f['continuous/med/s0'][:] = s0
                            if 'continuous/med/T' in f:
                                del f['continuous/med/T']
                                del f['continuous/med/S']
                            f.create_dataset('continuous/med/T', data=T)
                            f.create_dataset('continuous/med/S', data=S)
                            smed = ssize
                    ri += 1
            iadd = self.opts.chunk_no * self.opts.n_samp
            ii = slice(bi * self.opts.n_iter + iadd,
                       (bi + 1) * self.opts.n_iter + iadd)
            with h5py.File(fagg, "a") as f:
                f['by_time/S'][ii, :, :] = sims_by_time
                f['by_time/Sw'][ii, :, :] = sims_by_time_wtd
                f['econtrib'][ii, :, :] = sims_econtrib
                for ci in self.opts.cout:
                    f['by_space_{}'.format(ci)][ii, :] = sims_by_space[ci][0]
                    # snap times
                    for i, t in enumerate(self.opts.snap_times):
                        f['by_space_{}_t{}'.format(ci, t)][ii, :] =\
                            sims_by_space[ci][i + 1]
                if self.opts.draw_data and self.model.data is not None:
                    f['by_time/D'][ii, :] = D_by_time
                    f['by_time/Dw'][ii, :] = D_by_time_wtd
                    f['by_space_dcount'][ii, :] = dcount_by_space[0]
                    # snap times
                    for i, t in enumerate(self.opts.snap_times):
                        f['by_space_dcount_t{}'.format(t)][ii, :] =\
                            dcount_by_space[i + 1]
            print "Block {} saved.".format(bi)
        with h5py.File(fagg, "a") as f:
            for ci in self.opts.cout:
                f['mean_full_{}'.format(ci)][:, :] =\
                    sims_mean[ci] * 1. / self.opts.n_samp
        print fsize / self.opts.n_samp, (slow, smed, shigh)

    @staticmethod
    def load_sims(outdir, tag, rid=0, prefix='', with_D=False):
        if tag == 'by_time':
            sims = {}
            fname = os.path.join(outdir, '{}sim_aggregate.h5'.format(prefix))
            with h5py.File(fname) as f:
                sims['T'] = f['by_time/T'][:]
                sims['S'] = f['by_time/S'][:]
                D = f['by_time/D']
                sims['D'] = D[:] if D.shape is not None else None
        elif tag == 'by_time_wtd':
            sims = {}
            fname = os.path.join(outdir, '{}sim_aggregate.h5'.format(prefix))
            with h5py.File(fname) as f:
                sims['T'] = f['by_time/T'][:]
                sims['S'] = f['by_time/Sw'][:]
                D = f['by_time/Dw']
                sims['D'] = D[:] if D.shape is not None else None
        elif tag == 'econtrib':
            fname = os.path.join(outdir, '{}sim_aggregate.h5'.format(prefix))
            with h5py.File(fname) as f:
                sims = f['econtrib'][:]
        elif tag[:8] == 'by_space':
            fname = os.path.join(outdir, '{}sim_aggregate.h5'.format(prefix))
            with h5py.File(fname) as f:
                sims = f[tag][:] if f[tag].shape is not None else None
        elif tag[:9] == 'mean_full':
            fname = os.path.join(outdir, '{}sim_aggregate.h5'.format(prefix))
            with h5py.File(fname) as f:
                sims = f[tag][:]
        # tag: single_discrete_low
        elif tag[:6] == 'single':
            sims = {}
            fname = os.path.join(outdir, '{}sim_single_runs.h5'.format(prefix))
            stype, level = tag[7:].split('_')
            print stype, level
            with h5py.File(fname) as f:
                if stype == 'discrete':
                    sims['T'] = f['discrete/T'][:]
                    sims['S'] = f['discrete/S_{}'.format(level)][:]
                    if with_D:
                        sims['D'] = f['discrete/D_{}'.format(level)][:]
                elif stype == 'continuous':
                    sims['s0'] = f['continuous/{}/s0'.format(level)][:]
                    sims['T'] = f['continuous/{}/T'.format(level)][:]
                    sims['S'] = f['continuous/{}/S'.format(level)][:]
        return sims


class PopLearn(object):
    """
    Sampling from a posterior population model
    """
    def __init__(self, model, opts):
        self.model = model
        self.opts = opts

    def run(self, prefix='', param_list=['alpha', 'beta', 'epsilon', 'dprob'],
            dp_len=1, dtrajs=[], iV=None):
        n_dcomp, n_comp = self.opts.n_dcomp, self.opts.n_comp
        tt = util.discrete_T(self.model.pop.tvec, self.opts.tau)
        #
        psamps = {}
        for pname in param_list:
            if pname == 'dprob':
                plen = dp_len
            elif pname == 'delta':
                plen = 1
            elif 'epsilon' in pname:
                plen = min(2, self.model.params[pname].ncells)
            else:
                plen = self.model.params[pname].ncells
            psamps[pname] = np.empty((self.opts.n_iter, plen))

        psamps['logl'] = np.zeros((self.opts.n_iter,))
        psamps['logp'] = np.zeros((self.opts.n_iter,))
        # prepare file of posterior samples
        fname = os.path.join(self.opts.outdir, '{}posterior.h5'.format(prefix))
        if self.opts.chunk_no == 0:
            with h5py.File(fname, "w") as f:
                print "Create datasets ..."
                f.create_group('params')
                for pname in param_list:
                    if pname == 'dprob':
                        plen = dp_len
                    elif pname == 'delta':
                        plen = 1
                    elif 'epsilon' in pname:
                        plen = min(2, self.model.params[pname].ncells)
                    else:
                        plen = self.model.params[pname].ncells
                    ds_name = 'params/{}'.format(pname)
                    f.create_dataset(ds_name, dtype=np.float64, shape=(
                        self.opts.n_samp, plen), maxshape=(None, plen))
                f.create_dataset('params/logl', dtype=np.float64, shape=(
                    self.opts.n_samp,), maxshape=(None, ))
                f.create_dataset('params/logp', dtype=np.float64, shape=(
                    self.opts.n_samp,), maxshape=(None, ))
                f.create_group('trajs')
                f.create_group('sims')
                f.create_dataset('trajs/init', dtype=np.int32, shape=(
                    self.opts.n_samp, self.model.pop.n_pop, self.opts.n_comp),
                    maxshape=(None, self.model.pop.n_pop, self.opts.n_comp))
                f.create_dataset('trajs/at_0', dtype=np.int32, shape=(
                    self.opts.n_samp, self.model.pop.n_pop, self.opts.n_comp),
                    maxshape=(None, self.model.pop.n_pop, self.opts.n_comp))
                f.create_dataset('trajs/T', data=tt)
                f.create_dataset('trajs/S_by_time', dtype=np.int32, shape=(
                    self.opts.n_samp, len(tt), self.opts.n_comp),
                    maxshape=(None, len(tt), self.opts.n_comp))
                for ci in self.opts.cout:
                    f.create_dataset('trajs/S_by_space_{}'.format(ci), shape=(
                        self.opts.n_samp, self.model.pop.n_pop),
                        dtype=np.int32, maxshape=(None, self.model.pop.n_pop))
                    f.create_dataset(
                        'trajs/S_mean_{}'.format(ci), dtype=float, shape=(
                            self.opts.n_block, len(tt), self.model.pop.n_pop),
                        maxshape=(None, len(tt), self.model.pop.n_pop))
                    f.create_dataset('sims/by_space_{}'.format(ci), shape=(
                        self.opts.n_samp, self.model.pop.n_pop),
                        dtype=np.int32, maxshape=(None, self.model.pop.n_pop))
                    f.create_dataset('sims/mean_full_{}'.format(
                        ci), dtype=float, shape=(
                            len(tt), self.model.pop.n_pop))
        else:
            # extend data
            with h5py.File(fname, "a") as f:
                for pname in psamps.keys():
                    dset = f['params/{}'.format(pname)]
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                for key in ['init', 'at_0', 'S_by_time']:
                    dset = f['trajs/{}'.format(key)]
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                for ci in self.opts.cout:
                    dset = f['trajs/S_mean_{}'.format(ci)]
                    dset.resize(dset.shape[0] + self.opts.n_block, axis=0)
                    dset = f['trajs/S_by_space_{}'.format(ci)]
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    dset = f['sims/by_space_{}'.format(ci)]
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
            # reset parameters to last_state values
            params, ignore = pickle.load(open(os.path.join(
                self.opts.outdir, "{}last_state.pkl".format(prefix)), "rb"))
            for pname in param_list:
                print pname,
                val = self.model.params[pname].value
                print val,
                if np.isscalar(val):
                    self.model.params[pname].value = params[pname]
                else:
                    self.model.params[pname].value = np.reshape(
                        np.array(params[pname]), val.shape)
                print self.model.params[pname].value
        #
        init = np.empty((
            self.opts.n_iter, self.model.pop.n_pop, self.opts.n_comp))
        at_0 = np.empty((
            self.opts.n_iter, self.model.pop.n_pop, self.opts.n_comp))
        trajs_by_t = np.empty((self.opts.n_iter, len(tt), self.opts.n_comp))
        trajs_by_space = {
            ci: np.empty((self.opts.n_iter, self.model.pop.n_pop))
            for ci in self.opts.cout}
        trajs_mean = {ci: np.zeros((len(tt), self.model.pop.n_pop))
                      for ci in self.opts.cout}
        sims_by_space = {ci: np.empty((self.opts.n_iter, self.model.pop.n_pop))
                         for ci in self.opts.cout}
        sims_mean = {ci: np.zeros((len(tt), self.model.pop.n_pop))
                     for ci in self.opts.cout}
        for si, (logl, logp, acc) in enumerate(self.model.sample()):
            # print si, logl, logp, acc
            i = si % self.opts.n_iter
            psamps['logl'][i] = logl
            psamps['logp'][i] = logp
            for pname in param_list:
                if pname == 'dprob':
                    psamps[pname][i, :] = self.model.params[
                        pname].value.flatten()
                elif pname == 'delta' and self.model.params[pname].ncells > 1:
                    psamps[pname][i, :] = self.model.params[pname].value[
                        0, 0] / self.model.params[pname].wc
                elif 'epsilon' in pname:
                    v = self.model.params[pname].value
                    psamps[pname][i, :] = v if np.isscalar(v) else v[:, 0]
                else:
                    psamps[pname][i, :] = self.model.params[pname].value
            S_expand = self.model.pop_traj(tt)
            #
            Sinit = S_expand[0, :, :] * 1.
            for ci in xrange(Sinit.shape[1] - 1 - n_dcomp, -1, -1):
                Sinit[:, ci] -= Sinit[:, ci + 1:n_comp - n_dcomp].sum(1)
            for ci in xrange(n_comp - 1, n_comp - 1 - n_dcomp, -1):
                Sinit[:, ci] -= Sinit[:, ci + 1:n_comp].sum(1)
            init[i, :, :] = Sinit
            #
            it0 = np.argmin(tt <= 0.0)  # time point at or right after 0
            S0 = S_expand[it0, :, :] * 1.
            # change from cumulative to instanteneous
            for ci in xrange(S0.shape[1] - 1 - n_dcomp, -1, -1):
                S0[:, ci] -= S0[:, ci + 1:n_comp - n_dcomp].sum(1)
            for ci in xrange(S0.shape[1] - 1, S0.shape[1] - 1 - n_dcomp, -1):
                S0[:, ci] -= S0[:, ci + 1:n_comp].sum(1)
            at_0[i, :, :] = S0
            #
            trajs_by_t[i, :, :] = S_expand.sum(1)
            for ci in self.opts.cout:
                trajs_by_space[ci][i, :] =\
                    S_expand[-1, :, self.opts.cout[ci]:].sum(1)
                trajs_mean[ci] += S_expand[
                    :, :, self.opts.cout[ci]:].sum(2) * 1.
                # TODO - need to do something!
                if iV is not None and iV > ci:
                    ii = np.append(np.arange(ci, iV),
                                   np.arange(iV + 1, n_comp - n_dcomp))
                elif ci < n_comp - n_dcomp:
                    ii = np.arange(ci, n_comp - n_dcomp)
                else:
                    ii = np.arange(ci, n_comp)
                sims_by_space[ci][i, :] = S_expand[-1, :, ii].sum(0)
                sims_mean[ci] += S_expand[:, :, ii].sum(2)

            if i == self.opts.n_iter - 1:
                si_ext = self.opts.chunk_no * self.opts.n_samp + si
                print "Up to sample {}: ".format(si_ext+1),
                print "logp=({}, {}); logl=({}, {}); ".format(
                    psamps['logp'].max(), psamps['logp'].mean(),
                    psamps['logl'].max(), psamps['logl'].mean()),
                print "acceptance={}".format(acc)
                ii = slice(si_ext - i, si_ext + 1)
                block_id = si_ext // self.opts.n_iter

                with h5py.File(fname, "a") as f:
                    print "Save..."
                    f.attrs["iteration"] = si_ext + 1
                    print f.attrs['iteration']
                    # save new samples
                    for pname in psamps.keys():
                        f['params/{}'.format(pname)][ii] = psamps[pname]
                    f['trajs/init'][ii, :, :] = init
                    f['trajs/at_0'][ii, :, :] = at_0
                    f['trajs/S_by_time'][ii, :, :] = trajs_by_t
                    for ci in self.opts.cout:
                        f['trajs/S_by_space_{}'.format(ci)][ii, :]\
                            = trajs_by_space[ci]
                        f['trajs/S_mean_{}'.format(ci)][block_id, :, :]\
                            = trajs_mean[ci] / self.opts.n_iter
                        f['sims/by_space_{}'.format(ci)][ii, :]\
                            = sims_by_space[ci]
                # Trace plot of the block
                make_traceplot(
                    psamps, self.model, os.path.join(
                        self.opts.outdir, '{}param_trace_block{}.png'.format(
                            prefix, block_id)), dp_len=dp_len,
                    param_list=param_list)
                make_fillplot(tt, np.array(trajs_by_t), os.path.join(
                    self.opts.outdir, '{}latent_traj_block{}.png'.format(
                        prefix, block_id)), data=dtrajs,
                        labels=self.model.compartments)
                make_heatmap(
                    trajs_by_space[0].sum(0), self.model.pop.grid, None, None,
                    os.path.join(
                        self.opts.outdir, '{}latent_heat_block{}.png'.format(
                            prefix, block_id)))
                plt.close("all")
            if si >= self.opts.n_samp - 1:
                break
        # pickle last state: parameters, trajectories
        mfile = os.path.join(
            self.opts.outdir, "{}last_state.pkl".format(prefix))
        params, trajs = {}, {}
        for pname in param_list:
            val = np.squeeze(psamps[pname][-1])
            val = np.asscalar(val) if sum(val.shape) == 0 else val
            params[pname] = val
        for c in self.model.compartments:
            trajs[c] = [self.model.nodes[c][j].value
                        for j in xrange(self.model.pop.n_pop)]
        pickle.dump((params, trajs), open(mfile, "wb"), -1)
        # mean trajectories
        with h5py.File(fname, "a") as f:
            for ci in self.opts.cout:
                f['sims/mean_full_{}'.format(ci)][:, :]\
                    = sims_mean[ci] * 1. / self.opts.n_samp
        with h5py.File(fname) as f:
            n_iter = int(f.attrs["iteration"])
            print n_iter, "end"
        return True

    """def test_ess(self, cprefs, param_list=['beta'], n_samp=None):
        if n_samp is None:
            n_samp = self.opts.n_samp - self.model.opts.n_burnin
        # run, load and merge param chains
        params = [[] for p in param_list]
        for prefix in cprefs:
            fname = os.path.join(
                self.opts.outdir, '{}posterior.h5'.format(prefix))
            with h5py.File(fname) as f:
                ii = slice(self.opts.n_samp - n_samp, self.opts.n_samp)
                for j, pname in enumerate(param_list):
                    params[j] += [f['params/{}'.format(pname)][ii, :]]
        params = np.array(params)
        # compute and return effective sample size
        ess, ess_pymc = [], []
        print params.shape, params[0].shape
        for j, pname in enumerate(param_list):
            ess += [util.compute_ess(params[j, :, :, 0].T)]
            ess_pymc += [effective_n(params[j])]
        return ess, ess_pymc"""

    @staticmethod
    def load_params(outdir, param_list, n_samp=None, prefix=''):
        fname = os.path.join(outdir, '{}posterior.h5'.format(prefix))
        param_dist = {}
        with h5py.File(fname) as f:
            n_iter = int(f.attrs["iteration"])
            i = slice(n_iter/2, n_iter) if n_samp is None\
                else slice(n_iter - n_samp, n_iter)
            for pname in param_list:
                try:
                    samps = f['params/{}'.format(pname)][i, :]
                    if samps.shape[1] == 1:
                        samps = samps[:, 0]
                    param_dist[pname] = samps
                except KeyError:
                    print('No samples found for {}'.format(pname))
                    param_dist[pname] = None
        return param_dist

    @staticmethod
    def load_sims(outdir, tag, prefix=''):
        fname = os.path.join(outdir, '{}posterior.h5'.format(prefix))
        print fname
        if tag == 'by_time':
            sims = {}
            with h5py.File(fname) as f:
                print int(f.attrs["iteration"])
                sims['T'] = f['trajs/T'][:]
                sims['S'] = f['trajs/S_by_time'][:]
        elif tag[:8] == 'by_space':
            with h5py.File(fname) as f:
                sims = f['sims/{}'.format(tag)][:]
        elif tag[:9] == 'mean_full':
            with h5py.File(fname) as f:
                sims = f['sims/{}'.format(tag)][:]
        elif tag == 'init':
            with h5py.File(fname) as f:
                sims = f['trajs/init'][:]
        elif tag == 'at_0':
            with h5py.File(fname) as f:
                sims = f['trajs/at_0'][:]
        return sims


class PopSimRetro(PopSim):
    """
    Retrospective sampling then forward
    """
    def __init__(self, postmodel, priormodel, opts, posts_dir=None):
        assert postmodel.pop.tvec[-1] == priormodel.pop.tvec[0], (
            postmodel.pop.tvec[-1], priormodel.pop.tvec[0])
        self.postmodel = postmodel
        self.priormodel = priormodel
        self.opts = opts
        self.posts_dir = posts_dir

    def run(self, prefix='', iV=None):
        n_dcomp, n_comp = self.opts.n_dcomp, self.opts.n_comp
        tt0 = util.discrete_T(self.postmodel.pop.tvec, self.opts.tau)
        tt1 = util.discrete_T(self.priormodel.pop.tvec, self.opts.tau)
        assert tt0[-1] == tt1[0], (tt0[-1], tt1[0])
        tt = np.append(tt0, tt1[1:])
        n0 = len(tt0)
        #
        snap_i1 = np.argmax(np.append(self.opts.snap_times, tt1[-1]) > tt1[0])
        #
        laststate_file = '{}_last_retro_state.pkl'.format(prefix)
        retro_continue = os.path.isfile(laststate_file)
        print "laststate_file", laststate_file, retro_continue
        if self.posts_dir is None and retro_continue:
            # if this is original retrospective run and a laststate_file exists
            # continue retrospective sampling from last saved state
            trajs, ni = pickle.load(open(laststate_file, "rb"))
            print "Last retrospective state loaded ({} iterations)".format(ni)
            for c in self.postmodel.compartments:
                for i in xrange(self.postmodel.pop.n_pop):
                    self.postmodel.nodes[c][i].value = trajs[c][i]
        #
        # Set up .H5 files
        #
        # sim_aggregate
        print "Set up sim_aggregate"
        fname = os.path.join(
            self.opts.outdir, '{}sim_aggregate.h5'.format(prefix))
        if self.opts.chunk_no == 0:
            with h5py.File(fname, "w") as f:
                f.create_group('by_time')
                f.create_dataset('by_time/T', data=tt)
                f.create_dataset('by_time/S', dtype=np.int32, shape=(
                    self.opts.n_samp, len(tt), self.opts.n_comp), maxshape=(
                    None, len(tt), self.opts.n_comp))
                f.create_dataset('by_time/Sw', dtype=np.float, shape=(
                    self.opts.n_samp, len(tt), self.opts.n_comp), maxshape=(
                        None, len(tt), self.opts.n_comp))
                for ci in self.opts.cout:
                    f.create_dataset('by_space_{}'.format(ci), shape=(
                        self.opts.n_samp, self.postmodel.pop.n_pop), maxshape=(
                        None, self.postmodel.pop.n_pop), dtype=np.int32)
                    f.create_dataset('mean_full_{}'.format(ci), dtype=float,
                                     shape=(len(tt), self.postmodel.pop.n_pop))
                    # snap times
                    for t in self.opts.snap_times:
                        f.create_dataset(
                            'by_space_{}_t{}'.format(ci, t), shape=(
                                self.opts.n_samp, self.postmodel.pop.n_pop),
                            maxshape=(None, self.postmodel.pop.n_pop),
                            dtype=np.int32)
                # dcount
                if self.opts.draw_data:
                    f.create_dataset('by_time/D', dtype=np.int32, shape=(
                        self.opts.n_samp, len(tt)), maxshape=(None, len(tt)))
                    f.create_dataset('by_time/Dw', dtype=np.float, shape=(
                        self.opts.n_samp, len(tt)), maxshape=(None, len(tt)))
                    f.create_dataset('by_space_dcount', dtype=np.int32, shape=(
                        self.opts.n_samp, self.postmodel.pop.n_pop), maxshape=(
                        None, self.postmodel.pop.n_pop))
                    # snap times
                    for t in self.opts.snap_times:
                        f.create_dataset(
                            'by_space_dcount_t{}'.format(t), dtype=np.int32,
                            shape=(self.opts.n_samp, self.postmodel.pop.n_pop),
                            maxshape=(None, self.postmodel.pop.n_pop))
                else:
                    f.create_dataset('by_time/D', data=h5py.Empty('f'))
                    f.create_dataset('by_time/Dw', data=h5py.Empty('f'))
                    f.create_dataset('by_space_dcount', data=h5py.Empty('f'))
        else:
            # extend data
            with h5py.File(fname, "a") as f:
                dset = f['by_time/S']
                dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                dset = f['by_time/Sw']
                dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                for ci in self.opts.cout:
                    dset = f['by_space_{}'.format(ci)]
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    for t in self.opts.snap_times:
                        dset = f['by_space_{}_t{}'.format(ci, t)]
                        dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                if self.opts.draw_data:
                    dset = f['by_time/D']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    dset = f['by_time/Dw']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    dset = f['by_space_dcount']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    for t in self.opts.snap_times:
                        dset = f['by_space_dcount_t{}'.format(t)]
                        dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)

        # postsim - posterior trajectories
        print "Set up postsim"
        fname_spost = os.path.join(
            self.opts.outdir, '{}postsim.h5'.format(prefix))
        if self.posts_dir is None and not retro_continue:
            # create a new file to save posterior trajectories
            with h5py.File(fname_spost, "w") as f:
                # final spatial snapshot
                f.create_dataset('Sposts', dtype=float, shape=(
                    self.opts.n_samp, self.postmodel.pop.n_pop,
                    self.postmodel.n_comp), maxshape=(
                        None, self.postmodel.pop.n_pop, self.postmodel.n_comp))
                # DPC
                f.create_dataset('sp_by_time', dtype=np.int32, shape=(
                    self.opts.n_samp, len(tt0), self.opts.n_comp), maxshape=(
                        None, len(tt0), self.opts.n_comp))
                f.create_dataset('sp_by_time_wtd', dtype=np.float, shape=(
                    self.opts.n_samp, len(tt0), self.opts.n_comp), maxshape=(
                        None, len(tt0), self.opts.n_comp))
                # intermediary spatial snapshots
                for ci in self.opts.cout:
                    for t in self.opts.snap_times[:snap_i1]:
                        f.create_dataset(
                            'sp_by_space_{}_t{}'.format(ci, t),
                            shape=(self.opts.n_samp, self.postmodel.pop.n_pop),
                            dtype=np.int32, maxshape=(
                                None, self.postmodel.pop.n_pop))
                    f.create_dataset(
                        'sp_mean_{}'.format(ci), dtype=float, shape=(
                            len(tt0), self.postmodel.pop.n_pop))
                # data
                if self.opts.draw_data:
                    f.create_dataset(
                        'Dposts', dtype=np.int32, shape=(
                            self.opts.n_samp, self.postmodel.pop.n_pop),
                        maxshape=(None, self.postmodel.pop.n_pop))
                    f.create_dataset('Dp_by_time', dtype=np.int32, shape=(
                        self.opts.n_samp, len(tt0)), maxshape=(None, len(tt0)))
                    f.create_dataset('Dp_by_time_wtd', dtype=np.float, shape=(
                        self.opts.n_samp, len(tt0)), maxshape=(None, len(tt0)))
                    for t in self.opts.snap_times[:snap_i1]:
                        f.create_dataset(
                            'sp_by_space_dcount_t{}'.format(t), dtype=np.int32,
                            shape=(self.opts.n_samp, self.postmodel.pop.n_pop),
                            maxshape=(None, self.postmodel.pop.n_pop))
            sposts = np.empty((self.opts.n_iter, self.postmodel.pop.n_pop,
                               self.postmodel.n_comp))
            if self.opts.draw_data:
                dposts = np.empty((self.opts.n_iter, self.postmodel.pop.n_pop))
        elif self.posts_dir is None:
            assert self.opts.chunk_no > 0
            # extend data
            with h5py.File(fname_spost, "a") as f:
                dset = f['Sposts']
                dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                dset = f['sp_by_time']
                dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                dset = f['sp_by_time_wtd']
                dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                for ci in self.opts.cout:
                    for t in self.opts.snap_times[:snap_i1]:
                        dset = f['sp_by_space_{}_t{}'.format(ci, t)]
                        dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                if self.opts.draw_data:
                    dset = f['Dposts']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    dset = f['Dp_by_time']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    dset = f['Dp_by_time_wtd']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    for t in self.opts.snap_times[:snap_i1]:
                        dset = f['sp_by_space_dcount_t{}'.format(t)]
                        dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
            sposts = np.empty((self.opts.n_iter, self.postmodel.pop.n_pop,
                               self.postmodel.n_comp))
            if self.opts.draw_data:
                dposts = np.empty((self.opts.n_iter, self.postmodel.pop.n_pop))
        else:
            # read from existing file from self.posts_dir directory
            fname_spost = os.path.join(
                self.posts_dir, '{}postsim.h5'.format(prefix))
            print fname_spost
            with h5py.File(fname_spost) as f:
                Sposts = f['Sposts'][:]
                sp_by_time = f['sp_by_time'][:]
                sp_by_time_wtd = f['sp_by_time_wtd'][:]
                sp_by_space, sp_mean = {}, {}
                for ci in self.opts.cout:
                    sp_by_space[ci] = []
                    for t in self.opts.snap_times[:snap_i1]:
                        sp_by_space[ci] += [f['sp_by_space_{}_t{}'.format(
                            ci, t)][:]]
                    sp_mean[ci] = f['sp_mean_{}'.format(ci)][:]
                if self.opts.draw_data and self.priormodel.data is not None:
                    Dposts = f['Dposts'][:]
                    Dp_by_time = f['Dp_by_time'][:]
                    Dp_by_time_wtd = f['Dp_by_time_wtd'][:]
                    sp_by_space_dcount = [
                        f['sp_by_space_dcount_t{}'.format(t)][:]
                        for t in self.opts.snap_times[:snap_i1]]
                #
                # Adjusting for forward SECD using backward SECI with data
                if self.opts.det_by_data:
                    # load simulated data
                    Dp_by_time = f['Dp_by_time'][:]
                    Dp_by_time_wtd = f['Dp_by_time_wtd'][:]
                    sp_by_space_dcount = [
                        f['sp_by_space_dcount_t{}'.format(t)][:]
                        for t in self.opts.snap_times[:snap_i1]]
                    Dposts = f['Dposts'][:].clip(max=1)
                    # let C = C + I and D = simulated data
                    sp_by_time[:, :, -2] += sp_by_time[:, :, -1] - Dp_by_time
                    sp_by_time[:, :, -1] = Dp_by_time
                    sp_by_time_wtd[:, :, -2] +=\
                        sp_by_time_wtd[:, :, -1] - Dp_by_time_wtd
                    sp_by_time_wtd[:, :, -1] = Dp_by_time_wtd
                    sp_by_space[-1] = sp_by_space_dcount
                    Sposts[:, :, -2] += Sposts[:, :, -1] - Dposts
                    Sposts[:, :, -1] = Dposts
                    assert np.all(Sposts.sum(2) <= 1)
                    assert np.all(
                        Sposts[0, :, :].sum(0) == sp_by_time[0, -1, :])
        #

        def get_kk(ci):
            if iV is not None and iV > ci:
                return np.append(np.arange(ci, iV),
                                 np.arange(iV + 1, n_comp - n_dcomp))
            elif ci < n_comp - n_dcomp:
                return np.arange(ci, n_comp - n_dcomp)
            else:
                return np.arange(ci, n_comp)
        #
        sims_by_time = np.empty((self.opts.n_iter, len(tt), self.opts.n_comp))
        sims_by_time_wtd = np.empty_like(sims_by_time)
        sims_by_space = {ci: [
            np.empty((self.opts.n_iter, self.postmodel.pop.n_pop))
            for i in xrange(len(self.opts.snap_times) + 1)]
                         for ci in self.opts.cout}
        sims_mean = {ci: np.zeros((len(tt), self.postmodel.pop.n_pop))
                     for ci in self.opts.cout}
        if self.opts.draw_data:
            D_by_time = np.empty((self.opts.n_iter, len(tt)))
            D_by_time_wtd = np.empty_like(D_by_time)
            dcount_by_space = [
                np.empty((self.opts.n_iter, self.postmodel.pop.n_pop))
                for i in xrange(len(self.opts.snap_times) + 1)]
        #
        # Run simulations
        #
        print "Start simulations ..."
        for bi in xrange(self.opts.n_block):
            for i in xrange(self.opts.n_iter):
                # Retrospective sampling
                # run if posterior trajectories are not available yet
                if self.posts_dir is None:
                    self.postmodel.update_params()
                    self.postmodel.update_trajs()
                    S_expand0 = self.postmodel.pop_traj(tt0)
                    for k in xrange(S_expand0.shape[2] - 1 - n_dcomp):
                        S_expand0[:, :, k] -= S_expand0[:, :, k + 1]
                    Sf_init = S_expand0[-1, :, :]
                    sposts[i, :, :] = Sf_init
                    sims_by_time0 = S_expand0.sum(1)
                    sims_by_time_wtd0 = (
                        S_expand0 * self.postmodel.pop.hdens[
                            None, :, None]).sum(1)
                    sims_by_space0, sims_mean0 = {}, {}
                    for ci in self.opts.cout:
                        kk = get_kk(ci)
                        sims_mean0[ci] = S_expand0[:, :, kk].sum(2)
                        sims_by_space0[ci] = []
                        for t in self.opts.snap_times[:snap_i1]:
                            it = int((t - tt0[0]) / self.opts.tau) + 1
                            sims_by_space0[ci] += [S_expand0[it, :, kk].sum(0)]
                    #
                    if self.opts.draw_data:
                        D, dcounts0 = self.postmodel.sample_data(
                            S_expand0[:, :, -1 - n_dcomp], tt0,
                            self.opts.snap_times[:snap_i1])
                        D_by_time0 = D.sum(1)
                        D_by_time_wtd0 = (
                            D * self.postmodel.pop.hdens[None, :]).sum(1)
                        D0_last = D[-1, :]
                        dcount0_last = dcounts0[-1]
                        dposts[i, :] = dcount0_last
                # else load from saved samples
                else:
                    j = bi * self.opts.n_iter + i + self.opts.n_skipped_samp\
                        + self.opts.chunk_no * self.opts.n_samp
                    Sf_init = Sposts[j, :, :]
                    sims_by_time0 = sp_by_time[j, :, :]
                    sims_by_time_wtd0 = sp_by_time_wtd[j, :, :]
                    # print Sf_init.sum(0)
                    # print sims_by_time0[-1, :]
                    # assert False
                    sims_by_space0, sims_mean0 = {}, {}
                    for ci in self.opts.cout:
                        sims_by_space0[ci] = [
                            sp_by_space[ci][k][j, :]
                            for k in xrange(len(sp_by_space[ci]))]
                        sims_mean0[ci] = sp_mean[ci]
                    if self.opts.draw_data:
                        D_by_time0 = Dp_by_time[j, :]
                        D_by_time_wtd0 = Dp_by_time_wtd[j, :]
                        dcounts0 = [x[j, :] for x in sp_by_space_dcount]
                        dcount0_last = Dposts[j, :]
                        D0_last = (dcount0_last > 0).astype(int)
                #
                # Forward simulations
                # start from the final spatial snapshot
                # Sf_init[:, 1] = Sf_init[:, 0]
                # Sf_init[:, 0] = 0
                # Sf_init[:, -1] = 1
                # Sf_init[:, 3:5] = 0
                # print Sf_init.sum(0)
                self.priormodel.init.value = Sf_init
                T, S, s0, ignore = self.priormodel.sample(init_update=False)
                S_expand1 = util.get_discrete_expand_S(
                    T, S, s0, self.priormodel.rupdate, tt1)
                # print S_expand1[-1, :, :].sum(0)
                # assert False
                # print (S[:, 1] == 2).sum(), (S[:, 1] == 3).sum()
                #
                if self.opts.draw_data and self.priormodel.data is not None:
                    D, dcounts1 = self.priormodel.sample_data(
                        S_expand1[:, :, -1 - n_dcomp], tt1,
                        self.opts.snap_times)
                    D = (D + D0_last).clip(max=1)
                    D_by_time1 = D.sum(1)
                    D_by_time_wtd1 = (
                        D * self.priormodel.pop.hdens[None, :]).sum(1)
                    dcounts1 = [x + dcount0_last for x in dcounts1]
                #
                # Combine results
                # by time
                '''if n_dcomp > 0:
                    assert n_dcomp == 1, 'not implemented yet'
                    sims_by_time[i, :n0, :] = sims_by_time0
                    sims_by_time[i, n0:, :-n_dcomp] = S_expand1[
                        1:, :, :-n_dcomp].sum(1)
                    #
                    scum = S_expand1[1:, :, -1]
                    scum = scum.cumsum(0).clip(
                        max=self.postmodel.pop.n_maxhost)
                    sims_by_time[i, n0:, -n_dcomp:] = scum.sum(1)
                else:'''
                sims_by_time[i, :n0, :] = sims_by_time0
                sims_by_time[i, n0:, :] = S_expand1[1:, :, :].sum(1)
                sims_by_time_wtd[i, :n0, :] = sims_by_time_wtd0
                sims_by_time_wtd[i, n0:, :] = (
                    S_expand1[1:, :, :] * self.priormodel.pop.hdens[
                        None, :, None]).sum(1)
                # print sims_by_time[i, n0-1, :]
                # print sims_by_time[i, n0, :]
                # assert False
                # by space
                for ci in self.opts.cout:
                    kk = get_kk(ci)
                    sims_mean[ci][:n0, :] = sims_mean0[ci]
                    sims_mean[ci][n0:, :] += S_expand1[1:, :, kk].sum(2)
                    sims_by_space[ci][0][i, :] = S_expand1[-1, :, kk].sum(0)
                    for k, t in enumerate(self.opts.snap_times):
                        if k < snap_i1:
                            sims_by_space[ci][k + 1][i, :] =\
                                sims_by_space0[ci][k]
                        else:
                            it = int((t - tt1[0]) / self.opts.tau) + 1
                            sims_by_space[ci][k + 1][i, :] =\
                                S_expand1[it, :, kk].sum(0)
                    # print "sims_by_space"
                    # print [x[i, :].sum() for x in sims_by_space[ci]]
                # dcount
                if self.opts.draw_data and self.priormodel.data is not None:
                    D_by_time[i, :n0] = D_by_time0
                    D_by_time[i, n0:] = D_by_time1[1:]
                    D_by_time_wtd[i, :n0] = D_by_time_wtd0
                    D_by_time_wtd[i, n0:] = D_by_time_wtd1[1:]
                    dcount_by_space[0][i, :] = dcounts1[-1]
                    for k, t in enumerate(self.opts.snap_times):
                        if k < snap_i1:
                            dcount_by_space[k + 1][i, :] = dcounts0[k]
                        else:
                            dcount_by_space[k + 1][i, :] = dcounts1[
                                k - snap_i1]
                    # print "dcount_by_space"
                    # print [(x[i, :] > 0).sum() for x in dcount_by_space]

            #
            iadd = self.opts.chunk_no * self.opts.n_samp
            ii = slice(bi * self.opts.n_iter + iadd,
                       (bi + 1) * self.opts.n_iter + iadd)
            print "slice", ii
            with h5py.File(fname, "a") as f:
                f['by_time/S'][ii, :, :] = sims_by_time
                f['by_time/Sw'][ii, :, :] = sims_by_time_wtd
                for ci in self.opts.cout:
                    f['by_space_{}'.format(ci)][ii, :] = sims_by_space[ci][0]
                    # snap times
                    for i, t in enumerate(self.opts.snap_times):
                        f['by_space_{}_t{}'.format(ci, t)][ii, :] =\
                            sims_by_space[ci][i + 1]
                if self.opts.draw_data and self.priormodel.data is not None:
                    f['by_time/D'][ii, :] = D_by_time
                    f['by_time/Dw'][ii, :] = D_by_time_wtd
                    f['by_space_dcount'][ii, :] = dcount_by_space[0]
                    for i, t in enumerate(self.opts.snap_times):
                        f['by_space_dcount_t{}'.format(t)][ii, :] =\
                            dcount_by_space[i + 1]
            if self.posts_dir is None:
                with h5py.File(fname_spost, "a") as f:
                    f['Sposts'][ii, :, :] = sposts
                    f['sp_by_time'][ii, :, :] = sims_by_time[:, :n0, :]
                    f['sp_by_time_wtd'][ii, :, :] = sims_by_time_wtd[:, :n0, :]
                    for ci in self.opts.cout:
                        for i, t in enumerate(self.opts.snap_times[:snap_i1]):
                            f['sp_by_space_{}_t{}'.format(ci, t)][ii, :] =\
                                sims_by_space[ci][i+1]
                    if self.opts.draw_data:
                        f['Dposts'][ii, :] = dposts
                        f['Dp_by_time'][ii, :] = D_by_time[:, :n0]
                        f['Dp_by_time_wtd'][ii, :] = D_by_time_wtd[:, :n0]
                        for i, t in enumerate(self.opts.snap_times[:snap_i1]):
                            f['sp_by_space_dcount_t{}'.format(t)][ii, :] =\
                                dcount_by_space[i+1]
                # save last state
                trajs = {}
                for c in self.postmodel.compartments:
                    trajs[c] = [self.postmodel.nodes[c][n].value
                                for n in xrange(self.postmodel.pop.n_pop)]
                pickle.dump((trajs, (bi + 1) * self.opts.n_iter),
                            open(laststate_file, "wb"), -1)
            print "Block {} saved.".format(bi)
            print "sims_by_time", sims_by_time[:, -1, :]
        with h5py.File(fname, "a") as f:
            for ci in self.opts.cout:
                f['mean_full_{}'.format(ci)][:, :] =\
                    sims_mean[ci] * 1. / self.opts.n_samp
        if self.posts_dir is None:
            with h5py.File(fname_spost, "a") as f:
                for ci in self.opts.cout:
                    f['sp_mean_{}'.format(ci)][:, :] =\
                        sims_mean0[ci] * 1. / self.opts.n_samp

    """@staticmethod
    def load_sims(outdir, tag, rid=0, prefix='', with_D=False):
        if tag == 'by_time_wtd':
            pass
        else:
            super(PopSimRetro, PopSimRetro).load_sims(
                outdir, tag, rid, prefix, with_D)"""


class PopSimMixed(PopSim):
    """
    Simulations of a two consecutive population models, switching at tobs
    """
    def __init__(self, premodel, sufmodel, opts):
        self.premodel = premodel
        self.sufmodel = sufmodel
        self.opts = opts
        assert type(self.opts.draw_data) == list and\
            type(self.opts.n_comp) == list

    def run(self, prefix='', iV=None):
        assert self.opts.n_comp[0] == self.opts.n_comp[1]
        n_dcomp, n_comp = self.opts.n_dcomp, self.opts.n_comp[0]
        tt0 = util.discrete_T(self.premodel.pop.tvec, self.opts.tau)
        tt1 = util.discrete_T(self.sufmodel.pop.tvec, self.opts.tau)
        assert tt0[-1] == tt1[0], (tt0[-1], tt1[0])
        tt = np.append(tt0, tt1[1:])
        n0 = len(tt0)
        snap_i1 = np.argmax(np.append(self.opts.snap_times, tt1[-1]) > tt1[0])
        #
        draw_data = self.opts.draw_data[0] * self.opts.draw_data[1]
        no_transform = self.opts.n_comp[0] == self.opts.n_comp[1] and\
            self.opts.draw_data[0] == self.opts.draw_data[0]
        dcount_to_D = self.opts.n_comp[0] == self.opts.n_comp[1] and\
            self.opts.draw_data[0] and not self.opts.draw_data[1]
        #
        # Set up output files
        #
        fname = os.path.join(
            self.opts.outdir, '{}sim_aggregate.h5'.format(prefix))
        if self.opts.chunk_no == 0:
            with h5py.File(fname, "w") as f:
                f.create_group('by_time')
                f.create_dataset('by_time/T', data=tt)
                f.create_dataset('by_time/S', dtype=np.int32, shape=(
                    self.opts.n_samp, len(tt), n_comp), maxshape=(
                    None, len(tt), n_comp))
                for ci in self.opts.cout:
                    f.create_dataset('by_space_{}'.format(ci), shape=(
                        self.opts.n_samp, self.premodel.pop.n_pop), maxshape=(
                        None, self.premodel.pop.n_pop), dtype=np.int32)
                    f.create_dataset('mean_full_{}'.format(ci), dtype=float,
                                     shape=(len(tt), self.premodel.pop.n_pop))
                    # snap times
                    for t in self.opts.snap_times:
                        f.create_dataset(
                            'by_space_{}_t{}'.format(ci, t), shape=(
                                self.opts.n_samp, self.premodel.pop.n_pop),
                            maxshape=(None, self.premodel.pop.n_pop),
                            dtype=np.int32)
                # save dcount only if both models want it
                if draw_data:
                    f.create_dataset('by_time/D', dtype=np.int32, shape=(
                        self.opts.n_samp, len(tt)), maxshape=(None, len(tt)))
                    f.create_dataset('by_space_dcount', dtype=np.int32, shape=(
                        self.opts.n_samp, self.premodel.pop.n_pop), maxshape=(
                        None, self.premodel.pop.n_pop))
                    # snap times
                    for t in self.opts.snap_times:
                        f.create_dataset(
                            'by_space_dcount_t{}'.format(t), dtype=np.int32,
                            shape=(self.opts.n_samp, self.premodel.pop.n_pop),
                            maxshape=(None, self.premodel.pop.n_pop))
                else:
                    f.create_dataset('by_time/D', data=h5py.Empty('f'))
                    f.create_dataset('by_space_dcount', data=h5py.Empty('f'))
        else:
            # extend data
            with h5py.File(fname, "a") as f:
                dset = f['by_time/S']
                dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                for ci in self.opts.cout:
                    dset = f['by_space_{}'.format(ci)]
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    for t in self.opts.snap_times:
                        dset = f['by_space_{}_t{}'.format(ci, t)]
                        dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                if draw_data:
                    dset = f['by_time/D']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    dset = f['by_space_dcount']
                    dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
                    for t in self.opts.snap_times:
                        dset = f['by_space_dcount_t{}'.format(t)]
                        dset.resize(dset.shape[0] + self.opts.n_samp, axis=0)
        #
        # Run simulations
        #
        sims_by_time = np.empty((self.opts.n_iter, len(tt), n_comp))
        sims_by_space = {ci: [
            np.empty((self.opts.n_iter, self.premodel.pop.n_pop))
            for i in xrange(len(self.opts.snap_times) + 1)]
                         for ci in self.opts.cout}
        sims_mean = {ci: np.zeros((len(tt), self.premodel.pop.n_pop))
                     for ci in self.opts.cout}
        if self.opts.draw_data:
            D_by_time = np.empty((self.opts.n_iter, len(tt)))
            dcount_by_space = [
                np.empty((self.opts.n_iter, self.premodel.pop.n_pop))
                for i in xrange(len(self.opts.snap_times) + 1)]

        def get_kk(ci):
            if iV is not None and iV > ci:
                return np.append(np.arange(ci, iV),
                                 np.arange(iV + 1, n_comp - n_dcomp))
            elif ci < n_comp - n_dcomp:
                return np.arange(ci, n_comp - n_dcomp)
            else:
                return np.arange(ci, n_comp)
        #
        print "Start simulations ..."
        for bi in xrange(self.opts.n_block):
            for i in xrange(self.opts.n_iter):
                # Premodel
                T, S, s0, ignore = self.premodel.sample()
                S_expand0 = util.get_discrete_expand_S(
                    T, S, s0, self.premodel.rupdate, tt0)
                #
                if self.opts.draw_data[0] and self.premodel.data is not None:
                    D, dcounts0 = self.premodel.sample_data(
                        S_expand0[:, :, -1 - n_dcomp], tt0,
                        self.opts.snap_times[:snap_i1])
                    D_by_time0 = D.sum(1)
                    D0_last = D[-1, :]
                    dcount0_last = dcounts0[-1]
                # Forward simulations
                # start from the final spatial snapshot
                if no_transform:
                    self.sufmodel.init.value = S_expand0[-1, :, :]
                elif dcount_to_D:
                    self.sufmodel.init.value = S_expand0[-1, :, :]
                    self.sufmodel.init.value[:, -2] += S_expand0[-1, :, -1]
                    self.sufmodel.init.value[:, -1] = dcount0_last.clip(
                        max=1)
                else:
                    # not implemented yet
                    assert False, (self.opts.n_comp, self.opts.draw_data)
                #
                T, S, s0, ignore = self.sufmodel.sample(init_update=False)
                S_expand1 = util.get_discrete_expand_S(
                    T, S, s0, self.sufmodel.rupdate, tt1)
                # print S_expand1[-1, :, :].sum(0)
                # assert False
                # print (S[:, 1] == 2).sum(), (S[:, 1] == 3).sum()
                #
                if self.opts.draw_data[1] and self.sufmodel.data is not None:
                    D, dcounts1 = self.sufmodel.sample_data(
                        S_expand1[:, :, -1 - n_dcomp], tt1,
                        self.opts.snap_times)
                    D = (D + D0_last).clip(max=1)
                    D_by_time1 = D.sum(1)
                    dcounts1 = [x + dcount0_last for x in dcounts1]
                #
                # Combine results
                # by time
                sims_by_time[i, :n0, :] = S_expand0.sum(1)
                sims_by_time[i, n0:, :] = S_expand1[1:, :, :].sum(1)
                # by space
                for ci in self.opts.cout:
                    kk = get_kk(ci)
                    sims_mean[ci][:n0, :] = S_expand0[:, :, kk].sum(2)
                    sims_mean[ci][n0:, :] += S_expand1[1:, :, kk].sum(2)
                    sims_by_space[ci][0][i, :] = S_expand1[-1, :, kk].sum(0)
                    for k, t in enumerate(self.opts.snap_times):
                        it = int((t - tt1[0]) / self.opts.tau) + 1
                        if k < snap_i1:
                            sims_by_space[ci][k + 1][i, :] =\
                                S_expand0[it, :, kk].sum(0)
                        else:
                            sims_by_space[ci][k + 1][i, :] =\
                                S_expand1[it, :, kk].sum(0)
                    # print "sims_by_space"
                    # print [x[i, :].sum() for x in sims_by_space[ci]]
                # dcount
                if draw_data:
                    D_by_time[i, :n0] = D_by_time0
                    D_by_time[i, n0:] = D_by_time1[1:]
                    dcount_by_space[0][i, :] = dcounts1[-1]
                    for k, t in enumerate(self.opts.snap_times):
                        if k < snap_i1:
                            dcount_by_space[k + 1][i, :] = dcounts0[k]
                        else:
                            dcount_by_space[k + 1][i, :] = dcounts1[
                                k - snap_i1]
            #
            iadd = self.opts.chunk_no * self.opts.n_samp
            ii = slice(bi * self.opts.n_iter + iadd,
                       (bi + 1) * self.opts.n_iter + iadd)
            print "slice", ii
            with h5py.File(fname, "a") as f:
                f['by_time/S'][ii, :, :] = sims_by_time
                for ci in self.opts.cout:
                    f['by_space_{}'.format(ci)][ii, :] = sims_by_space[ci][0]
                    # snap times
                    for i, t in enumerate(self.opts.snap_times):
                        f['by_space_{}_t{}'.format(ci, t)][ii, :] =\
                            sims_by_space[ci][i + 1]
                if draw_data:
                    f['by_time/D'][ii, :] = D_by_time
                    f['by_space_dcount'][ii, :] = dcount_by_space[0]
                    for i, t in enumerate(self.opts.snap_times):
                        f['by_space_dcount_t{}'.format(t)][ii, :] =\
                            dcount_by_space[i + 1]
            print "Block {} saved.".format(bi)
            print "sims_by_time", sims_by_time[:, -1, :]
        with h5py.File(fname, "a") as f:
            for ci in self.opts.cout:
                f['mean_full_{}'.format(ci)][:, :] =\
                    sims_mean[ci] * 1. / self.opts.n_samp
