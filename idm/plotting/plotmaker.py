from colormap import gradient_cmap, Spectral_colors
from layout import remove_tick_marks
from corner import corner
import idm.model.sstats as sstats
import colormap as cm

import numpy as np
import matplotlib.pyplot as plt
import datetime
import seaborn as sns
from matplotlib import gridspec
import matplotlib.ticker as ticker
from sklearn.metrics import roc_curve, auc
from sklearn.metrics.cluster import contingency_matrix

sns.set()
sns.set_style('whitegrid')
plt.rcParams["mathtext.fontset"] = "cm"


percentile = np.array([2.5, 12.5, 25, 75, 87.5, 97.5])


def make_fillplot(
        T, S, outfile, data=[], trajs=[], xticks=None, labels=None,
        data_labels=None, with_mean=True, ax=None, plot_type='step',
        xlabel='Year', ylabel=None, figsize=None, plot_median=True,
        fontsize=12, lloc='upper left', ylim=None, with_legend=True,
        mean_only=False, xlim=None, ticksize=None, tmark=None, n_cells=1,
        seq_color=None, pcs=None):
    pcs = percentile if pcs is None else pcs
    n_sim, n_dt, n_var = S.shape
    labels = np.arange(n_var) if labels is None else labels
    pct = (pcs * n_sim / 100).astype(int)
    n_layers = len(pcs)
    n_colors = n_var + len(data)
    colors = cm.qualitative_colors(n_colors, seq_color) if seq_color is None\
        else cm.sequential_colors(n_colors, seq_color)

    #
    S = S * 1. / n_cells
    if ylabel is None:
        ylabel = 'Infection prevalance' if n_cells > 1\
                 else 'Infection area (km2)'
    #
    if ax is None:
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111, frame_on=True)
    for k in xrange(n_var):
        if not mean_only:
            layers = np.zeros((n_layers, n_dt))
            for it in range(n_dt):
                layers[:, it] = np.sort(S[:, it, k])[pct]
            for i in range(n_layers / 2):
                ax.fill_between(T, layers[i, :], layers[n_layers - i - 1, :],
                                alpha=.2, facecolor=colors[k], edgecolor=None,
                                linewidth=.1)
        if with_mean:
            Sm = np.median(S[:, :, k], axis=0) if plot_median\
                 else S[:, :, k].mean(0)
            if plot_type == 'step':
                ax.step(T, Sm, linewidth=1,
                        color=colors[k], where='post', label=labels[k])
            elif plot_type == 'line':
                ax.plot(T, Sm, linewidth=2,
                        color=colors[k], label=labels[k])
        for i in xrange(len(trajs)):
            ax.step(trajs[i][0], trajs[i][1][:, k] * 1. / n_cells,
                    color=colors[k], where='post', linewidth=1)
    for k, D in enumerate(data):
        if len(D[0]) == 0:
            continue
        label = data_labels[k] if data_labels is not None\
            else 'Data_{}'.format(k)
        ax.step(D[0], D[1] * 1. / n_cells, color=colors[-k-1],
                label=label, where='post')
    if tmark is not None:
        ax.axvline(tmark, 0, 0.7, color='k', linestyle='--', linewidth=1)
    if xticks is not None:
        plt.sca(ax)
        plt.xticks(*xticks)
    if ylim is not None:
        ax.set_ylim(*ylim)
    if xlim is not None:
        ax.set_xlim(*xlim)
    if ticksize is not None:
        plt.xticks(fontsize=ticksize)
        plt.yticks(fontsize=ticksize)
    #
    if outfile is not None:
        if ylabel is not None:
            ax.set_ylabel(ylabel, fontsize=fontsize)
        if xlabel is not None:
            ax.set_xlabel(xlabel, fontsize=fontsize)
        if with_legend:
            plt.legend(loc=lloc, fontsize=fontsize)
        fig.savefig(outfile, dpi=200)
        plt.clf()
    return colors


def make_traceplot(
        psamps, model, outfile, dp_len=1,
        param_list=['alpha', 'beta', 'epsilon', 'dprob']):
    # parameters
    fig, ax = plt.subplots(nrows=len(psamps), figsize=(7.5, len(psamps) * 2.))
    plt.tight_layout()
    i = 0
    for pname in param_list:
        samp = psamps[pname]
        if pname == 'dprob':
            for k in xrange(dp_len):
                ax[i].plot(samp[:, k], label='dp_{}'.format(k))
        elif pname == 'drate':
            for k in xrange(samp.shape[1]):
                ax[i].plot(samp[:, k], label='dr_{}$'.format(k))
        else:
            for k in xrange(samp.shape[1]):
                ax[i].plot(samp[:, k], label=r'$\{}_{}$'.format(
                    pname, k))
        ax[i].legend(loc='best')
        i += 1
    ax[i].plot(psamps['logl'], label='logl')
    ax[i].legend(loc='best')
    i += 1
    ax[i].plot(psamps['logp'], label='logp')
    ax[i].legend(loc='best')
    fig.savefig(outfile, dpi=200)
    plt.clf()


def make_trajplot(T, S, dtau, date0, outfile, tunit='year'):
    assert len(dtau) == 2
    tau = dtau[1]
    print tau
    T = T.astype(float) * tau
    print T
    if tunit == 'year':
        # convert T to year unit
        for i in xrange(len(T)):
            date = date0 + datetime.timedelta(T[i])
            T[i] = date.year + (date.month + 0.5) / 12.
        locs = np.arange(np.round(T[0]), np.round(T[-1]) + 1).astype(int)
        labels = ['{}'.format(yr) for yr in locs]
    elif tunit == 'month':
        # convert T to month unit
        for i in xrange(len(T)):
            date = date0 + datetime.timedelta(T[i])
            T[i] = date.month + (date.day + 0.5) / 30.
        locs = np.arange(np.floor(T[0]), np.floor(T[-1]) + 1).astype(int)
        labels = ['{}'.format(datetime.date(2000, m, 1).strftime('%b'))
                  for m in locs]
    #
    fig = plt.figure(figsize=(9, 5))
    ax = fig.add_subplot(111, frame_on=True)
    ax.step(T, S, where='post')
    plt.xticks(locs, labels)
    fig.savefig(outfile, dpi=200)
    plt.clf()


def make_heatplot(xedges, yedges, H, outfile, dpi=100,
                  vmin=None, vmax=None, labels=None):
    fig = plt.figure()
    ax = fig.add_subplot(111, frame_on=True)
    remove_tick_marks(ax)
    H = np.ma.masked_array(H, mask=~np.isfinite(H))
    vmin = H.min() if vmin is None else vmin
    vmax = H.max() if vmax is None else vmax
    plt.pcolormesh(xedges, yedges, H, cmap=gradient_cmap(
        Spectral_colors(inverse=True)), vmin=vmin, vmax=vmax)
    if labels is not None:
        assert len(labels) == 2
        plt.xlabel(labels[0])
        plt.ylabel(labels[1])
    plt.colorbar()
    fig.savefig(outfile, dpi=dpi)
    plt.clf()


def make_cornerplot(param_df, outfile, dpi=100):
    fig = corner(param_df)
    fig.savefig(outfile, dpi=dpi)


def make_param_histogram(
        param_df_list, params, outfile, pnames=None, dpi=100, gsize=None,
        figsize=(10, 5.5), units=None, eps_scale=0, with_prior=True,
        fontsize=12, with_hist=True, bw='scott', med_ticks=False, labels=None):
    colors = cm.qualitative_colors(len(param_df_list))
    plt.rc('xtick', labelsize=16)
    ec = 10 ** eps_scale
    pnames = param_df_list[0].columns if pnames is None else pnames
    n_plots = len(pnames)
    units = {k: '' for k in pnames} if units is None else units
    fig = plt.figure(figsize=figsize)
    gsize = [1, n_plots] if gsize is None else gsize
    gs = gridspec.GridSpec(gsize[0], gsize[1])
    ax = [plt.subplot(gs[i, j]) for i in xrange(gsize[0])
          for j in xrange(gsize[1])]
    percentiles = [2.5, 50, 97.5]
    for i in xrange(n_plots):
        for k, param_df in enumerate(param_df_list):
            samp = param_df[pnames[i]]
            if samp.values[0] is None:
                continue
            smean = samp.mean()
            print pnames[i], smean
            pcts = np.array([np.percentile(samp, q) for q in percentiles])
            pcts = pcts * ec if 'epsilon' in pnames[i] or 'mu' in pnames[i]\
                else pcts
            decimals = 3 - len(str(smean).split('.')[0])
            pcts = np.around(pcts, decimals=decimals)
            pct_str =\
                str(pcts[1]) + '_{' + str(pcts[0]) + '}^{' + str(pcts[2]) + '}'
            if with_hist:
                sns.distplot(samp, ax=ax[i], norm_hist=True, label='posterior')
            else:
                pbw = bw * smean if np.isscalar(bw) else bw
                pbw = pbw * 2 if 'mu' in pnames[i] or\
                    'varepsilon' in pnames[i] else pbw
                if labels is not None and i == 0:
                    label = r'{}: ${}$'.format(labels[k], pct_str)
                else:
                    label = r'${}$'.format(pct_str)
                sns.kdeplot(samp, bw=pbw, ax=ax[i], shade=True, legend=False,
                            label=label, color=colors[k])
                ymin, ymax = ax[i].get_ylim()
                ydata_max = ax[i].lines[-1].get_ydata().max()
                if ydata_max * 1.04 > ymax:
                    ax[i].set_ylim([ymin, ydata_max * 1.05])
        ax[i].legend(loc='upper right', fontsize=fontsize)
        #
        ax[i].set_yticks([])
        if med_ticks:
            decimals = eps_scale + 1 if 'epsilon' in pnames[i] else 1
            pcts = np.around(pcts, decimals=decimals)
            print pnames[i], pcts, decimals
            ax[i].set_xticks(pcts)
        else:
            ax[i].set_xticks([])
        if with_prior:
            x1, x2 = ax[i].get_xlim()
            y1, y2 = ax[i].get_ylim()
            xx = np.linspace(max(1e-10, x1), x2, 1000)
            yy = np.exp(params[pnames[i].split('_')[0]].prior_logpdf(xx))
            if pnames[i] == 'eta' or pnames[i] == 'dprob' or\
               pnames[i] == 'alpha' or pnames[i] == 'delta':
                yy = yy / 100.
            ax[i].plot(xx, yy, label='prior distribution', color='k',
                       lw=1, linestyle='--')
            ax[i].set_xlim(x1, x2)
            ax[i].set_ylim(y1, y2)
        if 'epsilon' in pnames[i]:
            ax[i].xaxis.set_major_formatter(ticker.FuncFormatter(
                lambda x, pos: '{0:.1g}'.format(x * ec)))
        else:
            ax[i].xaxis.set_major_formatter(
                ticker.FormatStrFormatter('%.2g'))
        if ('epsilon' in pnames[i] or 'mu' in pnames[i]) and eps_scale != 0:
            ax[i].set_title(r'$\{}\times 10^{}$ {}'.format(
                pnames[i], eps_scale, units[pnames[i]]), fontsize=fontsize)
        elif pnames[i] == 'dprob':
            ax[i].set_title(r'$\pi$ {}'.format(units[pnames[i]]),
                            fontsize=fontsize)
        elif pnames[i] == 'sigma':
            pname = 'sigma^{-1}'
            ax[i]. set_title(r'$\{}$ {}'.format(
                pname, units[pnames[i]]), fontsize=fontsize)
        else:
            ax[i].set_title(r'$\{}$ {}'.format(
                pnames[i], units[pnames[i]]), fontsize=fontsize)
        ax[i].set_xlabel('')
    if with_prior:
        ax[0].legend(loc='upper right', fontsize=fontsize)
    gs.tight_layout(fig)
    fig.savefig(outfile, dpi=dpi)
    plt.clf()


def make_correlogram(
        df, grid, sw, outfile, sims=None, MI=None, omodel='Noulli',
        binary=False, gf=2., N_r=100, dpi=100, dplot=False, ax=None,
        colors=None, labels=None, with_mean=True, fontsize=12,
        ymin=None, ymax=None, title=None, data_r=1, sim_r=1, tree_wtd=False,
        hdens=None, plot_median=True):
    if tree_wtd:
        assert hdens is not None
    colors = cm.qualitative_colors(2) if colors is None else colors
    labels = ['sim', 'data'] if labels is None else labels
    if dplot:
        assert ax is None
        fig, axes = plt.subplots(2, 1)
        ax, ax_d = axes
    elif ax is None:
        fig, ax = plt.subplots()
    delta_r = grid.csize * gf
    # data
    y = np.zeros(grid.n_pos)  # per-cell counts
    for i in xrange(grid.n_pos):
        if omodel == 'Nomial':
            y[i] = df[df.Gi == i].PosNo.values.sum()
        elif omodel == 'Noulli':
            # count the number of positive trials for each cell
            df1 = df[df.Gi == i]
            df1 = df1.groupby(['TauNo']).agg({'PosNo': max})
            y[i] = df1.PosNo.values.clip(max=1).sum()
    y = (y > 0).astype(int) if binary else y
    if tree_wtd:
        y = y * hdens
    rs, mis_data, ws = sstats.make_moransi_correlogram(
        y, sw, binary, delta_r, N_r)
    print type(mis_data), len(mis_data)
    mis_data = np.array(mis_data) * data_r
    # simulation
    if sims is not None or MI is not None:
        if MI is None:
            MI = []
            for i in xrange(sims.shape[0]):
                # infection probability for each cell
                y = (sims[i, :] > 0).astype(int)  # if binary else sims[i, :] - DELETED
                if tree_wtd:
                    y = y * hdens
                rs, mis, ws = sstats.make_moransi_correlogram(
                    y, sw, binary, delta_r, N_r)
                MI += [mis]
            MI = np.array(MI)
        MI *= sim_r
        # fill plot
        n_layers = len(percentile)
        layers = np.zeros((n_layers, N_r - 1))
        pct = (percentile * MI.shape[0] / 100).astype(int)
        for it in range(N_r - 1):
            layers[:, it] = np.sort(MI[:, it])[pct]
        for i in range(n_layers / 2):
            ax.fill_between(
                rs[1:-1], layers[i, :], layers[n_layers - i - 1, :],
                alpha=.2, color=colors[0], edgecolor=None, linewidth=.1)
        if with_mean:
            m = np.median(MI, axis=0) if plot_median else MI.mean(0)
            ax.plot(rs[1:-1], m, linewidth=1, color=colors[0],
                    label=labels[0])
    # print rs
    ax.plot(rs[1:-1], mis_data, color=colors[1], label=labels[1])
    ymin = 0 if ymin is None and np.min(mis_data) > 0 else None
    ax.set_ylim(bottom=ymin)
    if ymax is not None:
        ax.set_ylim(top=ymax)
    ax.legend(loc='best', fontsize=fontsize)
    ax.set_xlabel('Distance (km)', fontsize=fontsize)
    ax.set_ylabel("Moran's I score", fontsize=fontsize)
    ax.tick_params(labelsize=fontsize)
    if dplot:
        ax_d.plot(rs[1:-1], ws)
    if title is not None:
        ax.set_title(title, fontsize=fontsize + 2)
    plt.tight_layout()
    if outfile is not None:
        plt.legend(loc='best', fontsize=fontsize)
        print "save", outfile
        fig.savefig(outfile, dpi=dpi)
        plt.clf()
    return MI


def make_dprob_spectrum(
        x, slist, outfile, labels=None, cb_title=None,
        figsize=(10, 5.5), dpi=100):
    fig = plt.figure(figsize=figsize)
    n_plots, pf = len(slist), 4
    labels = ['' for i in xrange(n_plots)] if labels is None else labels
    assert len(labels) == n_plots
    gs = gridspec.GridSpec(1, n_plots * pf + 1)
    ax = [plt.subplot(gs[0, 0:pf])]
    ax += [plt.subplot(gs[0, pf * i:pf * (i + 1)], sharey=ax[0])
           for i in xrange(1, n_plots)]
    ax += [plt.subplot(gs[0, -1])]  # colorbar
    n_lines = len(slist[0])
    cmap = gradient_cmap(Spectral_colors(inverse=True))
    cl = [cmap(a) for a in np.linspace(0, 1, n_lines)]
    # dprobs
    for i in xrange(n_plots):
        dprob = slist[i]
        if i > 0:
            plt.setp(ax[i].get_yticklabels(), visible=False)
        for j in xrange(n_lines):
            ax[i].plot(x, dprob[j, :], color=cl[j], alpha=0.8)
        ax[i].set_title(labels[i])
    # colorbar
    a = np.array([[0, 1]])
    im = ax[-1].imshow(a, cmap=cmap)
    ax[-1].set_visible(False)
    cb_ax = fig.add_axes([0.85, 0.2, 0.03, 0.5])  # [left, bottom, w, h]
    cb = plt.colorbar(im, cax=cb_ax, format='%.2g')
    if cb_title is not None:
        cb.ax.set_title(cb_title)
    fig.savefig(outfile, dpi=dpi)
    plt.clf()


def make_barplot(x, counts, outfile, labels=None, figsize=None, dpi=100,
                 fontsize=12, with_legend=False, xlabel='Year',
                 ylabel='# detected sites'):
    assert len(x) == len(counts[0])
    fig = plt.figure(figsize=figsize)
    ll = []
    for i in xrange(len(counts)):
        if i == 0:
            ll += [plt.bar(x, counts[i])[0]]
        else:
            ll += [plt.bar(x, counts[i], bottom=counts[i-1])[0]]
    if with_legend and (labels is not None):
        plt.legend(ll, labels)
    plt.xlabel(xlabel, fontsize=fontsize)
    plt.ylabel(ylabel, fontsize=fontsize)
    plt.tick_params(labelsize=fontsize)
    fig.savefig(outfile, dpi=dpi)


def make_roc_plot(
        risk, obs, outfile, labels=None, figsize=(6, 6), dpi=100,
        fontsize=12, title=None):
    assert risk.ndim == 2, risk.ndim
    labels = ['' for k in xrange(risk.shape[1])] if labels is None else labels
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, frame_on=True)
    for k in xrange(risk.shape[1]):
        obs_risk = obs[k] if type(obs) == list else obs
        fpr, tpr, th = roc_curve(obs_risk, risk[:, k])
        roc_auc = auc(fpr, tpr)
        ax.plot(fpr, tpr, label='{} (AUC = {:.3f})'.format(labels[k], roc_auc))
    ax.plot([0, 1], [0, 1], color='k', lw=1, linestyle='--')
    ax.set_xlabel('False positive rate', fontsize=fontsize)
    ax.set_ylabel('True positive rate', fontsize=fontsize)
    if title is not None:
        ax.set_title(title, fontsize=fontsize + 4)
    plt.legend(loc='best', fontsize=fontsize)
    fig.savefig(outfile, dpi=dpi)


def make_roc_cloud(
        sims, obs, outfile, figsize=(6, 6), dpi=100, fontsize=12):
    assert sims.ndim == 2, sims.ndim
    #
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, frame_on=True)
    fpr, tpr = [], []
    for i in xrange(sims.shape[0]):
        c = contingency_matrix(obs, sims[i, :]).astype(float)
        fpr += [c[0, 1] / c[0, :].sum()]
        tpr += [c[1, 1] / c[1, :].sum()]
    ax.scatter(fpr, tpr, alpha=0.1, marker='o')
    ax.plot([0, 1], [0, 1], color='k', lw=1, linestyle='--')
    ax.set_xlabel('False positive rate', fontsize=fontsize)
    ax.set_ylabel('True positive rate', fontsize=fontsize)
    fig.savefig(outfile, dpi=dpi)
