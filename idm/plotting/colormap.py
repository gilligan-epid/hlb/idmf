import numpy as np
from matplotlib.colors import LinearSegmentedColormap
import seaborn as sns


_RdYlGn_data = {
    'blue': [
        (0.0, 0.14901961386203766, 0.14901961386203766),
        (0.10000000000000001, 0.15294118225574493, 0.15294118225574493),
        (0.20000000000000001, 0.26274511218070984, 0.26274511218070984),
        (0.29999999999999999, 0.3803921639919281, 0.3803921639919281),
        (0.40000000000000002, 0.54509806632995605, 0.54509806632995605),
        (0.5, 0.74901962280273438, 0.74901962280273438),
        (0.59999999999999998, 0.54509806632995605, 0.54509806632995605),
        (0.69999999999999996, 0.41568627953529358, 0.41568627953529358),
        (0.80000000000000004, 0.38823530077934265, 0.38823530077934265),
        (0.90000000000000002, 0.31372550129890442, 0.31372550129890442),
        (1.0, 0.21568627655506134, 0.21568627655506134)],
    'green': [
        (0.0, 0.0, 0.0),
        (0.10000000000000001, 0.18823529779911041, 0.18823529779911041),
        (0.20000000000000001, 0.42745098471641541, 0.42745098471641541),
        (0.29999999999999999, 0.68235296010971069, 0.68235296010971069),
        (0.40000000000000002, 0.87843137979507446, 0.87843137979507446),
        (0.5, 1.0, 1.0),
        (0.59999999999999998, 0.93725490570068359, 0.93725490570068359),
        (0.69999999999999996, 0.85098040103912354, 0.85098040103912354),
        (0.80000000000000004, 0.74117648601531982, 0.74117648601531982),
        (0.90000000000000002, 0.59607845544815063, 0.59607845544815063),
        (1.0, 0.40784314274787903, 0.40784314274787903)],
    'red': [
        (0.0, 0.64705884456634521, 0.64705884456634521),
        (0.10000000000000001, 0.84313726425170898, 0.84313726425170898),
        (0.20000000000000001, 0.95686274766921997, 0.95686274766921997),
        (0.29999999999999999, 0.99215686321258545, 0.99215686321258545),
        (0.40000000000000002, 0.99607843160629272, 0.99607843160629272),
        (0.5, 1.0, 1.0),
        (0.59999999999999998, 0.85098040103912354, 0.85098040103912354),
        (0.69999999999999996, 0.65098041296005249, 0.65098041296005249),
        (0.80000000000000004, 0.40000000596046448, 0.40000000596046448),
        (0.90000000000000002, 0.10196078568696976, 0.10196078568696976),
        (1.0, 0.0, 0.0)]
}


def gradient_cmap(colors, nsteps=256, bounds=None):
    # Make a colormap that interpolates between a set of colors
    ncolors = len(colors)
    # assert colors.shape[1] == 3
    if bounds is None:
        bounds = np.linspace(0, 1, ncolors)

    reds = []
    greens = []
    blues = []
    for b, c in zip(bounds, colors):
        reds.append((b, c[0], c[0]))
        greens.append((b, c[1], c[1]))
        blues.append((b, c[2], c[2]))

    cdict = {'red': tuple(reds),
             'green': tuple(greens),
             'blue': tuple(blues)}

    cmap = LinearSegmentedColormap('grad_colormap', cdict, nsteps)
    return cmap


def white_to_color_cmap(color, nsteps=256):
    # Get a red-white-black cmap
    cdict = {
        'red': ((0.0, 1.0, 1.0),
                (1.0, color[0], color[0])),
        'green': ((0.0, 1.0, 1.0),
                  (1.0, color[1], color[0])),
        'blue': ((0.0, 1.0, 1.0),
                 (1.0, color[2], color[0]))}
    cmap = LinearSegmentedColormap('white_color_colormap', cdict, nsteps)
    return cmap


def combo_white_to_color_cmap(colors, nsteps=1000):
    ncolors = colors.shape[0]
    # assert colors.shape[1] == 3
    bounds = np.linspace(0, 1, ncolors + 1)

    # Get a red-white-black cmap
    reds = [(0.0, 1.0, 1.0)]
    greens = [(0.0, 1.0, 1.0)]
    blues = [(0.0, 1.0, 1.0)]
    for i, b in enumerate(bounds):
        if i == 0:
            continue
        reds.append((b, colors[i-1][0], 1.0))
        greens.append((b, colors[i-1][1], 1.0))
        blues.append((b, colors[i-1][2], 1.0))

    cdict = {'red': tuple(reds),
             'green': tuple(greens),
             'blue': tuple(blues)}
    cmap = LinearSegmentedColormap('white_color_colormap', cdict, nsteps)
    return cmap


def cmap_discretize(cmap, N):
    """
    Return a discrete colormap from the continuous colormap cmap.

        cmap: colormap instance, eg. cm.jet.
        N: number of colors.

    Example
        x = resize(arange(100), (5,100))
        djet = cmap_discretize(cm.jet, 5)
        imshow(x, cmap=djet)

    """
    if type(cmap) == str:
        cmap = get_cmap(cmap)
    colors_i = np.concatenate((np.linspace(0, 1., N), (0., 0., 0., 0.)))
    colors_rgba = cmap(colors_i)
    indices = np.linspace(0, 1., N + 1)
    cdict = {}
    for ki, key in enumerate(('red', 'green', 'blue')):
        cdict[key] = [(indices[i], colors_rgba[i - 1, ki], colors_rgba[i, ki]) for i in xrange(N + 1)]
    return LinearSegmentedColormap(cmap.name + "_%d" % N, cdict, 1024)


def harvard_colors():
    return [
        np.array([165,  28,  48]) / 255.0,    # 0.  crimson
        np.array([ 78, 132, 196]) / 255.0,    # 1.  blue bonnet
        np.array([ 82, 133,  76]) / 255.0,    # 2.  ivy
        np.array([196, 150,  26]) / 255.0,    # 3.  gold
        np.array([  0, 156, 163]) / 255.0,    # 4.  aqua
        np.array([232, 125,  30]) / 255.0,    # 5.  saffron
        np.array([137, 150, 160]) / 255.0,    # 6.  slate
        np.array([ 41,  51,  82]) / 255.0,    # 7.  indigo
        np.array([195, 215, 164]) / 255.0,    # 8.  pear
        np.array([255, 219, 109]) / 255.0,    # 9.  lemon
        np.array([ 43,  13,  97]) / 255.0,    # 10. dark purple
    ]


def RdYlGn_colors():
    """
    http://colorbrewer2.org/#type=diverging&scheme=RdYlGn&n=11
    """
    return [
        np.array([165,   0,  38]) / 255.,  # 0. dark red
        np.array([215,  48,  39]) / 255.,  # 1. red
        np.array([244, 109,  67]) / 255.,  # 2. orange
        np.array([253, 174,  97]) / 255.,  # 3. light orange
        np.array([254, 224, 139]) / 255.,  # 4. very light orange
        np.array([255, 255, 191]) / 255.,  # 5. yellow
        np.array([217, 239, 139]) / 255.,  # 6.
        np.array([166, 217, 106]) / 255.,  # 7. very light green
        np.array([102, 189,  99]) / 255.,  # 8. light green
        np.array([26,  152,  80]) / 255.,  # 9. green
        np.array([0,   104,  55]) / 255.,  # 10. dark green
    ]


def Spectral_colors(inverse=False):
    """
    http://colorbrewer2.org/#type=diverging&scheme=Spectral&n=11
    """
    cols = [
        np.array([158,   1,  66]) / 255.,  # 0. dark pink red
        np.array([213,  62,  79]) / 255.,  # 1. pink
        np.array([244, 109,  67]) / 255.,  # 2. orange
        np.array([253, 174,  97]) / 255.,  # 3. light orange
        np.array([254, 224, 139]) / 255.,  # 4. very light orange
        np.array([255, 255, 191]) / 255.,  # 5. yellow
        np.array([230, 245, 152]) / 255.,  # 6. yellow green
        np.array([171, 221, 164]) / 255.,  # 7. very light blue green
        np.array([102, 194, 165]) / 255.,  # 8. light blue green
        np.array([50,  136, 189]) / 255.,  # 9. blue
        np.array([94,   79, 162]) / 255.,  # 10. purple
    ]
    if inverse:
        return cols[::-1]
    else:
        return cols


def PuOr_colors():
    """
    http://colorbrewer2.org/#type=diverging&scheme=PuOr&n=11
    """
    return [
        np.array([127,  59,   8]) / 255.,  # 0. dark brown
        np.array([179,  88,   6]) / 255.,  # 1. brown
        np.array([244, 130,  20]) / 255.,  # 2. light brown
        np.array([253, 184,  99]) / 255.,  # 3. skin
        np.array([254, 224, 182]) / 255.,  # 4. light skin
        np.array([247, 247, 247]) / 255.,  # 5. white
        np.array([216, 218, 235]) / 255.,  # 6. very light purple
        np.array([178, 171, 210]) / 255.,  # 7.
        np.array([128, 115, 172]) / 255.,  # 8.
        np.array([84,   39, 136]) / 255.,  # 9. purple
        np.array([45,    0,  75]) / 255.,  # 10. dark purple
    ]


def Grey_colors():
    """
    http://colorbrewer2.org/#type=sequential&scheme=Greys&n=9
    """
    return [
        np.array([255, 255, 255]) / 255.,  # 0. white
        np.array([240, 240, 240]) / 255.,  # 1. slightly grey
        np.array([217, 217, 217]) / 255.,  # 2.
        np.array([189, 189, 189]) / 255.,  # 3.
        np.array([150, 150, 150]) / 255.,  # 4.
        np.array([115, 115, 115]) / 255.,  # 5.
        np.array([82,   82,  82]) / 255.,  # 6. grey
        np.array([37,   37,  37]) / 255.,  # 7. dark grey
        np.array([0,     0,   0]) / 255.,  # 8. black
    ]


def Green_colors():
    " http://colorbrewer2.org/#type=sequential&scheme=Greens&n=9 "
    return [
        np.array([247, 252, 245]) / 255.,  # very light green
        np.array([229, 245, 224]) / 255.,
        np.array([199, 233, 192]) / 255.,
        np.array([161, 217, 155]) / 255.,
        np.array([116, 196, 118]) / 255.,
        np.array([65,  171,  93]) / 255.,
        np.array([35,  139,  69]) / 255.,
        np.array([0,   109,  44]) / 255.,
        np.array([0,   68,   27]) / 255.,  # dark green
    ]


def YlGn_colors():
    " http://colorbrewer2.org/#type=sequential&scheme=YlGn&n=9 "
    return [
        np.array([255, 255, 229]) / 255.,  # very light yellow
        np.array([247, 252, 185]) / 255.,
        np.array([217, 240, 163]) / 255.,
        np.array([173, 221, 142]) / 255.,
        np.array([120, 198, 121]) / 255.,
        np.array([65,  171,  93]) / 255.,
        np.array([35,  132,  67]) / 255.,
        np.array([0,   104,  55]) / 255.,
        np.array([0,    69,  41]) / 255.,  # dark green
    ]


def primary_colors():
    # return RdYlGn_colors()[::-1][1:-1]
    return Spectral_colors()[::-1]#[1:-1]


def secondary_colors():
    return PuOr_colors()[1:]


def backgd_colors():
    c = Grey_colors()[::-1][4]
    return [c, c]


def qualitative_colors(n_colors, name=None):
    # name \in {deep, muted, bright, pastel, dark, colorblind}
    return [np.array(c) for c in sns.color_palette(name, n_colors=n_colors)]


def sequential_colors(n_colors, name=None):
    if name is None:
        return [np.array(c) for c in sns.cubehelix_palette(
            n_colors + 2, reverse=True)][1:-1]
    else:
        return [np.array(c) for c in sns.color_palette(
            name, n_colors=n_colors + 1)][:-1]
