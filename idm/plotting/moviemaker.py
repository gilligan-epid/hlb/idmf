from mapmaker import make_patches
from colormap import gradient_cmap, backgd_colors, primary_colors
from colormap import qualitative_colors

import pyproj
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np
import matplotlib.animation as animation
from matplotlib import gridspec, ticker
import os
import datetime
import sys

if sys.platform == 'win32':
    ffmpeg_path = os.path.join('E:/', 'Tools', 'bins', 'ffmpeg')
elif sys.platform == 'linux' or sys.platform == 'linux2':
    ffmpeg_path = os.path.join(
        '/data', 'user_spaces', 'van25', 'CODE', 'bins', 'ffmpeg', 'ffmpeg')
plt.rcParams['animation.ffmpeg_path'] = unicode(ffmpeg_path)


def make_heatmovie(
        T, S, grid, region, const, outfile, vmax=1, hidx=None, dtau='Week',
        bmap=None, date0=None, title='', figsize=(10, 5.5), dpi=200, nf=None):
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)
    if bmap is None:
        pc = make_patches(const.projected_shpfile[region])
        edges = grid.edges
        bmap = plt
        ax.set_xlim(edges[0][[0, -1]])
        ax.set_ylim(edges[1][[-1, 0]])
    else:
        x0, y0 = grid.edges[0][0], grid.edges[1][-1]
        bproj = pyproj.Proj(bmap.proj4string)
        assert np.all(bmap(x0, y0) == bproj(x0, y0))
        pc = make_patches(const.shpfile[region], bmap)
        edges = grid.project_edges(bproj)
    ax.add_collection(pc)
    if hidx is not None:
        bmap.pcolormesh(edges[0], edges[1], grid.get_grid(hidx),
                        norm=colors.LogNorm(vmin=0.01, vmax=1),
                        cmap=gradient_cmap(backgd_colors()),
                        zorder=3, alpha=.9)
    metadata = dict(title=title)
    writer = animation.FFMpegWriter(fps=3, bitrate=20000, metadata=metadata)
    nT = len(T)
    nf = nT if nf is None else nf
    date0 = const.date0[region] if date0 is None else date0
    with writer.saving(fig, outfile, dpi):
        for i in xrange(min(nT, nf)):
            print '{}/{}'.format(i, nT)
            bmap.pcolormesh(edges[0], edges[1], grid.get_grid(S[i, :]),
                            norm=colors.LogNorm(vmin=0.01, vmax=vmax),
                            cmap=gradient_cmap(primary_colors()),
                            zorder=4, alpha=.9)
            if i == 0:
                cb = plt.colorbar(shrink=0.5, format='%.2g')
                cb.ax.tick_params(labelsize=8)
                cb.locator = ticker.FixedLocator([.01, .1, 1])
                cb.update_ticks()
            date = date0 + datetime.timedelta(T[i])
            plt.title(date.strftime("%b %Y"), fontsize=18, ha='right')
            writer.grab_frame()
    plt.close("all")


def make_heat_vs_data_movie(
        T, S, pos, grid, region, const, outfile,
        old_dt=30, vmax=None, vmin=None, hidx=None, bmap=None, title='',
        figsize=(12, 5.5), dpi=200, date0=None, nf=None):
    assert vmax is not None and vmin is not None
    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(1, 2, width_ratios=[1.25, 1])
    ax1 = plt.subplot(gs[0], facecolor='w')
    ax2 = plt.subplot(gs[1], facecolor='w')
    if bmap is None:
        edges = grid.edges
        bmap = plt
        ax1.set_xlim(edges[0][[0, -1]])
        ax1.set_ylim(edges[1][[-1, 0]])
        ax2.set_xlim(edges[0][[0, -1]])
        ax2.set_ylim(edges[1][[-1, 0]])
    else:
        x0, y0 = grid.edges[0][0], grid.edges[1][-1]
        bproj = pyproj.Proj(bmap.proj4string)
        assert np.all(bmap(x0, y0) == bproj(x0, y0))
        edges = grid.project_edges(bproj)
    ax1.add_collection(make_patches(const.shpfile[region], bmap))
    ax2.add_collection(make_patches(const.shpfile[region], bmap))
    ax1.set_title('Predicted', fontsize=12)
    ax2.set_title('Reported', fontsize=12)
    plt.tight_layout()
    if hidx is not None:
        bmap.pcolormesh(edges[0], edges[1], grid.get_grid(hidx),
                        norm=colors.LogNorm(vmin=0.01, vmax=1),
                        cmap=gradient_cmap(backgd_colors()),
                        zorder=3, alpha=.9, ax=ax1)
        bmap.pcolormesh(edges[0], edges[1], grid.get_grid(hidx),
                        norm=colors.LogNorm(vmin=0.01, vmax=1),
                        cmap=gradient_cmap(backgd_colors()),
                        zorder=3, alpha=.9, ax=ax2)
    metadata = dict(title=title)
    writer = animation.FFMpegWriter(fps=3, bitrate=20000, metadata=metadata)
    pos_old = [[], []]
    nT = len(T)
    nf = nT if nf is None else nf
    date0 = const.date0[region] if date0 is None else date0
    with writer.saving(fig, outfile, dpi):
        for i in xrange(min(nT, nf)):
            print '{}/{}'.format(i, nT)
            im = bmap.pcolormesh(edges[0], edges[1], grid.get_grid(S[i, :]),
                                 norm=colors.LogNorm(vmin=vmin, vmax=vmax),
                                 cmap=gradient_cmap(primary_colors()),
                                 zorder=4, alpha=.9, ax=ax1)
            if i == 0:
                cb = plt.colorbar(im, shrink=0.4, format='%.2g', ax=ax1)
                cb.ax.tick_params(labelsize=8)
                cb.locator = ticker.FixedLocator([.01, .1, 1])
                cb.update_ticks()
            bmap.scatter(pos_old[0], pos_old[1], 5, marker='o', lw=.25,
                         facecolor='b', edgecolor='w', zorder=4, alpha=.9,
                         antialiased=True, ax=ax2)
            pos_new = [[], []]
            for j in xrange(max(0, i-old_dt), i+1):
                pos_new[0] += list(pos[j][0])
                pos_new[1] += list(pos[j][1])
            bmap.scatter(pos_new[0], pos_new[1], 5, marker='o', lw=.25,
                         facecolor='r', edgecolor='w', zorder=4, alpha=.9,
                         antialiased=True, ax=ax2)
            date = date0 + datetime.timedelta(T[i])
            plt.suptitle(date.strftime("%b %Y"), fontsize=18, ha='right')
            writer.grab_frame()
            #
            if i > old_dt:
                pos_old[0] += list(pos[i-old_dt][0])
                pos_old[1] += list(pos[i-old_dt][1])
    plt.close("all")


def make_multiple_heatmovie(
        T, Slist, grid, region, const, outfile, gsize, bmap, labels,
        vmax=1, hidx=None, title='', figsize=(10, 5.5), dpi=200):
    assert np.prod(gsize) == len(Slist), (np.prod(gsize), len(Slist))
    assert len(Slist) == len(labels)
    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(gsize[0] * 2, gsize[1] + 2, width_ratios=[
            100 for i in xrange(gsize[1])] + [5, 1])
    ax = [plt.subplot(gs[2*i:2*(i+1), j], facecolor='w')
          for i in xrange(gsize[0]) for j in xrange(gsize[1])]
    cb_ax = plt.subplot(gs[1:-1, -2], frame_on=False, facecolor='w')
    x0, y0 = grid.edges[0][0], grid.edges[1][-1]
    bproj = pyproj.Proj(bmap.proj4string)
    assert np.all(bmap(x0, y0) == bproj(x0, y0))
    edges = grid.project_edges(bproj)
    for i in xrange(len(ax)):
        ax[i].add_collection(make_patches(const.shpfile[region], bmap))
        ax[i].set_title(labels[i], fontsize=14, fontweight='bold')
        if hidx is not None:
            bmap.pcolormesh(edges[0], edges[1], grid.get_grid(hidx),
                            norm=colors.LogNorm(vmin=0.01, vmax=1),
                            cmap=gradient_cmap(backgd_colors()),
                            zorder=3, alpha=.9, ax=ax[i])
    plt.tight_layout()
    #
    metadata = dict(title=title)
    writer = animation.FFMpegWriter(fps=3, bitrate=20000, metadata=metadata)
    nT = len(T)
    with writer.saving(fig, outfile, dpi):
        for i in xrange(nT):
            print '{}/{}'.format(i, nT)
            for k in xrange(len(ax)):
                im = bmap.pcolormesh(
                    edges[0], edges[1], grid.get_grid(Slist[k][i, :]),
                    norm=colors.LogNorm(vmin=0.01, vmax=vmax), alpha=.9,
                    cmap=gradient_cmap(primary_colors()), zorder=4, ax=ax[k])
            if i == 0:
                cb = plt.colorbar(im, format='%.2g', cax=cb_ax)
                cb.ax.tick_params(labelsize=12)
                cb.locator = ticker.FixedLocator([.01, .1, 1])
                cb.update_ticks()
            date = const.date0[region] + datetime.timedelta(T[i])
            plt.suptitle(date.strftime("%b %Y"), fontsize=18, ha='right')
            writer.grab_frame()
    plt.close("all")


def make_overview_movie(
        T, S_time, S_space, tau, yr0, grid, region, const, bmap, outfile,
        labels, data=None, xticks=None, title='', vmax=1,
        hidx=None, figsize=(10, 5.5), dpi=200):
    percentile = np.array([2.5, 12.5, 25, 75, 87.5, 97.5])
    n_layers = len(percentile)
    n_sim, n_dt, n_var = S_time.shape
    pct = (percentile * n_sim / 100).astype(int)
    layers = []
    for k in xrange(n_var):
        layers += [np.zeros((n_layers, n_dt))]
        for it in range(n_dt):
            layers[k][:, it] = np.sort(S_time[:, it, k])[pct]
    dd0 = (datetime.datetime(yr0, 1, 1) - const.date0[region]).days
    Ty = (T - dd0) / 365.  # change to year scale
    line_colors = qualitative_colors(n_var + int(data is not None))
    #
    fig = plt.figure(figsize=figsize)
    width_ratios = [1.25] + [1 for i in xrange(len(S_space) - 1)]
    gs = gridspec.GridSpec(2, len(S_space), width_ratios=width_ratios,
                           height_ratios=[1, 1])
    ax1 = [plt.subplot(gs[0, i], facecolor='w') for i in xrange(len(S_space))]
    x0, y0 = grid.edges[0][0], grid.edges[1][-1]
    bproj = pyproj.Proj(bmap.proj4string)
    assert np.all(bmap(x0, y0) == bproj(x0, y0))
    edges = grid.project_edges(bproj)
    for i in xrange(len(ax1)):
        ax1[i].add_collection(make_patches(const.shpfile[region], bmap))
        ax1[i].set_title(labels[i], fontsize=14, fontweight='bold')
        if hidx is not None:
            bmap.pcolormesh(edges[0], edges[1], grid.get_grid(hidx),
                            norm=colors.LogNorm(vmin=0.01, vmax=1),
                            cmap=gradient_cmap(backgd_colors()),
                            zorder=3, alpha=.9, ax=ax1[i])
    ax2 = plt.subplot(gs[1, :])
    buff = [0.1, 100]
    ax2.set_xlim([Ty[0] - buff[0], Ty[-1] + buff[0]])
    ax2.set_ylim([-buff[1], S_time[:, -1, -1].max() + buff[1]])
    plt.tight_layout()
    #
    metadata = dict(title=title)
    writer = animation.FFMpegWriter(fps=3, bitrate=20000, metadata=metadata)
    nT = int(n_dt/tau) + 1
    with writer.saving(fig, outfile, dpi):
        for i in xrange(nT):
            print '{}/{}'.format(i, nT)
            i_to = min((i + 1) * tau, n_dt) - 1
            ii = slice(i * tau, i_to + 2)
            # Riskmaps
            for k in xrange(len(ax1)):
                im = bmap.pcolormesh(
                    edges[0], edges[1], grid.get_grid(S_space[k][i_to, :]),
                    norm=colors.LogNorm(vmin=0.01, vmax=vmax), alpha=.9,
                    cmap=gradient_cmap(primary_colors()), zorder=4, ax=ax1[k])
            # DPC
            for k in xrange(n_var):
                for j in range(n_layers / 2):
                    ax2.fill_between(
                        Ty[ii], layers[k][j, ii], layers[k][n_layers-j-1, ii],
                        alpha=.2, facecolor=line_colors[k], edgecolor=None,
                        linewidth=.1)
                ax2.step(Ty[ii], S_time[:, ii, k].mean(0),
                         color=line_colors[k], where='post', label=labels[k])
            if data is not None:
                ax2.step(data[0][ii], data[1][ii], color=line_colors[-1],
                         label='Data', where='post')
            if i == 0:
                plt.legend(loc='upper left')
                if xticks is not None:
                    plt.xticks(*xticks)
                cb = plt.colorbar(im, shrink=0.4, format='%.2g', ax=ax1[0])
                cb.ax.tick_params(labelsize=8)
                cb.locator = ticker.FixedLocator([.01, .1, 1])
                cb.update_ticks()
            date = const.date0[region] + datetime.timedelta(T[i_to])
            ax2.set_title(date.strftime("%b %Y"), fontsize=14, ha='right')
            writer.grab_frame()
    plt.close("all")


def make_fillmovie(T, S, tau, dd0, outfile, data=None,
                   xticks=None, labels=None, dpi=200, title=''):
    percentile = np.array([2.5, 12.5, 25, 75, 87.5, 97.5])
    n_layers = len(percentile)
    n_sim, n_dt, n_var = S.shape
    labels = np.arange(n_var) if labels is None else labels
    pct = (percentile * n_sim / 100).astype(int)
    layers = []
    for k in xrange(n_var):
        layers += [np.zeros((n_layers, n_dt))]
        for it in range(n_dt):
            layers[k][:, it] = np.sort(S[:, it, k])[pct]
    colors = qualitative_colors(n_var + int(data is not None))
    #
    T = (T - dd0) / 365.  # change to year scale
    fig = plt.figure(figsize=(9, 5))
    ax = fig.add_subplot(111, frame_on=True)
    buff = [0.1, 10]
    ax.set_xlim([T[0] - buff[0], T[-1] + buff[0]])
    ax.set_ylim([-buff[1], S[:, -1, 0].max() + buff[1]])
    metadata = dict(title=title)
    writer = animation.FFMpegWriter(fps=3, bitrate=20000, metadata=metadata)
    nT = int(n_dt/tau) + 1
    with writer.saving(fig, outfile, dpi):
        for it in xrange(nT):
            print '{}/{}'.format(it, nT)
            ii = slice(it * tau, min((it + 1) * tau + 1, n_dt + 1))
            for k in xrange(n_var):
                for j in range(n_layers / 2):
                    ax.fill_between(
                        T[ii], layers[k][j, ii], layers[k][n_layers-j-1, ii],
                        alpha=.2, facecolor=colors[k], edgecolor=None,
                        linewidth=.1)
                ax.step(T[ii], S[:, ii, k].mean(0),
                        color=colors[k], where='post', label=labels[k])
            if data is not None:
                ax.step(data[0][ii], data[1][ii], color=colors[-1],
                        label='Data', where='post')
            if it == 0:
                plt.legend(loc='best')
                if xticks is not None:
                    plt.xticks(*xticks)
            plt.title(str(it))
            writer.grab_frame()
    plt.close("all")


def make_sim_combined_movie(
        T, sims, data, grid, region, const, outfile, labels, gsize, bmap,
        old_dt=30, vmax=None, vmin=None, hidx=None, title='',
        figsize=(12, 5.5), dpi=200, i0=0, with_label=True, msize=5):
    assert np.prod(gsize) >= len(sims) + len(data)
    assert len(labels) == len(sims) + len(data)
    # Axes
    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(gsize[0], gsize[1], width_ratios=[1] * gsize[1])
    ax = [plt.subplot(gs[i, j], facecolor='w', frame_on=False)
          for i in xrange(gsize[0]) for j in xrange(gsize[1])]
    # Set up basemaps
    x0, y0 = grid.edges[0][0], grid.edges[1][-1]
    bproj = pyproj.Proj(bmap.proj4string)
    assert np.all(bmap(x0, y0) == bproj(x0, y0))
    edges = grid.project_edges(bproj)
    for i in xrange(len(labels)):
        ax[i].add_collection(make_patches(const.shpfile[region], bmap))
        if with_label:
            ax[i].set_title(labels[i], fontsize=14, fontweight='bold')
        if hidx is not None:
            bmap.pcolormesh(edges[0], edges[1], grid.get_grid(hidx),
                            norm=colors.LogNorm(vmin=0.01, vmax=1),
                            cmap=gradient_cmap(backgd_colors()),
                            zorder=3, alpha=.9, ax=ax[i])
    ax[-1].set_xlabel(const.date0[region].strftime("%b %Y"), fontsize=18)
    plt.tight_layout()
    # Movie
    metadata = dict(title=title)
    writer = animation.FFMpegWriter(fps=3, bitrate=20000, metadata=metadata)
    nT = len(T)
    nC = len(sims)
    pos_old = [[[], []] for k in xrange(len(data))]
    with writer.saving(fig, outfile, dpi):
        for i in xrange(i0, nT):
            print '{}/{}'.format(i, nT)
            # heatmaps
            for k in xrange(nC):
                bmap.pcolormesh(
                    edges[0], edges[1], grid.get_grid(sims[k][i, :]),
                    norm=colors.LogNorm(vmin=0.01, vmax=vmax), alpha=.9,
                    cmap=gradient_cmap(primary_colors()), zorder=4, ax=ax[k])
            # data
            for k in xrange(len(data)):
                bmap.scatter(
                    pos_old[k][0], pos_old[k][1], msize, marker='o', lw=.25,
                    facecolor='k', edgecolor='w', zorder=4, alpha=.9,
                    antialiased=True, ax=ax[k + nC])
                pos_new = [[], []]
                for j in xrange(max(0, i-old_dt), i+1):
                    pos_new[0] += list(data[k][j][0])
                    pos_new[1] += list(data[k][j][1])
                bmap.scatter(
                    pos_new[0], pos_new[1], msize, marker='o', lw=.25,
                    facecolor='r', edgecolor='w', zorder=4, alpha=.9,
                    antialiased=True, ax=ax[k + nC])
                #
                if i > old_dt:
                    pos_old[k][0] += list(data[k][i-old_dt][0])
                    pos_old[k][1] += list(data[k][i-old_dt][1])
            date = const.date0[region] + datetime.timedelta(T[i])
            ax[-1].set_xlabel(date.strftime("%b %Y"), fontsize=18, ha='right')
            writer.grab_frame()

    plt.close("all")
