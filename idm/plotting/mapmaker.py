from colormap import gradient_cmap, backgd_colors, primary_colors
from layout import remove_tick_marks

from mpl_toolkits.basemap import Basemap
from shapely.geometry import Polygon, MultiPolygon, shape
from matplotlib.collections import PatchCollection
from descartes import PolygonPatch
import fiona
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from pysal.esda.mapclassify import Natural_Breaks
from matplotlib import ticker
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from matplotlib import gridspec

import pyproj
import numpy as np
import seaborn as sns


def make_basemap(bounds, **kwargs):
    """
    Create a basemap with provided lon/lat bounds
    """
    x0, y0, x1, y1 = bounds
    m = Basemap(llcrnrlon=x0, llcrnrlat=y0, urcrnrlon=x1, urcrnrlat=y1,
                **kwargs)
    return m


def make_aea_basemap(bounds, lat_1, lat_2, lon_0, lat_0, resolution='l'):
    """
    Create a basemap in Albers Equal Area projection,
    i.e. the area is preserved but the shape is not. Distortions is very large
    near the poles.
    """
    x0, y0, x1, y1 = bounds
    m = Basemap(projection='aea', resolution=resolution,
                llcrnrlon=x0, llcrnrlat=y0, urcrnrlon=x1, urcrnrlat=y1,
                lat_1=lat_1, lat_2=lat_2, lon_0=lon_0, lat_0=lat_0)
    return m


def make_patches(shpfile, bmap=None, shade='bright'):
    patches = []
    if shade == 'dark':
        fc, ec, lw = '#555555', '#787878', .75
    else:
        assert shade == 'bright'
        fc, ec, lw = 'w', '#111111', .75
    if bmap is not None:
        bmap.readshapefile(shpfile, 'shp', drawbounds=False)
        for xy in bmap.shp:
            patches += [PolygonPatch(Polygon(xy), fc=fc, ec=ec,
                                     lw=lw, alpha=.9, zorder=4)]
    else:
        with fiona.open(shpfile+'.shp', 'r') as shp:
            for f in shp:
                p = shape(f['geometry'])
                if type(p) == MultiPolygon:
                    for g in p.geoms:
                        patches += [PolygonPatch(g, fc=fc, ec=ec,
                                                 lw=lw, alpha=.9, zorder=4)]
                else:
                    patches += [PolygonPatch(p, fc=fc, ec=ec,
                                             lw=lw, alpha=.9, zorder=4)]
    return PatchCollection(patches, match_original=True)


def make_heatmap(
        vals, grid, region, const, outfile, vmax=None, vmin=0.01,
        hidx=None, bmap=None, figsize=None, dpi=None, mcolor=primary_colors(),
        mapscale=None, ms_loc='ll', cb_title=None, title=None, cb_loc=0,
        cb_fontsize=12, shade='bright', init=None, with_cb=True,
        dataProj=None):
    figsize = (10, 5.5) if figsize is None else figsize
    dpi = 200 if dpi is None else dpi
    vmin = vals.min() if vmin is None else vmin
    vmax = vals.max() if vmax is None else vmax
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)
    if bmap is None:
        pc = make_patches(const.projected_shpfile[region], shade=shade)\
            if const is not None else None
        edges = grid.edges
        bmap = plt
        remove_tick_marks(ax)
    else:
        x0, y0 = grid.edges[0][0], grid.edges[1][-1]
        bproj = pyproj.Proj(bmap.proj4string)
        assert np.all(bmap(x0, y0) == bproj(x0, y0))
        pc = make_patches(const.shpfile[region], bmap, shade=shade)
        edges = grid.project_edges(bproj, dataProj)
    if hidx is not None:
        bmap.pcolormesh(edges[0], edges[1], grid.get_grid(hidx),
                        norm=colors.LogNorm(vmin=vmin, vmax=vmax),
                        cmap=gradient_cmap(backgd_colors()),
                        zorder=3, alpha=.9)
    im = bmap.pcolormesh(
        edges[0], edges[1], grid.get_grid(vals), norm=colors.LogNorm(
            vmin=vmin, vmax=vmax), cmap=gradient_cmap(mcolor),
        zorder=4, alpha=.9)
    if init is not None:
        bmap.scatter(init[0], init[1], 10, marker='x',
                     lw=1, facecolor='k', edgecolor='k',
                     zorder=4, antialiased=True, label='infectious sites')
        plt.legend(loc='lower center', fontsize=cb_fontsize)

    if with_cb:
        if cb_loc == 0:
            # outside, mid right
            cb = plt.colorbar(shrink=0.2, format='%.2g', aspect=5)
            cb.ax.tick_params(labelsize=8)
        elif cb_loc == 1:
            # inside, lower left corner
            cb_ax = fig.add_axes([0.05, 0.1, 0.03, 0.2])  # [left, bottom, w, h]
            cb = plt.colorbar(im, cax=cb_ax, format='%.2g')
        if vmax == 1 and vmin == 0.01:
            cb.locator = ticker.FixedLocator([.01, .1, 1])
            # cb.formatter = ticker.FixedFormatter([
            #     'Low: 0.01', '0.1', 'High: 0.99'])
        else:
            cb.locator = ticker.FixedLocator([vmin, vmax])
        cb.update_ticks()
        cb.ax.tick_params(labelsize=cb_fontsize)
        if cb_title is not None:
            cb.ax.set_title(cb_title, fontsize=cb_fontsize)
    if pc is not None:
        ax.add_collection(pc)
    if title is not None:
        ax.set_title(title)
    if mapscale is not None:
        try:
            x0, y0, xF, yF = const.bounds[region]
        except AttributeError:
            x0, y0, xF, yF = const.cyl_bounds(region)
        if ms_loc == 'll':
            lon, lat = x0 + 2.8, y0 + 0.5
        elif ms_loc == 'lr':
            lon, lat = xF - 0.26, y0 + 0.1
        elif ms_loc == 'ul':
            lon, lat = x0 + 0.26, yF - 0.1
        elif ms_loc == 'ur':
            lon, lat = xF - 0.26, yF - 0.1
        else:
            assert False
        bmap.drawmapscale(lon, lat, (x0 + xF) * .5, (y0 + yF) * .5,
                          mapscale, barstyle='fancy', fillcolor2='#555555')

    plt.tight_layout()
    fig.savefig(outfile, dpi=dpi)
    plt.clf()


def make_scattermap(pos, grid, region, const, outfile, neg=None, init=None,
                    vmax=None, hvals=None, hidx=None, bmap=None, label=None,
                    dpi=None, figsize=None, msize=5, mcolor=primary_colors(),
                    alpha=.9, shade='bright', dataProj=None):
    figsize = (10, 5.5) if figsize is None else figsize
    dpi = 200 if dpi is None else dpi
    if vmax is None:
        vmax = hvals.max()
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)
    if bmap is None:
        pc = make_patches(const.projected_shpfile[region], shade=shade)\
             if const is not None else None
        edges = grid.edges
        bmap = plt
        remove_tick_marks(ax)
    else:
        x0, y0 = grid.edges[0][0], grid.edges[1][-1]
        bproj = pyproj.Proj(bmap.proj4string)
        assert np.all(bmap(x0, y0) == bproj(x0, y0))
        pc = make_patches(const.shpfile[region], bmap, shade=shade)
        edges = grid.project_edges(bproj, dataProj)
    if hidx is not None:
        bmap.pcolormesh(
            edges[0], edges[1], grid.get_grid(hidx),
            norm=colors.LogNorm(vmin=0.01, vmax=1),
            cmap=gradient_cmap(backgd_colors()), zorder=2, alpha=.9)
    if hvals is not None:
        bmap.pcolormesh(
            edges[0], edges[1], grid.get_grid(hvals),
            norm=colors.LogNorm(vmin=0.01, vmax=vmax),
            cmap=gradient_cmap(mcolor), zorder=3, alpha=.9)
        cb = plt.colorbar(shrink=0.5, format='%.2g')
        cb.ax.tick_params(labelsize=8)
        cb.locator = ticker.FixedLocator([.01, .1, 1])
        cb.update_ticks()
    bmap.scatter(pos[0], pos[1], msize, label='positives',
                 marker='o', lw=.25, facecolor='r', edgecolor='w',
                 zorder=4, alpha=alpha, antialiased=True)
    if neg is not None:
        bmap.scatter(neg[0], neg[1], msize, label='negatives',
                     marker='o', lw=.25, facecolor='b', edgecolor='w',
                     zorder=3, alpha=alpha, antialiased=True)
    if init is not None:
        bmap.scatter(init[0], init[1], msize, label=label,
                     marker='o', lw=.25, facecolor='k', edgecolor='w',
                     zorder=4, alpha=alpha, antialiased=True)
    if pc is not None:
        ax.add_collection(pc)
    if neg is not None:
        plt.legend(loc='lower left', fontsize=18, markerscale=3)
    fig.savefig(outfile, dpi=dpi)
    plt.clf()


def make_spreadmap(
        yrs, pos_list, grid, region, const, outfile, old_diff=True,
        vmax=None, hvals=None, hidx=None, bmap=None, label=None, dpi=None,
        figsize=None, msize=5, mcolor=primary_colors(), alpha=.9, gsize=None,
        shade='bright', dataProj=None, fontsize=12,):
    sc = sns.color_palette("bright", 10)
    scolors = [sc[2]]
    if old_diff:
        scolors += [sc[3]]
    n_plots = len(yrs)
    assert len(pos_list) == n_plots, (n_plots, len(pos_list))
    figsize = (10, 5.5) if figsize is None else figsize
    dpi = 200 if dpi is None else dpi
    if vmax is None:
        vmax = hvals.max()
    fig = plt.figure(figsize=figsize)
    gsize = [1, n_plots] if gsize is None else gsize
    gs = gridspec.GridSpec(gsize[0], gsize[1])
    ax = []
    for i in xrange(gsize[0]):
        ax += [plt.subplot(gs[i, j], facecolor='w', frameon=False)
               for j in xrange(gsize[1])]
    x0, y0 = grid.edges[0][0], grid.edges[1][-1]
    bproj = pyproj.Proj(bmap.proj4string)
    assert np.all(bmap(x0, y0) == bproj(x0, y0))
    edges = grid.project_edges(bproj, dataProj)
    pos_old = [[], []]
    for i in xrange(n_plots):
        if pos_list[i] is None:
            break
        ax[i].add_collection(make_patches(const.shpfile[region], bmap,
                                          shade=shade))
        ax[i].set_title(yrs[i], fontsize=fontsize)
        if hidx is not None:
            bmap.pcolormesh(
                edges[0], edges[1], grid.get_grid(hidx),
                norm=colors.LogNorm(vmin=0.01, vmax=1), ax=ax[i],
                cmap=gradient_cmap(backgd_colors()), zorder=2, alpha=.9)
        if hvals is not None:
            bmap.pcolormesh(
                edges[0], edges[1], grid.get_grid(hvals),
                norm=colors.LogNorm(vmin=0.01, vmax=vmax), ax=ax[i],
                cmap=gradient_cmap(mcolor), zorder=3, alpha=.9)
        bmap.scatter(
            pos_old[0], pos_old[1], msize, marker='o', lw=.25,
            facecolor=scolors[-1], edgecolor='w', zorder=4, alpha=alpha,
            antialiased=True, ax=ax[i])
        pos_new = pos_list[i]
        bmap.scatter(
            pos_new[0], pos_new[1], msize, marker='o', lw=.25,
            facecolor=scolors[0], edgecolor='w', zorder=4, alpha=alpha,
            antialiased=True, ax=ax[i])
        pos_old[0] += list(pos_new[0])
        pos_old[1] += list(pos_new[1])
    gs.tight_layout(fig)
    fig.savefig(outfile, dpi=dpi)
    plt.clf()


def make_sim_spreadmap(
        yrs, sim_snaps, data_snaps, grid, region, const, outfile,
        vmax=None, hvals=None, hidx=None, bmap=None, labels=None, dpi=None,
        figsize=None, msize=5, mcolor=primary_colors(), alpha=.9, fontsize=12,
        data_labels=None, shade='bright', dataProj=None):
    n_data = len(data_snaps)
    n_cols = len(yrs)
    if sim_snaps is not None:
        n_rows = len(sim_snaps) + n_data
        assert len(sim_snaps[0]) == n_cols, (n_cols, len(sim_snaps[0]))
    else:
        n_rows = n_data
    # assert len(data_snaps[0]) == n_cols, (n_cols, len(data_snaps[0]))
    labels = ['' for i in xrange(n_rows - 1)] if labels is None else labels
    data_labels = ['Data' for k in xrange(n_data)]\
        if data_labels is None else data_labels
    labels += data_labels
    figsize = (10, 5.5) if figsize is None else figsize
    dpi = 200 if dpi is None else dpi
    if vmax is None:
        vmax = hvals.max()
    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(n_rows, n_cols)
    ax = []
    for i in xrange(n_rows):
        ax += [[plt.subplot(gs[i, j], facecolor='w', frameon=False)
                for j in xrange(n_cols)]]
    if bmap is None:
        pc = None
        edges = grid.edges
        bmap = plt
    else:
        x0, y0 = grid.edges[0][0], grid.edges[1][-1]
        bproj = pyproj.Proj(bmap.proj4string)
        assert np.all(bmap(x0, y0) == bproj(x0, y0))
        pc = make_patches(const.shpfile[region], bmap, shade=shade)
        edges = grid.project_edges(bproj, dataProj)
    data_pos = [[[], []] for k in xrange(n_data)]
    data_neg = [[[], []] for k in xrange(n_data)]
    for j in xrange(n_cols):
        # simulations
        if sim_snaps is not None:
            for i in xrange(n_rows - n_data):
                if i == 0:
                    ax[i][j].set_title(yrs[j], fontsize=fontsize)
                if j == 0:
                    ax[i][j].set_ylabel(labels[i], fontsize=fontsize)
                if pc is not None:
                    ax[i][j].add_collection(make_patches(
                        const.shpfile[region], bmap, shade=shade))
                plt.sca(ax[i][j])
                if hidx is not None:
                    bmap.pcolormesh(
                        edges[0], edges[1], grid.get_grid(hidx),
                        norm=colors.LogNorm(vmin=0.01, vmax=1),
                        cmap=gradient_cmap(backgd_colors()), zorder=2,
                        alpha=.9)
                bmap.pcolormesh(
                    edges[0], edges[1], grid.get_grid(sim_snaps[i][j]),
                    norm=colors.LogNorm(vmin=0.01, vmax=1),
                    cmap=gradient_cmap(primary_colors()), zorder=2, alpha=.9)
        else:
            ax[0][j].set_title(yrs[j], fontsize=fontsize)
        # data - the last rows
        for k in xrange(n_data):
            if data_snaps[k][j] is None:
                ax[k-n_data][j].axis('off')
                continue
            if pc is not None:
                ax[k-n_data][j].add_collection(make_patches(
                    const.shpfile[region], bmap, shade=shade))
            if j == 0:
                ax[k-n_data][j].set_ylabel(labels[k-n_data], fontsize=fontsize)
            plt.sca(ax[k-n_data][j])
            if hidx is not None:
                bmap.pcolormesh(
                    edges[0], edges[1], grid.get_grid(hidx),
                    norm=colors.LogNorm(vmin=0.01, vmax=1),
                    cmap=gradient_cmap(backgd_colors()), zorder=2, alpha=.9)
            if hvals is not None:
                bmap.pcolormesh(
                    edges[0], edges[1], grid.get_grid(hvals),
                    norm=colors.LogNorm(vmin=0.01, vmax=vmax),
                    cmap=gradient_cmap(mcolor), zorder=3, alpha=.9)
            print k, j
            print len(data_snaps[k][j][0])
            data_pos[k][0] += list(data_snaps[k][j][0][0])
            data_pos[k][1] += list(data_snaps[k][j][0][1])
            bmap.scatter(
                data_pos[k][0], data_pos[k][1], msize, marker='o', lw=.25,
                facecolor='r', edgecolor='w', zorder=4, alpha=alpha,
                antialiased=True)
            data_neg[k][0] += list(data_snaps[k][j][1][0])
            data_neg[k][1] += list(data_snaps[k][j][1][1])
            bmap.scatter(
                data_neg[k][0], data_neg[k][1], msize, marker='o', lw=.25,
                facecolor='b', edgecolor='w', zorder=3, alpha=alpha,
                antialiased=True)
    gs.tight_layout(fig, w_pad=0.1, h_pad=0.0)
    fig.savefig(outfile, dpi=dpi)
    plt.clf()


def make_sim_yearmaps(
        yrs, sim_snaps, data_snaps, grid, region, const, outfile,
        vmax=None, hvals=None, hidx=None, bmap=None, labels=None, dpi=None,
        figsize=None, msize=5, mcolor=primary_colors(), alpha=.9, fontsize=12,
        data_labels=None, shade='bright', dataProj=None):
    n_data = len(data_snaps)
    n_cols = len(yrs)
    n_rows = len(sim_snaps) + n_data
    assert len(sim_snaps[0]) == n_cols, (n_cols, len(sim_snaps[0]))
    assert len(data_snaps[0]) == n_cols, (n_cols, len(data_snaps[0]))
    labels = ['' for i in xrange(n_rows - 1)] if labels is None else labels
    data_labels = ['Data' for k in xrange(n_data)]\
        if data_labels is None else data_labels
    labels += data_labels
    figsize = (10, 5.5) if figsize is None else figsize
    dpi = 200 if dpi is None else dpi
    if vmax is None:
        vmax = hvals.max()
    #
    for j in xrange(n_cols):
        fig = plt.figure(figsize=figsize)
        gs = gridspec.GridSpec(1, n_rows)
        ax = []
        for i in xrange(n_rows):
            ax += [plt.subplot(gs[0, i], facecolor='w', frameon=False)]
        if bmap is None:
            pc = None
            edges = grid.edges
            bmap = plt
        else:
            x0, y0 = grid.edges[0][0], grid.edges[1][-1]
            bproj = pyproj.Proj(bmap.proj4string)
            assert np.all(bmap(x0, y0) == bproj(x0, y0))
            pc = make_patches(const.shpfile[region], bmap, shade=shade)
            edges = grid.project_edges(bproj, dataProj)
        data_pos = [[[], []] for k in xrange(n_data)]
        data_neg = [[[], []] for k in xrange(n_data)]
        # simulations
        for i in xrange(n_rows - n_data):
            if i == 0:
                ax[i].set_title(yrs[j], fontsize=fontsize)
            if j == 0:
                ax[i].set_ylabel(labels[i], fontsize=fontsize)
            if pc is not None:
                ax[i].add_collection(pc)
            if hidx is not None:
                bmap.pcolormesh(
                    edges[0], edges[1], grid.get_grid(hidx),
                    norm=colors.LogNorm(vmin=0.01, vmax=1), ax=ax[i],
                    cmap=gradient_cmap(backgd_colors()), zorder=2, alpha=.9)
            bmap.pcolormesh(
                edges[0], edges[1], grid.get_grid(sim_snaps[i][j]),
                norm=colors.LogNorm(vmin=0.01, vmax=1), ax=ax[i],
                cmap=gradient_cmap(primary_colors()), zorder=2, alpha=.9)
        # data - the last rows
        for k in xrange(n_data):
            if data_snaps[k][j] is None:
                ax[k-n_data].axis('off')
                continue
            if pc is not None:
                ax[k-n_data].add_collection(pc)
            if hidx is not None:
                bmap.pcolormesh(
                    edges[0], edges[1], grid.get_grid(hidx),
                    norm=colors.LogNorm(vmin=0.01, vmax=1), ax=ax[k-n_data],
                    cmap=gradient_cmap(backgd_colors()), zorder=2, alpha=.9)
            if hvals is not None:
                bmap.pcolormesh(
                    edges[0], edges[1], grid.get_grid(hvals),
                    norm=colors.LogNorm(vmin=0.01, vmax=vmax),
                    ax=ax[k-n_data], cmap=gradient_cmap(mcolor),
                    zorder=3, alpha=.9)
            data_pos[k][0] += list(data_snaps[k][j][0][0])
            data_pos[k][1] += list(data_snaps[k][j][0][1])
            bmap.scatter(
                data_pos[k][0], data_pos[k][1], msize, marker='o', lw=.25,
                facecolor='r', edgecolor='w', zorder=4, alpha=alpha,
                antialiased=True, ax=ax[k-n_data])
            data_neg[k][0] += list(data_snaps[k][j][1][0])
            data_neg[k][1] += list(data_snaps[k][j][1][1])
            bmap.scatter(
                data_neg[k][0], data_neg[k][1], msize, marker='o', lw=.25,
                facecolor='b', edgecolor='w', zorder=3, alpha=alpha,
                antialiased=True, ax=ax[k-n_data])
        gs.tight_layout(fig, w_pad=0.1, h_pad=0.0)
        fig.savefig(outfile + '_{}.png'.format(j), dpi=dpi)
        plt.clf()


def make_classmap(nclass, vals, grid, region, const, outfile,
                  hdens=None, bmap=None, labels=None):
    idx = (vals>0)
    breaks = Natural_Breaks(vals[idx], k=nclass)
    jbins = np.zeros_like(vals); jbins[idx] = breaks.yb + 1
    H = grid.get_grid(jbins)
    if labels is None:
        labels = ['<= {} infections ({} cells)'.format(b, c) for b, c in zip(breaks.bins, breaks.counts)]
    #
    fig = plt.figure(figsize=(10, 5.5))
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)
    if bmap is None:
        pc = make_patches(const.projected_shpfile[region])
        edges = grid.edges
        bmap = plt
        remove_tick_marks(ax)
    else:
        x0, y0 = grid.edges[0][0], grid.edges[1][-1]
        bproj = pyproj.Proj(bmap.proj4string)
        assert np.all(bmap(x0, y0)==bproj(x0, y0))
        pc = make_patches(const.shpfile[region], bmap)
        edges = grid.project_edges(bproj)
    if hdens is not None:
        bmap.pcolormesh(edges[0], edges[1], grid.get_grid(hdens),
                        norm=colors.LogNorm(vmin=0.01, vmax=1),
                        cmap=gradient_cmap(backgd_colors()), zorder=3, alpha=.9)
    bmap.pcolormesh(edges[0], edges[1], H,
                    cmap=gradient_cmap(primary_colors()), zorder=4, alpha=.9)
    cb = create_discrete_colorbar(nclass, cmap=gradient_cmap(primary_colors()),
                                  labels=labels, shrink=0.5)
    cb.ax.tick_params(labelsize=6)
    ax.add_collection(pc)
    fig.savefig(outfile, dpi=200)


def make_inset_heatmap(
        vals, grid, regions, const, outfile, vmax=None, vmin=0.01,
        hidx=None, figsize=None, dpi=None, mcolor=primary_colors(),
        shade='bright', dataProj=None):
    figsize = (10, 5.5) if figsize is None else figsize
    dpi = 200 if dpi is None else dpi
    vmin = vals.min() if vmin is None else vmin
    vmax = vals.max() if vmax is None else vmax
    fig = plt.figure(figsize=figsize)
    # external map
    ax = fig.add_subplot(111, facecolor='w', frame_on=False)
    bmap0 = const.bmap(regions[0])
    x0, y0 = grid.edges[0][0], grid.edges[1][-1]
    print (x0, y0)
    pc0 = make_patches(const.shpfile[regions[0]], bmap0, shade=shade)
    bproj0 = pyproj.Proj(bmap0.proj4string)
    edges0 = grid.project_edges(bproj0, dataProj)
    if hidx is not None:
        bmap0.pcolormesh(edges0[0], edges0[1], grid.get_grid(hidx),
                         norm=colors.LogNorm(vmin=vmin, vmax=vmax),
                         cmap=gradient_cmap(backgd_colors()),
                         zorder=3, alpha=.9)
    bmap0.pcolormesh(edges0[0], edges0[1], grid.get_grid(vals),
                     norm=colors.LogNorm(vmin=vmin, vmax=vmax),
                     cmap=gradient_cmap(mcolor), zorder=4, alpha=.9)

    ax.add_collection(pc0)
    # internal map
    axin = zoomed_inset_axes(ax, 12, loc=4)
    '''lon0, lat0, lonF, latF = const.bounds[regions[1]]
    x0, y0 = bmap0(lon0, lat0)
    xF, yF = bmap0(lonF, latF)
    axin.set_xlim(x0, xF)
    axin.set_ylim(y0, yF)'''
    pc1 = make_patches(const.shpfile[regions[1]], bmap0, shade=shade)
    plt.pcolormesh(edges0[0], edges0[1], grid.get_grid(vals),
                   norm=colors.LogNorm(vmin=vmin, vmax=vmax),
                   cmap=gradient_cmap(mcolor), zorder=4, alpha=.9)
    axin.add_collection(pc1)
    plt.xticks(visible=False)
    plt.yticks(visible=False)
    cb = plt.colorbar(shrink=0.4, format='%.2g')
    cb.ax.tick_params(labelsize=8)
    if vmax == 1 and vmin == 0.01:
        cb.locator = ticker.FixedLocator([.01, .1, 1])
    else:
        cb.locator = ticker.FixedLocator([vmin, vmax])
    cb.update_ticks()
    mark_inset(ax, axin, loc1=1, loc2=2) # , fc="none", ec="0.5")
    #
    fig.savefig(outfile, dpi=dpi)
    plt.clf()
