from model.dispersal import exp_sfn, cau_sfn, uni_sfn, cau_adj_sfn
from model.dispersal import exponential_fn, cauchy_fn
fkernel = {
    'exp': exp_sfn,
    'pow': cau_sfn,
    'pow2': cau_adj_sfn,
    'uni': uni_sfn
}
fkernel_fn = {
    'exp': exponential_fn,
    'pow': cauchy_fn,
}
