import numpy as np
import numpy.random as nr

"""
Methods for landscape generation:
 - hosts are generated as points from a spatial Poisson process
 - intensity function of the Poissson process defines type of landscape:
   + uniform landscape: constant intensity
   + structured landscape: intensity ~ Gaussian process
"""


"""
Uniform landscape within a rectangle of n_row * n_col square cells
 1. Generate points from a constant-intensity Poisson process
"""
n_row, n_col, csize = 2, 3, 10
print nr.poisson(1, (n_row, n_col))
