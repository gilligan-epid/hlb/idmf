import util

import numpy as np
import scipy.stats as sp
import numpy.random as nr

tol = 1e-5


class DiscreteSurvey(object):
    get_data_fn = {
        'Noulli': util.get_noulli_data,
        'Nomial': util.get_nomial_data,
        'Vector': util.get_vector_data,
    }

    def __init__(self, data_list, model_list, pop, pos_inds):
        self.data = data_list
        self.models = model_list
        self.pop = pop
        self.pos_inds = pos_inds  # from primary data only
        #
        self.n_source = len(self.data)
        assert len(self.models) == self.n_source
        #
        for model in self.models:
            assert model in self.get_data_fn.keys()

    @classmethod
    def from_df(cls, df_list, model_list, pop, dt, fields):
        print "fields", fields
        assert len(df_list) == len(model_list)
        data_list, pos_inds = [], []
        for i in xrange(len(df_list)):
            data, pinds = cls.get_data_fn[model_list[i]](
                df_list[i], pop, dt, fields)
            data_list += [data]
            if i == 0:
                pos_inds = np.array(pinds).astype(int)
        return cls(data_list, model_list, pop, pos_inds)

    def get_nodedata(self, gi):
        # data objects for node gi
        dlist = []
        for i in xrange(self.n_source):
            if self.models[i] == 'Vector':
                continue
            dlist += [NodeData(self.data[i][gi], self.models[i], gi)]
        return dlist

    def get_vector_data(self):
        out = []
        for i in xrange(self.n_source):
            if self.models[i] == 'Vector':
                out += [self.data[i]]
        return out


class ContinuousSurvey(DiscreteSurvey):
    pass


class NodeData(object):
    def __init__(self, traj, model, gi):
        self.traj = traj
        self.model = model
        self.gi = gi

    def t_1st_pos(self):
        Dt, Ds = self.traj[:2, :]
        # print "data", zip(Dt, Ds)
        if Ds.sum() == 0:
            return None
        else:
            return Dt[np.argmax(Ds > 0)]

    def set_probs(self, pstates, dprobs, pop=None):
        probs = dprobs * (pstates > 0)
        probs[:, 1:] += tol
        psum = probs.sum(1)
        assert np.all(psum <= 1), psum
        # probs.shape = (n_state, n_dprob + 1)
        self.probs = np.append(probs, 1 - psum[:, None], axis=1)
        self.pstates = pstates

    def segment_logp(self, it, sind, t, tp):
        if it is None:
            logp = 0
        elif self.model == 'Noulli':
            # param = per-site detection probabilities
            logp = np.log(self.probs[sind, self.traj[1:, it] > 0])
        elif self.model == 'Nomial':
            # param = per-host detection probabilities
            # detection probability < 1 accounts for false negatives
            n_counts = self.traj[1:, it]
            n_trial = n_counts.sum()
            logp = sp.multinomial.logpmf(
                n_counts, n=n_trial, p=self.probs[sind, :])
        return logp

    def segment_logp_space(self, it, t, tp):
        if it is None:
            logp = np.zeros_like(self.pstates)
        elif self.model == 'Noulli':
            logp = np.log(self.probs[:, self.traj[1:, it] > 0])
            logp.shape = self.pstates.shape
        elif self.model == 'Nomial':
            # param = per-host detection probabilities
            # detection probability < 1 accounts for false negatives
            n_counts = self.traj[1:, it]
            n_trial = n_counts.sum()
            logp = sp.multinomial.logpmf(n_counts, n=n_trial, p=self.probs)
            logp.shape = self.pstates.shape
        return logp

    def sample(self, I, tt, dprobs, snap_times):
        n_snaps = len(snap_times)
        assert self.model == 'Noulli', 'model not implemented yet'
        Dt = self.traj[0, :]
        r = nr.rand(len(Dt))
        tau = tt[1] - tt[0]
        D, dcounts = np.zeros_like(I), np.zeros(len(snap_times) + 1)
        k = 0
        for j in xrange(len(Dt)):
            t = Dt[j]
            if k < n_snaps and t > snap_times[k]:
                k += 1
            if t <= tt[0] or t >= tt[-1]:
                continue
            it = int(t - tt[0] / tau) + 1
            npos = int(r[j] < dprobs[0] * I[it])
            D[it] = npos
            dcounts[k:] += npos
        return D.cumsum().clip(max=1), dcounts
