import numpy as np
import numpy.random as nr
import copy


def read_header(fname, comment, n_header):
    lc = len(comment)
    with open(fname, 'r') as f:
        header = {}
        for i in xrange(n_header):
            a, b = f.readline()[lc:].split()
            header[a] = b
    x0, y0 = float(header['xllcorner']), float(header['yllcorner'])
    csize = float(header['cellsize'])
    ncols, nrows = int(header['ncols']), int(header['nrows'])
    xF, yF = x0 + ncols * csize, y0 + nrows * csize
    xbins = np.arange(x0 + 0.5 * csize, xF, csize)
    assert len(xbins) == ncols
    ybins = np.arange(yF - 0.5 * csize, y0, -csize)
    assert len(ybins) == nrows
    assert xbins[0] == x0 + .5 * csize and ybins[-1] == y0 + .5 * csize
    assert xbins[-1] == xF - .5 * csize and ybins[0] == yF - .5 * csize
    return xbins, ybins


def get_hinf(df, grid, t=None, hidx=None):
    if t is not None:
        df1 = copy.deepcopy(df[df['DayNo'] <= t])
    else:
        df1 = copy.deepcopy(df)
    df2 = df1.groupby(['Xi', 'Yi'], as_index=False).count()
    H = np.zeros(grid.shape)
    H[df2.Xi, df2.Yi] = df2['DayNo'].values
    hinf = H[grid.ixy[:, 0], grid.ixy[:, 1]]
    if hidx is not None:
        hinf = (hinf / hidx) * 100
    return hinf


def get_I0(df, grid, t0):
    " Get the number of confirmed infections before t0 "
    out = np.zeros(grid.n_pos)
    df = df[df['DayNo'] < t0]
    for i, row in df.iterrows():
        out[row['Gi']] += row['Positive']
    return out


def get_noulli_data(df, pop, dt, fields):
    # e.g. fields = ['PosNo', 'IncNo', 'SampNo']
    assert fields[0] == 'PosNo'
    df = df.sort_values('TauNo', ascending=True)
    nF = 1 + len(fields)
    out, pinds = [np.empty((nF, 0)) for i in xrange(pop.n_pop)], []
    for key, group in df.groupby('Gi'):
        nT = len(group)  # no. of obs for site Gi
        traj = np.empty((nF, nT))
        traj[0, :] = group.TauNo.values  # obs times
        for i, f in enumerate(fields):
            traj[i + 1, :] = (group[f].values > 0).astype(int) -\
                            traj[1:i+1, :].sum(0)
        assert np.all(traj[1:, :].sum(0) == 1), traj[1:, :].sum(0)
        assert np.all(traj[1:, :] >= 0)
        out[key] = traj
        if traj[1, :].sum() > 0:
            pinds += [int(key)]
    return out, pinds


def get_nomial_data(df, pop, dt, fields):
    assert fields[0] == 'PosNo'
    df = df.sort_values('TauNo', ascending=True)
    nF = 1 + len(fields)
    out, pinds = [np.empty((nF, 0)) for i in xrange(pop.n_pop)], []
    for key, group in df.groupby('Gi'):
        nT = len(group)
        traj = np.empty((nF, nT))
        traj[0, :] = group.TauNo.values
        for i, f in enumerate(fields):
            traj[i + 1, :] = group[f].values - traj[1:i+1, :].sum(0)
        assert np.all(traj[1:, :] >= 0)
        out[key] = traj
        if traj[1, :].sum() > 0:
            pinds += [int(key)]
    return out, pinds


def get_vector_data(df, pop, dt, fields):
    assert fields[0] == 'PosNo'
    nT = len(df)
    nF = 1 + len(fields)
    #
    df['TauNo'] = df.TauNo.values + nr.rand(nT)
    df = df.sort_values('TauNo', ascending=True)
    inds = df.Gi.values
    trajs = np.empty((nF, nT))
    trajs[0, :] = df.TauNo.values
    for i, f in enumerate(fields):
        trajs[i + 1, :] = (df[f].values > 0).astype(int) -\
                        trajs[1:i+1, :].sum(0)
    assert np.all(trajs[1:, :].sum(0) == 1), trajs[1:, :].sum(0)
    assert np.all(trajs[1:, :] >= 0)
    return [inds, trajs], None


def get_direct_data(df, pop, dt):
    'direct observations of the chain at discrete times'
    s0 = (df.DayNo < pop.tvec[0]).sum()
    df = df[(df.DayNo >= pop.tvec[0]) * (df.DayNo < pop.tvec[-1])]
    T = [[] for i in xrange(pop.n_pop)]
    S = copy.deepcopy(T)
    for key, group in df.groupby('Gi'):
        days = group.DayNo.values
        nT = len(days)
        T[key] = np.sort(days + nr.rand(nT) * dt)
        S[key] = s0 + 1 + np.arange(nT)
    return T, S


def get_binomial_data(df, pop, dt):
    'multiple trials of leaf sampling per site at a given time'
    assert False, "to correct"
    df = df[(df.DayNo >= pop.tvec[0]) & (df.DayNo < pop.tvec[-1])]
    T = [[] for i in xrange(pop.n_pop)]
    S = copy.deepcopy(T)
    for key, group in df.groupby('Gi'):
        days = group.DayNo.unique()
        n_trial = [(group.DayNo == d).sum() for d in days]
        n_pos = [((group.DayNo == d) * (group.Positive)).sum() for d in days]
        days += nr.rand(len(days)) * dt
        idx = np.argsort(days)
        T[key] = days[idx]
        S[key] = np.vstack([n_pos[idx], n_trial[idx]])
    return T, S


def get_gaussian_data(df, pop, dt):
    'multiple trials, with diagnostic values available'
    df = df[(df.DayNo >= pop.tvec[0]) & (df.DayNo < pop.tvec[-1])]
    T = [[] for i in xrange(pop.n_pop)]
    S = copy.deepcopy(T)
    for key, group in df.groupby('Gi'):
        days = group.DayNo.unique()
        T[key] = days + nr.rand(len(days)) * dt
        idx = np.argsort(T[key])
        T[key] = T[key][idx]
        for d in days[idx]:
            S[key] += [group[group.DayNo == d].DiagVal.values]
    return T, S


def get_survival_data(df, pop, dt):
    'continuous observation, e.g. on-demand detection'
    df = df[(df.DayNo >= pop.tvec[0]) & (df.DayNo < pop.tvec[-1])]
    T = [[] for i in xrange(pop.n_pop)]
    for key, group in df.groupby('Gi'):
        days = group.DayNo.values
        T[key] = np.sort(days + nr.rand(len(days)) * dt)
    return T, None


def get_poisson_data(df, pop, dt):
    pass





def check_binomial_data(df_pos, df_all, grid):
    df_pos = df_pos[df_pos['Location'] == 'Residential']
    df_pos = df_pos[['DayNo', 'Gi', 'Positive']]
    df_all = df_all[['DayNo', 'Gi', 'Positive']]
    df = df_pos.append(df_all)
    # Load data
    tvec = [[] for i in xrange(grid.n_pos)]
    svec = copy.deepcopy(tvec)
    n_site = 0
    for key, group in df.groupby('Gi'):
        days = np.sort(group.DayNo.unique())
        n_trial = np.array([((group.DayNo == d) &
                             (not group.Positive)).sum() for d in days])
        n_pos = np.array([((group.DayNo == d) &
                           (group.Positive)).sum() for d in days])
        tvec[key] = days
        svec[key] = np.vstack([n_pos, n_trial])
        if not np.all(n_pos <= n_trial):
            n_site += 1
            idx = np.nonzero([n_pos[i] > n_trial[i]
                              for i in xrange(len(days))])[0]
            print idx
            print key, zip(idx, days[idx], n_pos[idx], n_trial[idx])
            break
    print n_site
    return tvec, svec
