import idm.model.multi_resol_struct as mr_struct
from idm.model import struct

import numpy as np
import numpy.random as nr

name = "SECI"
n_comp, iI = 3, 2
compartments = ['Exposed', 'Cryptic', 'Symptomatic']
param_list = [
    'epsilon', 'beta', 'alpha', 'mu', 'varepsilon',  # transmission params (S->E)
    'eta',    # control efficiency
    'gamma',  # rate of onset of infectiousness (E->C)
    'sigma',  # rate of emergence of symptoms (C->I)
    'dprob',  # detection probability per infected-unit-host
    'drate',  # rate of detection
]
rupdate = [
    np.array([1, 0, 0]),   # Exposed
    np.array([-1, 1, 0]),  # Infectious
    np.array([0, -1, 1]),  # Symptomatic
]


class PriorModel(struct.PopModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def get_rate(self, state, it, param_update):
        E0, C0, I0 = state[0]
        E1, C1, I1 = state[1]
        p0, p1 = E0 + C0 + I0, E1 + C1 + I1
        assert np.all(p0 <= self.pop.hidx[0]), zip(p0, self.pop.hidx[0])
        assert np.all(p1 <= self.pop.hidx[1]), zip(p1, self.pop.hidx[1])
        if param_update:
            # expand to coarse space-time construct only
            self.eps = self.params['epsilon'].expand_by_space(it, self.pop)
            self.vareps = self.params['varepsilon'].expand_by_space(
                it, self.pop)
            self.beta = self.params['beta'].expand_by_space(it, self.pop)
            self.gamma = self.params['gamma'].expand_by_space(it, self.pop)
            self.sigma = self.params['sigma'].expand_by_space(it, self.pop)
        #
        # coarse rates
        Cf = self.pop.covs[1][:, it] * (C0 + I0)
        erate = self.eps + self.pop.covs[0][:, it] * (
            self.vareps + self.beta * Cf * self.pop.Ksp[0])
        Erate0 = (self.pop.hidx[0] - p0) * erate
        Crate0 = E0 * self.gamma
        Irate0 = C0 * self.sigma
        # fine rates
        Cf = self.pop.hdens[1] * (C1 + I1)
        erate = self.eps + self.pop.hdens[1] * (
            self.vareps + self.beta * Cf * self.pop.Ksp[1])
        Erate1 = (self.pop.hidx[1] - p1) * erate
        Crate1 = E1 * self.gamma  # TODO: a different gamma?
        Irate1 = C1 * self.sigma  # TODO: a different sigma?
        # transfer mixed-cell rates to fine rates
        # TODO: could speed this up?
        for i in self.mixed_inds:
            cinds = self.pop.grid[0].cells[i].cinds
            Erate1[cinds] += Erate0[i]
            Erate0[i], Crate0[i], Irate0[i] = 0, 0, 0
        #
        self.cErate = [Erate0.cumsum(), Erate1.cumsum()]
        self.cCrate = [Crate0.cumsum(), Crate1.cumsum()]
        self.cIrate = [Irate0.cumsum(), Irate1.cumsum()]
        return np.cumsum([
            self.cErate[0][-1], self.cCrate[0][-1], self.cIrate[0][-1],
            self.cErate[1][-1], self.cCrate[1][-1], self.cIrate[1][-1]])

    def update_state(self, state, crate, t):
        ei = np.argmax(crate > nr.rand() * crate[-1])
        if ei == 0:
            # Coarse S->E to coarse-only cells only (other rates set to 0)
            ind = np.argmax(self.cErate[0] > nr.rand() * self.cErate[0][-1])
        elif ei == 1:
            # Coarse E->C to coarse-only cells only (other rates set to 0)
            ind = np.argmax(self.cCrate[0] > nr.rand() * self.cCrate[0][-1])
        elif ei == 2:
            # Coarse C->I to coarse-only cells only (other rates set to 0)
            ind = np.argmax(self.cIrate[0] > nr.rand() * self.cIrate[0][-1])
        elif ei == 3:
            # Fine S->E to coarse E cells
            ind = np.argmax(self.cErate[1] > nr.rand() * self.cErate[1][-1])
        elif ei == 4:
            # Fine E->C to mixed E/C/I cells
            ind = np.argmax(self.cCrate[1] > nr.rand() * self.cCrate[1][-1])
        elif ei == 5:
            # Fine C->I to mixed E/C/I cells
            ind = np.argmax(self.cIrate[1] > nr.rand() * self.cIrate[1][-1])
        #
        if ei < 3:
            state[0][:, ind] += self.rupdate[ei]
        else:
            state[1][:, ind] += self.rupdate[ei - 3]
        return state, [ind, ei]
