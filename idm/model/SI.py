from idm.model import struct, util, hyper

import numpy as np
import numpy.random as nr
from scipy.linalg import expm

name = "SI"
n_comp, iI = 1, 0
compartments = ['Infected']
param_list = [
    'epsilon', 'beta', 'alpha', 'mu', 'varepsilon',  # transmission params (S->I)
    'dprob',  # detection probability per infected-unit-host
    'drate',  # reverse detection lag (NOT used for simulation)
]
rupdate = [
    np.array([1]),  # Infected
]


class PriorModel(struct.PopModel):
    def setup(self, ):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def get_rate(self, state, it):
        """
            I = INSTANT number of infectives
            it = transition period index
        """
        I = state[0]
        assert np.all(I <= self.pop.hidx), zip(I, self.pop.hidx)
        eps = self.params['epsilon'].expand_by_space(it, self.pop)
        vareps = self.params['varepsilon'].expand_by_space(it, self.pop)
        beta = self.params['beta'].expand_by_space(it, self.pop)
        #
        If = self.pop.covs[1][:, it] * I
        irate = eps + self.pop.covs[0][:, it] * (
            vareps + beta * If * self.pop.Ksp)
        Irate = (self.pop.hidx - I) * irate
        self.cIrate = Irate.cumsum()
        #
        return np.array([self.cIrate[-1]])

    def update_state(self, state, crate):
        'Infection: S->I'
        ei = 0
        ind = np.argmax(self.cIrate > nr.rand() * self.cIrate[-1])
        state[:, ind] += self.rupdate[ei]
        return state, [ind, ei]


class PosteriorModel(struct.PopPostModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def build_nodes(self, init_trajs):
        """
        init_trajs: initialisation of trajectories = [Iinit]
          Iinit[j][0, i]: transition time i at site j
          Iinit[j][1, i]: number of cumulative infection at time t_i
        """
        if len(init_trajs) == 1:
            Iinit = init_trajs[0]
        else:
            assert False, len(init_trajs)
        #
        Inodes = []
        Dnodes = []
        data_nodes = []
        for j in xrange(self.pop.n_pop):
            data = self.data.get_nodedata(j)  # list
            data_nodes += [data[0]]
            smax = Iinit[j][1][-1] if self.opts.fsize_known else None
            # if smax > 0:
            #     print j, data.S[-1]
            #
            Iprior = INode(self.pop, j, smax, data, None)
            Ipi = np.zeros(Iprior.n_state)
            Ipi[int(Iinit[j][1][0])] = 1
            Inodes += [hyper.Trajectory(Iinit[j], Iprior, Ipi)]
            #
            dnode = data_nodes[j]
            if (dnode.traj[1] > 0).sum() == 0:
                t1 = self.pop.tvec[-1] + 1
            else:
                t1 = dnode.traj[0][np.argmax(dnode.traj[1] > 0)]
            if t1 < self.pop.tvec[0]:
                Dt = self.pop.tvec[[0, -1]]
                Ds = np.ones(2)
            elif t1 < self.pop.tvec[-1]:
                Dt = np.array([self.pop.tvec[0], t1, self.pop.tvec[-1]])
                Ds = np.array([0, 1, 1])
            else:
                Dt = self.pop.tvec[[0, -1]]
                Ds = np.zeros(2)
            Dnodes += [hyper.Trajectory(np.vstack([Dt, Ds]), None, None)]
        self.nodes = {'Infected': Inodes}
        self.Dnodes = Dnodes
        self.data_nodes = data_nodes
        for j in xrange(self.pop.n_pop):
            Inodes[j].prior.set_blanket(self.nodes)

    def joint_logl(self, alpha=None, mu=None, eps=None, vareps=None):
        if alpha is None and mu is None:
            Ksp, Kfull, Kloc = self.pop.Ksp, self.pop.Kfull, self.pop.Kloc
        elif mu is None:
            Ksp, Kfull, Kloc = self.pop.compute_kernel(
                alpha, self.params['mu'].value, self.opts.dkernel,
                self.opts.freq_depend, self.with_Kfull, self.opts.knorm)
        elif alpha is None:
            Ksp, Kfull, Kloc = self.pop.compute_kernel(
                None, mu, self.opts.dkernel,
                self.opts.freq_depend, self.with_Kfull, self.opts.knorm)
        if eps is None:
            eps = self.params['epsilon'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(eps):
            eps = eps[:, self.pop.base[1]][self.pop.base[0], :]
        if vareps is None:
            vareps = self.params['varepsilon'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(vareps):
            vareps = vareps[:, self.pop.base[1]][self.pop.base[0], :]
        #
        nodes = self.nodes['Infected']
        n = self.pop.n_pop
        eps = np.ones((n, self.pop.n_dt)) * eps if np.isscalar(eps)\
            else eps
        vareps = np.ones((n, self.pop.n_dt)) * vareps if np.isscalar(vareps)\
            else vareps
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((n, self.pop.n_dt)) * beta if np.isscalar(beta)\
            else beta

        def rate_fn(inp, i, save):
            I, it = inp[:n], int(inp[-1])
            assert np.all(self.pop.hidx >= I), [it, zip(self.pop.hidx, I)]

            def full_update():
                If = self.pop.covs[1][:, it] * I
                iprop = If * Ksp
                # per-capita infection rate at all sites
                irate = eps[:, it] + self.pop.covs[0][:, it] * (
                    vareps[:, it] + beta[:, it] * iprop)
                # sum of all rates
                rsum = ((self.pop.hidx - I) * irate).sum()
                save = [irate, rsum]
                return save

            if i is None:
                rind = None
                save = full_update()
            elif i < n:
                'I[i]: i -> i+1'
                # rate at which an individual at site i gets infected
                rind = save[0][i]
                # changes due to the event
                # to i's population
                dirate = self.pop.covs[0][i, it] * beta[i, it] *\
                    self.pop.covs[1][i, it]
                save[-1] += (self.pop.hidx[i] - I[i]) * dirate - save[0][i]
                save[0][i] += dirate
                # to neighbouring populations
                inds = Kfull.neighbors[i]
                dirate = self.pop.covs[0][inds, it] * beta[inds, it] *\
                    self.pop.covs[1][i, it] * Kfull.weights[i]
                save[-1] += ((self.pop.hidx[inds] - I[inds]) * dirate).sum()
                save[0][inds] += dirate
            elif i == n:
                'Environment dynamics'
                rind = None
                save = full_update()
            else:
                assert False, "Unexpected event"
            return rind, save, save[-1]
        logl = util.explogp_merge(rate_fn, nodes,
                                  [self.pop.tvec, np.arange(self.pop.n_dt)])
        # print "Without data", logl
        # Data likelihood
        for i in xrange(self.pop.n_pop):
            Inode = self.nodes['Infected'][i]
            data_list = Inode.prior.data
            for data in data_list:
                Dt = data.traj[0, :]

                def logp_fn(inp, i, t, tp):
                    r, it = inp
                    ri = Inode.prior.encode_state(r)
                    if i == 1:
                        # data observed
                        return data.segment_logp(it, ri, t, tp)
                    else:
                        return data.segment_logp(None, ri, t, tp)
                logl += util.data_merge(
                    logp_fn, [Inode], self.pop.tvec[[0, -1]],
                    [Dt, np.arange(len(Dt)) + 1])[0]
            if not np.isfinite(logl):
                break
        if alpha is not None or mu is not None:
            return logl, (Ksp, Kfull, Kloc)
        else:
            return logl

    def marginal_logl(self, beta=None):
        assert len(self.nodes['Infected']) == 1
        beta = self.params['beta'].value if beta is None else beta
        node = self.nodes['Infected'][0]
        Q, qtvec = util.make_generator_matrix(node.prior, self.params)
        assert len(qtvec) == 2, len(qtvec)
        Q = Q[:, :, 0]
        #
        data = node.prior.data
        assert data.model == 'Binomial' and data.param == 1
        logl = 0
        tau0 = data.T[0] - self.pop.tvec[0]
        assert tau0 >= 0
        if tau0 > 0:
            p0 = expm(Q * tau0)
            logl += np.inner(node.pi, p0[:, data.S[0]])
        for i in xrange(data.n - 1):
            prob = expm(Q * (data.T[i+1] - data.T[i]))
            logl += prob[data.S[i], data.S[i+1]]
        return logl

    def beta_gibbs(self):
        nodes = self.nodes['Infected']
        n = self.pop.n_pop
        eps = self.params['epsilon'].expand_by_time(self.pop.base[0], self.pop)
        eps = np.ones((self.pop.n_pop, self.pop.n_dt)) * eps\
            if np.isscalar(eps) else eps
        vareps = self.params['varepsilon'].expand_by_time(
            self.pop.base[0], self.pop)
        vareps = np.ones((self.pop.n_pop, self.pop.n_dt)) * vareps\
            if np.isscalar(vareps) else vareps
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((self.pop.n_pop, self.pop.n_dt)) * beta\
            if np.isscalar(beta) else beta

        def rate_fn(inp, i, save):
            I, it = inp[:n], int(inp[-1])
            assert np.all(self.pop.hidx >= I), [it, zip(self.pop.hidx, I)]

            def full_update():
                If = self.pop.covs[1][:, it] * I
                # per-capita secondary infection propensity
                iprop = If * self.pop.Ksp
                # per-capita secondary infection hazard at all sites
                ihazd = self.pop.covs[0][:, it] * iprop
                # sum of secondary infection hazards by spatial zones
                hsum = self.params['beta'].collapse_by_space(
                    (self.pop.hidx - I) * ihazd, self.pop)
                save = [ihazd, hsum]
                return save

            if i is None:
                event = None
                save = full_update()
            elif i < n:
                'I[i]: i -> i+1'
                # rate at which an individual at site i gets infected
                rate2nd = beta[i, it] * save[0][i]
                rate1st = eps[i, it] + self.pop.covs[0][i, it] * vareps[i, it]
                # primary vs. secondary infection
                secondary = False
                if nr.rand() < rate2nd / (rate1st + rate2nd):
                    secondary = True
                event = [secondary, rate1st, save[0][i], self.pop.base[0][i]]
                #
                # changes due to the event
                ds_all = np.zeros(self.pop.n_pop)
                # to i's population
                dhazd = self.pop.covs[0][i, it] * self.pop.covs[1][i, it]
                ds_all[i] = (self.pop.hidx[i] - I[i]) * dhazd
                save[0][i] += dhazd
                # to neighbouring populations
                inds = self.pop.Kfull.neighbors[i]
                dhazd = self.pop.covs[0][inds, it] * self.pop.covs[1][i, it] *\
                    self.pop.Kfull.weights[i]
                ds_all[inds] = (self.pop.hidx[inds] - I[inds]) * dhazd
                save[-1] += self.params['beta'].collapse_by_space(
                    ds_all, self.pop)
                save[0][inds] += dhazd
            elif i == n:
                'Environment dynamics'
                event = None
                save = full_update()
            else:
                assert False, "Unexpected event"
            return event, save, save[-1]
        if self.params['beta'].bins is None:
            shape, hagg, r1, h2 = util.beta_merge_no_bins(
                rate_fn, nodes, self.pop)
        else:
            shape, hagg, r1, h2, bit, biz = util.beta_merge_with_bins(
                rate_fn, nodes, self.pop)

        def get_betaval():
            beta = self.params['beta'].value * 1.
            if np.isscalar(beta):
                beta *= np.ones_like(hagg)
            return beta
        beta_old = get_betaval()
        a, b = self.params['beta'].prior
        # print a + shape, b + hagg
        self.params['beta'].update([a + shape, b + hagg])
        beta_new = get_betaval()
        if self.params['beta'].bins is None:
            dlogl = - hagg * (beta_new - beta_old) +\
                (np.log(r1 + h2 * beta_new) -
                 np.log(r1 + h2 * beta_old)).sum()
        else:
            dlogl = - (hagg * (beta_new - beta_old)).sum() +\
                (np.log(r1 + h2 * beta_new[bit][biz]) -
                 np.log(r1 + h2 * beta_old[bit][biz])).sum()
        return dlogl

    def drate_gibbs(self):
        nodes = self.nodes['Infected'] + self.Dnodes
        n = self.pop.n_pop

        def rate_fn(inp, i, hsum):
            C, D = inp[:n], inp[n:]  # C = Infected
            assert np.all(C >= D), np.argwhere(C < D)

            if i is None:
                # first call
                event = None
                hsum = C.sum()
            elif i < n:
                # I[i]: i -> i + 1
                event = None
                hsum += 1
            else:
                # D[j]: d -> d + 1
                event = 1
                hsum -= 1
            return event, hsum

        nevent, hagg = util.sigma_merge(rate_fn, nodes, self.pop)
        a, b = self.params['drate'].prior
        self.params['drate'].update([a + nevent, b + hagg])

    def epsilon_mh(self, logl, acc_rate):
        self.opts.tune['epsilon'] = util.tune(
            self.opts.tune['epsilon'], acc_rate)
        qa = self.opts.tune['epsilon'] * nr.randn()
        if self.params['epsilon'].ncells == 1:
            eps_q = self.params['epsilon'].value * np.exp(qa)
            logl_q = self.joint_logl(eps=eps_q)
            logp_q = logl_q + self.params['epsilon'].prior_logpdf(eps_q)
            logp = logl + self.params['epsilon'].prior_logpdf()
        else:
            je = nr.choice(2)
            eps_q = self.params['epsilon'].value * 1
            eps_q[je, :] = eps_q[je, :] * np.exp(qa)
            logl_q = self.joint_logl(eps=eps_q)
            logp_q = logl_q + self.params['epsilon'].prior_logpdf(eps_q)[je, 0]
            logp = logl + self.params['epsilon'].prior_logpdf()[je, 0]
        if logp_q - logp + qa > np.log(nr.rand()):
            self.params['epsilon'].set_val(eps_q)
            return True, logl_q, logp_q
        else:
            return False, logl, logp

    def varepsilon_mh(self, logl, acc_rate):
        self.opts.tune['varepsilon'] = util.tune(
            self.opts.tune['varepsilon'], acc_rate)
        qa = self.opts.tune['varepsilon'] * nr.randn()
        if self.params['varepsilon'].ncells == 1:
            eps_q = self.params['varepsilon'].value * np.exp(qa)
            logl_q = self.joint_logl(eps=eps_q)
            logp_q = logl_q + self.params['varepsilon'].prior_logpdf(eps_q)
            logp = logl + self.params['varepsilon'].prior_logpdf()
        else:
            je = nr.choice(2)
            eps_q = self.params['varepsilon'].value * 1
            eps_q[je, :] = eps_q[je, :] * np.exp(qa)
            logl_q = self.joint_logl(vareps=eps_q)
            logp_q = logl_q + self.params['varepsilon'].prior_logpdf(eps_q)[
                je, 0]
            logp = logl + self.params['varepsilon'].prior_logpdf()[je, 0]
        if logp_q - logp + qa > np.log(nr.rand()):
            self.params['varepsilon'].set_val(eps_q)
            return True, logl_q, logp_q
        else:
            return False, logl, logp


class INode(struct.NodePostModel):
    """
    Cumulative Infected
    """

    def get_genrate(self, params):
        """
        Rate at which the node increases its state by 1
        Parents = Inodes
        """
        pinds = self.pop.Kfull.neighbors[self.ind]
        parents = [self.nodes['Infected'][pinds[i]]
                   for i in xrange(len(pinds))]
        eps = params['epsilon'].expand_by_time(self.iz, self.pop)
        beta = params['beta'].expand_by_time(self.iz, self.pop)
        eps = np.ones(self.pop.n_dt) * eps if np.isscalar(eps) else eps
        beta = np.ones(self.pop.n_dt) * beta if np.isscalar(beta) else beta

        def rate_fn(inp):
            Ipar, it = inp[:-1], int(inp[-1])
            If_par = self.pop.covs[1][pinds, it] * Ipar
            If_this = self.pop.covs[0][self.ind, it] * self.space[:, 1:]
            irate = self.pop.covs[0][self.ind, it] * (eps[it] + beta[it] * (
                np.inner(self.pop.Kfull.weights[self.ind], If_par) +
                If_this) / self.pop.K0[self.ind, it])
            return (self.n_host - self.space[:, 1:]) * irate
        rates, tvec = util.rate_merge(
            rate_fn, parents, [self.pop.tvec, np.arange(self.pop.n_dt)])
        assert rates.shape == (self.n_state, len(tvec) - 1),\
            (rates.shape, (self.n_state, len(tvec) - 1))
        return [rates], tvec

    def get_outlogp(self, params, W):
        """
        Child = data
        Other Parents of Children = None
        TODO: How about other nodes?
        """
        def logp_fn(inp, i, t, tp):
            it = inp[0]
            if i == 0:
                # data observed
                return self.data.segment_logp(it, self.space[:, 1:], t, tp)
            else:
                return self.data.segment_logp(None, self.space[:, 1:], t, tp)
        logp = util.data_merge(
            logp_fn, [], W, [self.data.T, np.arange(self.data.n)])
        assert logp.shape == (self.n_state, len(W) - 1), logp.shape
        return logp
