

class StateSpaceError(Exception):
    pass


class DataError(Exception):
    pass


class TraceUpdateError(Exception):
    pass
