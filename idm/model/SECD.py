from idm.model import struct

import numpy as np
import numpy.random as nr

name = "SECD"
n_comp, iI = 3, 2
compartments = ['Exposed', 'Cryptic', 'Detectable']
param_list = [
    'epsilon', 'beta', 'alpha', 'mu', 'varepsilon',  # transmission params
    'eta',    # control efficiency
    'gamma',  # rate of onset of infectiousness (E->C)
    'drate',  # detection rate
    'dprob',  # detection probability
]
rupdate = [
    np.array([1, 0, 0]),   # Exposed
    np.array([-1, 1, 0]),  # Infectious
    np.array([0, -1, 1]),  # Symptomatic
]


class PriorModel(struct.PopModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.cov_wts = np.ones(self.pop.n_pop)

    def get_rate(self, state, it, param_update):
        """
            E, C, D = INSTANT number of exposeds, cryptics, detecteds,
            it = transition period index
        """
        E, C, D = state
        assert np.all(E + C + D <= self.pop.hidx), zip(
            E + C + D, self.pop.hidx)
        if param_update:
            self.eps = self.params['epsilon'].expand_by_space(it, self.pop)
            self.vareps = self.params['varepsilon'].expand_by_space(
                it, self.pop)
            self.beta = self.params['beta'].expand_by_space(it, self.pop)
            self.gamma = self.params['gamma'].expand_by_space(it, self.pop)
            self.drate = self.params['drate'].expand_by_space(it, self.pop)
            self.dprob = self.params['dprob'].value
        #
        Cf = self.cov_wts * self.pop.covs[1][:, it] * (C + D * self.dprob)
        e11 = self.eps * np.ones_like(self.pop.covs[0][:, it])
        e12 = self.cov_wts * self.pop.covs[0][:, it] * self.vareps
        e2 = self.cov_wts * self.pop.covs[0][:, it] * self.beta * \
            Cf * self.pop.Ksp
        Erate = (self.pop.hidx - E - C - D) * (e11 + e12 + e2)
        self.cErate = Erate.cumsum()
        #
        Crate = E * self.gamma * self.pop.covs[2][:, it]
        self.cCrate = Crate.cumsum()
        #
        Drate = C * self.drate
        self.cDrate = Drate.cumsum()
        #
        cumulative_rates = np.cumsum([
            self.cErate[-1], self.cCrate[-1], self.cDrate[-1]])
        econtrib = np.array([e11.sum(), e12.sum(), e2.sum()])
        return cumulative_rates, econtrib

    def update_state(self, state, crate, t):
        ei = np.argmax(crate > nr.rand() * crate[-1])
        if ei == 0:
            'Infection: S->E'
            ind = np.argmax(self.cErate > nr.rand() * self.cErate[-1])
        elif ei == 1:
            'Infectious: E->C'
            ind = np.argmax(self.cCrate > nr.rand() * self.cCrate[-1])
        elif ei == 2:
            'Symptomatic: C->D'
            ind = np.argmax(self.cDrate > nr.rand() * self.cDrate[-1])
            # quarantines
            for qr in self.quarantines:
                self.pop.Ksp = qr.update_K(ind, self.pop.Ksp)
            # treatment
            for tr in self.treatments:
                if tr.treset is not None and t > tr.treset:
                    # back to full infestation at treset
                    self.cov_wts = np.ones(self.pop.n_pop)
                    continue
                if tr.tstop is not None and t > tr.tstop:
                    # no further treatment after tstop
                    continue
                self.cov_wts = tr.update_wts(ind, self.cov_wts)
        state[:, ind] += self.rupdate[ei]
        return state, [ind, ei]


class PosteriorModel(struct.PopPostModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def build_nodes(self, init_trajs):
        pass
