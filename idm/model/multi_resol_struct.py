import idm.model.struct as struct


class Population(struct.Population):
    """
    Multi-resolution metapopulation of hosts. Class fiedls are:
    + tvec: vector of length (n_event + 2);
    + hdens: dictionary of host densities of different resolutions,
    e.g. hdens['coarse'], hdens['fine'];
    + grid: dictionary of spatial grid;
    + base: dictionary of space-time constructs;
    + covs: dictionary of space-time covariates;
    + dband: dictionary of distance thresholds;
    + sw: dictionary of spatial weight objects;
    + mixed_idx: indices of
    """
