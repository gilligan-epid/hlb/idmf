from idm.model import struct, util, hyper, SECI

import numpy as np
import numpy.random as nr
from scipy.stats import poisson

name = "SEIS_SIS"
n_comp, iI = 4, 2
compartments = ['Exposed', 'Infectious', 'Reservoir']
param_list = [
    'epsilon', 'beta', 'alpha', 'mu', 'varepsilon',  # transmission params
    'eta',    # control efficiency
    'gamma',  # rate of onset of infectiousness (E->C)
    'dprob',  # detection probability
    'rrate',  # reservoir decay rate
]
rupdate = [
    np.array([1, 0, 0]),   # main S->E
    np.array([-1, 1, 0]),  # main E->I
    np.array([0, -1, 0]),  # main I->S
    np.array([0, 0, 1]),   # reservoir S->I
    np.array([0, 0, -1]),  # reservoir I->S
]


class PriorModel(struct.PopModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate

    def get_rate(self, state, it, param_update):
        """
            E, I, R = INSTANT number of exposeds, infectious, spore reservoirs
            it = transition period index
            self.pop.covs[2] = summer survival suitability, 0 other time
        """
        E, I, R = state
        assert np.all(E + I <= self.pop.hidx), zip(E + I, self.pop.hidx)
        assert np.all(R <= self.pop.hidx), (R, self.pop.hidx)
        if param_update:
            self.eps = self.params['epsilon'].expand_by_space(it, self.pop)
            self.vareps = self.params['varepsilon'].expand_by_space(
                it, self.pop)
            self.beta = self.params['beta'].expand_by_space(it, self.pop)
            self.gamma = self.params['gamma'].expand_by_space(it, self.pop)
            self.rrate = self.params['rrate'].value
        #
        If = self.cov_wts * self.pop.covs[1][:, it] * I
        erate = self.pop.covs[0][:, it] * (
            self.eps * self.pop.covs[2][:, it] * R + self.vareps +
            self.beta * If * self.pop.Ksp)  # order matters!
        Erate = (self.pop.hidx - E - I) * erate
        assert np.all(Erate >= 0)
        self.cErate = Erate.cumsum()
        #
        Irate = E * self.gamma
        assert np.all(Irate >= 0)
        self.cIrate = Irate.cumsum()
        # main wheat back to Susceptible during summers
        Srate = I * (self.pop.covs[2][:, it] > 0)
        assert np.all(Srate >= 0)
        self.cSrate = Srate.cumsum()
        # infection to reservoir
        Rrate = (self.pop.hidx - R) * I
        assert np.all(Rrate >= 0)
        self.cRrate = Rrate.cumsum()
        # reservoir removal as main wheat stay healthy
        Sr_rate = self.rrate * R * (1 - self.pop.covs[2][:, it])
        assert np.all(Sr_rate >= 0)
        self.cSr_rate = Sr_rate.cumsum()
        #
        return np.cumsum([
            self.cErate[-1], self.cCrate[-1], self.cIrate[-1],
            self.cSrate[-1], self.cRrate[-1], self.cSr_rate[-1]])

    def update_state(self, state, crate, t):
        ei = np.argmax(crate > nr.rand() * crate[-1])
        if ei == 0:
            'main S->E'
            ind = np.argmax(self.cErate > nr.rand() * self.cErate[-1])
        elif ei == 1:
            'main E->I'
            ind = np.argmax(self.cCrate > nr.rand() * self.cIrate[-1])
        elif ei == 2:
            'main I->S'
            ind = np.argmax(self.cSrate > nr.rand() * self.cSrate[-1])
        elif ei == 3:
            'reservoir S->I'
            ind = np.argmax(self.cRrate > nr.rand() * self.cRrate[-1])
        elif ei == 4:
            'reservoir I->S'
            ind = np.argmax(self.cSr_rate > nr.rand() * self.cSr_rate[-1])
        state[:, ind] += self.rupdate[ei]
        return state, [ind, ei]


class PosteriorModel(SECI.PosteriorModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def build_nodes(self, init_trajs):
        if len(init_trajs) == 2:
            Einit = None
            Iinit, Rinit = init_trajs
            # Rinit to be simulated given Iinit
        elif len(init_trajs) == 3:
            Einit, Iinit, Rinit = init_trajs
        else:
            assert False, len(init_trajs)
        #
        Enodes, Inodes, Rnodes = [], [], [], []
        Dnodes = []
        data_nodes = []
        for j in xrange(self.pop.n_pop):
            data = self.data.get_nodedata(j)  # list
            data_nodes += [data[0]]
            smax = Iinit[j][1][-1] if self.opts.fsize_known else None
            #
            Iprior = INode(self.pop, j, smax, data, None)
            Ipi = np.zeros(Iprior.n_state)
            Ipi[int(Iinit[j][1][0])] = 1
            Inodes += [hyper.Trajectory(Iinit[j], Iprior, Ipi)]
            #
            Eprior = ENode(self.pop, j, smax, None, None)
            Epi = poisson.pmf(Eprior.space[:, 1], self.init.prior[j, 0])
            gamma = self.params['gamma'].value
            gamma = gamma.mean() if self.params['gamma'].ncells > 1 else gamma
            Eval = util.move_traj_back(Iinit[j], gamma, self.pop.tvec)\
                if Einit is None else Einit[j]
            Enodes += [hyper.Trajectory(Eval, Eprior, Epi)]
            #
            Rprior = RNode(self.pop, j, smax, None, None)
            Rpi = poisson.pmf(Rprior.space[:, 1], self.init.prior[j, 2])
            Rnodes += [hyper.Trajectory(Rinit[j], Rprior, Rpi)]
            #
            dnode = data_nodes[j]
            if (dnode.traj[1] > 0).sum() == 0:
                t1 = self.pop.tvec[-1] + 1
            else:
                t1 = dnode.traj[0][np.argmax(dnode.traj[1] > 0)]
            if t1 < self.pop.tvec[0]:
                Dt = self.pop.tvec[[0, -1]]
                Ds = np.ones(2)
            elif t1 < self.pop.tvec[-1]:
                Dt = np.array([self.pop.tvec[0], t1, self.pop.tvec[-1]])
                Ds = np.array([0, 1, 1])
            else:
                Dt = self.pop.tvec[[0, -1]]
                Ds = np.zeros(2)
            Dnodes += [hyper.Trajectory(np.vstack([Dt, Ds]), None, None)]
        self.nodes = {
            'Exposed': Enodes, 'Infectious': Inodes, 'Reservoir': Rnodes}
        self.Dnodes = Dnodes
        self.data_nodes = data_nodes
        for j in xrange(self.pop.n_pop):
            Enodes[j].prior.set_blanket(self.nodes)
            Inodes[j].prior.set_blanket(self.nodes)
            Rnodes[j].prior.set_blanket(self.nodes)

    def init_node_update(self, jratio=1.1, obs_dt=1):
        assert False, "not implemented yet"

    def data_dlogl(self, dprob_q):
        return super(PosteriorModel, self).data_dlogl(
            dprob_q, Iname='Infectious')

    def joint_logl(self, alpha=None, mu=None, eps=None, vareps=None):
        n = self.pop.n_pop
        if alpha is None and mu is None:
            Ksp, Kfull, Kloc = self.pop.Ksp, self.pop.Kfull, self.pop.Kloc
        elif mu is None:
            Ksp, Kfull, Kloc = self.pop.compute_kernel(
                alpha, self.params['mu'].value, self.opts.dkernel,
                self.opts.freq_depend, self.with_Kfull, self.opts.knorm)
        elif alpha is None:
            Ksp, Kfull, Kloc = self.pop.compute_kernel(
                None, mu, self.opts.dkernel,
                self.opts.freq_depend, self.with_Kfull, self.opts.knorm)
        if eps is None:
            eps = self.params['epsilon'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(eps):
            eps = eps[:, self.pop.base[1]][self.pop.base[0], :]
        if vareps is None:
            vareps = self.params['varepsilon'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(vareps):
            vareps = vareps[:, self.pop.base[1]][self.pop.base[0], :]
        #
        nodes = self.nodes['Exposed'] + self.nodes['Infectious'] +\
            self.nodes['Reservoir']
        eps = np.ones((n, self.pop.n_dt)) * eps if np.isscalar(eps)\
            else eps
        vareps = np.ones((n, self.pop.n_dt)) * vareps if np.isscalar(vareps)\
            else vareps
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((n, self.pop.n_dt)) * beta if np.isscalar(beta)\
            else beta
        gamma = self.params['gamma'].expand_by_time(self.pop.base[0], self.pop)
        gamma = np.ones((n, self.pop.n_dt)) * gamma if np.isscalar(gamma)\
            else gamma
        rrate = self.params['rrate'].expand_by_time(self.pop.base[0], self.pop)
        rrate = np.ones((n, self.pop.n_dt)) * rrate if np.isscalar(rrate)\
            else rrate

        def rate_fn(inp, i, save):
            S, R, it = inp[0], inp[1], int(inp[-1][0])
            # instantaneous states (not cumulative)

            def full_update():
                eprop = self.pop.covs[1][:, it] * S[1, :] * Ksp
                erate = (self.pop.hidx - S[0, :]) * self.pop.covs[0][:, it] * (
                    eps[:, it] * self.pop.covs[2][:, it] * R +
                    vareps[:, it] + beta[:, it] * eprop)
                irate = (S[0, :] - S[1, :]) * gamma[:, it]
                srate = S[1, :] * (self.pop.covs[2][:, it] > 0)
                Wrate = erate + irate + srate
                rr_rate = R * S[1, :]
                sr_rate = (self.pop.hidx - R) * rrate[:, it] * (
                    1 - self.pop.covs[2][:, it])
                Rrate = rr_rate + sr_rate
                save = [Wrate, Rrate, Wrate + Rrate]
                return save

            if i is None:
                rind = None
                save = full_update()
            elif i < n:
                'main wheat event in cell i'
                rind = (self.pop.hidx[i] - S[:, i].sum()) * save[0][i] +\
                    (S[0, i] - S[1, i]) * save[1][i] + S[1, i] * save[2][i]
            elif i < 2*n:
                i = i - n
                'reservoir event in cell j'
                rind = R[i] * save[3][i] +\
                    (self.pop.hidx[i] - R[i]) * save[4][i]
            elif i == 2 * n:
                'environment dynamics'
                rind = None
                save = full_update()
            else:
                assert False, "unexpected event"
            return rind, save, save[-1]

        pop_dyns = [self.pop.tvec, np.arange(self.pop.n_dt)]
        logl = util.explogp_merge(rate_fn, nodes, pop_dyns)
        print "Without data", logl
        #
        n_node = 1
        for j in xrange(self.pop.n_pop):
            Inode = self.nodes['Symptomatic'][j]
            data_list = Inode.prior.data
            for k, data in enumerate(data_list):

                def logp_fn(inp, i, t, tp):
                    r, it = inp
                    ri = Inode.prior.encode_state(r)
                    if i == n_node:
                        # data observed
                        return data.segment_logp(it, ri, t, tp)
                    else:
                        return data.segment_logp(None, ri, t, tp)
                Dt = data.traj[0, :]
                data.set_probs(Inode.prior.space[:, 1:],
                               self.params['dprob'].value[k, :], self.pop)
                logl += util.data_merge(
                    logp_fn, [Inode], self.pop.tvec[[0, -1]],
                    [Dt, np.arange(len(Dt)) + 1])[0]

    def beta_gibbs(self):
        nodes = self.nodes['Exposed'] + self.nodes['Infectious']
        n = self.pop.n_pop
        eps = self.params['epsilon'].expand_by_time(self.pop.base[0], self.pop)
        eps = np.ones((self.pop.n_pop, self.pop.n_dt)) * eps\
            if np.isscalar(eps) else eps
        vareps = self.params['varepsilon'].expand_by_time(
            self.pop.base[0], self.pop)
        vareps = np.ones((self.pop.n_pop, self.pop.n_dt)) * vareps\
            if np.isscalar(vareps) else vareps
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((self.pop.n_pop, self.pop.n_dt)) * beta\
            if np.isscalar(beta) else beta

        def rate_fn(inp, i, save):
            pass

        if self.params['beta'].bins is None:
            shape, hagg, r1, h2 = util.beta_merge_no_bins(
                rate_fn, nodes, self.pop)
        else:
            shape, hagg, r1, h2, bit, biz = util.beta_merge_with_bins(
                rate_fn, nodes, self.pop)

        def get_betaval():
            beta = self.params['beta'].value * 1.
            if np.isscalar(beta):
                beta *= np.ones_like(hagg)
            return beta
        beta_old = get_betaval()
        a, b = self.params['beta'].prior
        # print a + shape, b + hagg
        self.params['beta'].update([a + shape, b + hagg])
        beta_new = get_betaval()
        if self.params['beta'].bins is None:
            dlogl = - hagg * (beta_new - beta_old) +\
                (np.log(r1 + h2 * beta_new) -
                 np.log(r1 + h2 * beta_old)).sum()
        else:
            dlogl = - (hagg * (beta_new - beta_old)).sum() +\
                (np.log(r1 + h2 * beta_new[bit][biz]) -
                 np.log(r1 + h2 * beta_old[bit][biz])).sum()
        return dlogl


class WNode(struct.NodePostModel):
    """
    Main wheat status
    """
    def make_state_space(self):
        states = np.array([[0, 0],   # Susceptible
                           [1, 0],   # Exposed
                           [0, 1]])  # Infectious
        state_space = np.hstack([np.arange(len(states))[:, None], states])
        rupdate = [
            np.array([1, 0]),   # main S->E
            np.array([-1, 1]),  # main E->I
            np.array([0, -1]),  # main I->S
            ]
        return state_space, rupdate

    def get_genrate(self, params):
        """
        Rates at which 3 epidemiological events (S->E, E->I, I->S) happen
        Parents = Wnodes[neighbours + self.ind], Rnodes[self.ind]
        """
        erates, tvec =
        return [erates, irates, srates], tvec

    def get_outlogp(self, params, W):
        """
        """


class RNode(struct.NodePostModel):
    """
    Reservoir status
    """
    def make_state_space(self):
        states = np.array([[0],   # No reservoir
                           [1]])  # Infection reservoir
        state_space = np.hstack([np.arange(len(states))[:, None], states])
        rupdate = [
            np.array([1]),   # reservoir S->I
            np.array([-1]),  # reservoir I->S
            ]
        return state_space, rupdate

    def get_genrate(self, params):
        """
        Rates at which 2 epidemiological events (S->I, I->S) happen
        Parents = Wnodes[self.ind], Rnodes[self.ind]
        """
        return [irates, srates], tvec

    def get_outlogp(self, params, W):
        """
        """
