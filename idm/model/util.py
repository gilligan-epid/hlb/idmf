import exception
import numpy as np
import numpy.random as nr
from scipy.misc import logsumexp
import copy
import heapq


def sample_discrete(pvec, log=False, size=1):
    if log:
        pvec = np.exp(pvec - pvec.max())
    r = nr.rand(size)
    cpvec = pvec.cumsum()
    cpvec = cpvec/cpvec[-1]
    return np.array([np.argmax(cpvec > x) for x in r])


def move_traj_back(traj, param, Tp, fixed=False):
    " Move trajectory backward "
    T, S = traj
    W, V = copy.copy(T[1:-1]), copy.copy(S[1:-1])
    if len(W) == 0:
        return copy.copy(traj)
    if np.isscalar(param):
        if fixed:
            W = W - 1./param
        else:
            W = W - nr.exponential(scale=1./param, size=len(W))
    else:
        assert False, 'to check again'
        n = len(param)
        assert n == len(Tp)-1
        iters = [zip(W, zip(np.zeros_like(W), np.arange(len(W)))),
                 zip(Tp[:-1], zip(np.ones(n), np.arange(n)))]
        for (t, (i, s)) in heapq.merge(*iters):
            if i == 1:
                it = s
                continue
            for j in xrange(it, -1, -1):
                tau = nr.exponential(scale=1./param[it])
                if t - tau > Tp[it]:
                    break
                t = Tp[it]
            W[s] = t - tau
    idx = (W <= T[0])
    v0 = idx.sum()
    W = W[~idx]
    W.sort()
    W = np.hstack([[T[0]], W, [T[-1]]])
    V = np.hstack([[S[0] + v0], V[v0:], [S[-1]]])
    assert len(W) == len(V)
    return np.vstack([W, V])


def rate_merge(rate_fn, nodes, *dyns):
    """
    Utility to compute transition rate of epidemic nodes
    """
    i = slice(1, -1)
    n_node = len(nodes)
    iters = [zip(x.value[0][i], zip(np.ones_like(x.value[1][i], dtype=int) * j,
                 x.value[1][i])) for j, x in enumerate(nodes)]
    iters += [zip(x[0][i], zip(np.ones_like(x[1][1:], dtype=int) * (j+n_node),
                  x[1][1:])) for j, x in enumerate(dyns)]
    rates, tvec = [], []
    #
    t_start = nodes[0].value[0][0] if len(nodes) > 0 else dyns[0][0]
    inp = np.hstack([[x.value[1][0] for x in nodes], [x[1][0] for x in dyns]])
    tvec += [t_start]
    rates += [rate_fn(inp)]
    for (t, (i, s)) in heapq.merge(*iters):
        inp[i] = s
        tvec += [t]
        rates += [rate_fn(inp)]
    t_end = nodes[0].value[0][-1] if len(nodes) > 0 else dyns[0][-1]
    tvec += [t_end]
    return np.hstack(rates), np.array(tvec)


def traj_merge(nodes, compartments, tt):
    i = slice(1, -1)
    n = len(nodes[compartments[0]])
    iters = [zip(tt, zip(
        -np.ones_like(tt[1:], dtype=int), np.arange(1, len(tt))))]
    for ic, C in enumerate(compartments):
        iters += [
            zip(x.value[0][i],
                zip(np.ones_like(x.value[1][i], dtype=int) * j + ic * n,
                x.value[1][i])) for j, x in enumerate(nodes[C])]
    #
    S = np.zeros((len(tt), n, len(compartments)))
    s0 = np.array([[x.value[1][0] for x in nodes[C]]
                   for C in compartments])
    S[0, :, :] = s0.T
    it, state = 0, s0
    for (t, (i, s)) in heapq.merge(*iters):
        if i < 0:
            S[it, :, :] = state.T
            it = s
        else:
            ic = int(i / n)
            j = i % n
            state[ic, j] = s
    S[-1, :, :] = S[-2, :, :]
    return S


def explogp_merge(rate_fn, nodes, *dyns):
    """
    Compute epidemic log-probability by merging over multiple
    trajectories and dynamics.
    """
    i = slice(1, -1)
    n_node = len(nodes)
    n_traj = n_node + len(dyns)
    iters = [zip(x.value[0][i], zip(np.ones_like(x.value[1][i], dtype=int) * j,
                 x.value[1][i])) for j, x in enumerate(nodes)]
    iters += [zip(x[0][i], zip(np.ones_like(x[1][1:], dtype=int) * (j+n_node),
                  x[1][1:])) for j, x in enumerate(dyns)]
    t0, tF = nodes[0].value[0][[0, -1]] if len(nodes) > 0 else dyns[0][[0, -1]]
    logp = 0.
    #
    tp = t0
    inp = np.hstack([[x.value[1][0] for x in nodes], [x[1][0] for x in dyns]])
    ignore, save, rsum = rate_fn(inp, None, 0)
    # print "init", rsum, save, inp
    for (t, (i, s)) in heapq.merge(*iters):
        # print (t, (i, s))
        assert t > tp, (t, tp)
        inp[i] = s
        logp -= rsum * (t - tp)
        # print logp
        if not np.isfinite(logp):
            assert False
        if i < n_traj:
            rind, save, rsum = rate_fn(inp, i, save)
            if rind is not None:
                logp += np.log(rind)
        # print rind, save
        # print rsum, save, rind, logp
        tp = t
    logp -= rsum * (tF - tp)
    return logp


def outlogp_merge(rate_fn, logpi_fn, nodes, W, *dyns):
    """
    W already contains dyns's time points, but not nodes's
    """
    i = slice(1, -1)
    n_node, n_dyn = len(nodes), len(dyns)
    # print "n_dyn", n_dyn, n_node
    # print dyns[1]
    n_interval = len(W) - 1
    # (node_t, (i=node_i, node_state))
    iters = [zip(x.value[0][i], zip(np.ones_like(x.value[1][i], dtype=int) * j,
                 x.value[1][i])) for j, x in enumerate(nodes)]
    # (W_t, (i=n_node, W_it))
    iters += [zip(W[i], zip(np.ones(n_interval - 1, dtype=int) * n_node,
                            1 + np.arange(n_interval - 1)))]
    logp = [0. for j in xrange(n_interval)]
    #
    tp = W[0]
    inp = np.hstack([[x.value[1][0] for x in nodes],  # [node_state]
                     [0 for j in xrange(n_dyn)]])  # [dyns_it]
    iw = 0
    ignore, save, rsum = rate_fn(inp, None, None, 0)
    logp[iw] += logpi_fn(inp)
    for (t, (i, s)) in heapq.merge(*iters):
        # print (t, (i, s))
        logp[iw] -= rsum * (t - tp)
        # W_t
        if i == n_node:
            # dyns' time
            for k in xrange(n_dyn):
                if t == dyns[k][int(inp[k + n_node]) + 1]:
                    inp[k + n_node] += 1
                    rind, save, rsum = rate_fn(inp, None, k, save)
                    if rind is not None:
                        logp[iw] += np.log(rind)
                    break
            iw = s
        # node_t
        else:
            inp[i] = s
            rind, save, rsum = rate_fn(inp, i, None, save)
            if rind is not None:
                logp[iw] += np.log(rind)
        tp = t
    logp[-1] -= rsum * (W[-1] - tp)
    # print "logp", zip(W, logp)
    return np.vstack(logp).T


def data_merge(logp_fn, nodes, W, data):
    """
    Utility to compute data log-likelihood
    """
    ii = slice(1, -1)
    n_node = len(nodes)
    n_traj = n_node + 1
    # (node_t, (i=0, node_state))
    iters = [zip(x.value[0][ii], zip(
        np.ones_like(x.value[1][ii], dtype=int) * j, x.value[1][ii]))
             for j, x in enumerate(nodes)]
    # (data_t, (i=n_node, data_it))
    Dt, iD = data
    iters += [zip(Dt, zip(np.ones_like(iD, dtype=int) * n_node, iD))]
    # (W_t, (i=n_traj, W_it))
    nw = len(W[ii])
    iters += [zip(W[ii], zip(
        np.ones(nw, dtype=int) * n_traj, np.arange(nw) + 1))]
    logp = [0. for j in xrange(nw + 1)]
    #
    tp = W[0]
    inp = [x.value[1][0] for x in nodes] + [0] + [0]
    for (t, (i, s)) in heapq.merge(*iters):
        # print (t, (i, s)), inp
        logp[inp[-1]] += logp_fn(inp[:-1], i, t, tp)
        inp[i] = s
        tp = t
    logp[-1] += logp_fn(inp[:-1], n_traj, W[-1], tp)
    # print zip(W, logp)
    return np.hstack(logp)


def beta_merge_with_bins(rate_fn, nodes, pop):
    """
    Utility to compute quantities for beta gibbs step
    """
    i = slice(1, -1)
    n_node = len(nodes)
    # (node_t, (node_i, node_s))
    iters = [zip(x.value[0][i], zip(np.ones_like(x.value[1][i], dtype=int) * j,
                 x.value[1][i])) for j, x in enumerate(nodes)]
    # (pop.tvec_t, (i=n_node, tvec_i))
    iters += [zip(pop.tvec[i], zip(np.ones(pop.n_dt - 1, dtype=int) * n_node,
                  1 + np.arange(pop.n_dt-1)))]
    bshape = [np.zeros(pop.n_iz) for j in xrange(pop.n_it)]
    bsurv = [np.zeros(pop.n_iz) for j in xrange(pop.n_it)]
    hazd2, rate1, bit, biz = [], [], [], []
    #
    tp = pop.tvec[0]
    inp = np.hstack([[x.value[1][0] for x in nodes], [0]])
    ignore, save, rsum = rate_fn(inp, None, 0)
    # print "Init", save
    for (t, (i, s)) in heapq.merge(*iters):
        inp[i] = s
        it = pop.base[1][int(inp[-1])]
        bsurv[it] += rsum * (t - tp)
        event, save, rsum = rate_fn(inp, i, save)
        # print (t, (i, s)), event, save, rsum
        if event is not None:
            secondary, r1, h2, iz = event
            assert h2 >= 0, [(t, (i, s)), event]
            if secondary:
                bshape[it][iz] += 1
            rate1 += [r1]
            hazd2 += [h2]
            bit += [it]
            biz += [iz]
        tp = t
        # print "beta", (bshape, bsurv)
    bsurv[pop.base[1][-1]] += rsum * (pop.tvec[-1] - tp)
    bshape, bsurv = np.hstack(bshape), np.hstack(bsurv),
    rate1, hazd2 = np.array(rate1), np.array(hazd2)
    bit, biz = np.array(bit, dtype=int), np.array(biz, dtype=int)
    return bshape, bsurv, rate1, hazd2, bit, biz


def beta_merge_no_bins(rate_fn, nodes, pop):
    """
    Utility to compute quantities for beta gibbs step
    """
    i = slice(1, -1)
    n_node = len(nodes)
    # (node_t, (node_i, node_s))
    iters = [zip(x.value[0][i], zip(np.ones_like(x.value[1][i], dtype=int) * j,
                 x.value[1][i])) for j, x in enumerate(nodes)]
    # (pop.tvec_t, (i=n_node, tvec_i))
    iters += [zip(pop.tvec[i], zip(np.ones(pop.n_dt - 1, dtype=int) * n_node,
                  1 + np.arange(pop.n_dt-1)))]
    bshape = 0
    bsurv = 0
    hazd2, rate1 = [], []
    #
    tp = pop.tvec[0]
    inp = np.hstack([[x.value[1][0] for x in nodes], [0]])
    ignore, save, rsum = rate_fn(inp, None, 0)
    # print "Init", save
    for (t, (i, s)) in heapq.merge(*iters):
        inp[i] = s
        bsurv += rsum * (t - tp)
        event, save, rsum = rate_fn(inp, i, save)
        # print (t, (i, s)), event, save, rsum
        if event is not None:
            secondary, r1, h2, iz = event
            assert h2 >= 0, [(t, (i, s)), event]
            if secondary:
                bshape += 1
            rate1 += [r1]
            hazd2 += [h2]
        tp = t
        # print "beta", (bshape, bsurv)
    bsurv += rsum * (pop.tvec[-1] - tp)
    rate1, hazd2 = np.array(rate1), np.array(hazd2)
    return bshape, bsurv, rate1, hazd2


def sigma_merge(rate_fn, nodes, pop):
    ii = slice(1, -1)
    # (node_t, (node_i, node_s))
    iters = [zip(x.value[0][ii], zip(np.ones_like(x.value[1][ii], dtype=int) * j,
                 x.value[1][ii])) for j, x in enumerate(nodes)]
    #
    nevent, hagg = 0, 0
    inp = np.hstack([[x.value[1][0] for x in nodes]])  # init state
    ig, hsum = rate_fn(inp, None, 0)
    tp = pop.tvec[0]
    for (t, (i, s)) in heapq.merge(*iters):
        hagg += hsum * (t - tp)
        inp[i] = s  # update state
        event, hsum = rate_fn(inp, i, hsum)
        if event is not None:
            nevent += 1
        tp = t
    hagg += hsum * (pop.tvec[-1] - tp)
    return nevent, hagg


def make_generator_matrix(prior, params):
    """
    Input:
    + qtvec = intervals during which generation rates stay constant, i.e.
    decided by parent configurations.
    + jrates[ir][s, it] = instantaneous rate to move from state s to
    its next state, i.e. an event with deterministic state occurs, during
    interval [qtvec[it], qtvec[it+1])
    Output: Q[s, s', it] = rate to move from state s to s' during
    interval [qtvec[it], qtvec[it+1])
    -- VERIFIED
    """
    nS = prior.n_state + 1
    # Assume that only the first reaction depends on parent configuration
    jrates, qtvec = prior.get_genrate(params)
    assert np.all(jrates[0] >= 0), (jrates[0] < 0).sum()
    assert len(jrates) == prior.n_react, (len(jrates), prior.n_react)
    # print "qtvec", qtvec
    # print "Jump rates:", jrates[0][:, 0], jrates[0][:, 1]
    nT = len(qtvec) - 1
    Q = np.zeros((nS, nS, nT))
    for ir in xrange(prior.n_react):
        next_state = prior.space[:, 1:] + prior.rupdate[ir]
        for i, state in enumerate(next_state):
            sc = prior.encode_state(state)
            if sc >= 0:
                Q[i, sc, :] += jrates[ir][i, :]
            else:
                Q[i, -1, :] += jrates[ir][i, :]
            Q[i, i, :] -= jrates[ir][i, :]
    # print "Q0", Q[:5, :5, 0]
    # print "Q1", Q[:5, :5, 1]
    return Q, qtvec


def add_vjump(Q, qtvec, T, S, jratio):
    """
     1. Add virtual jumps by uniformisation: (T, S) --> (W, V)
     2. Compute transition matrix
        transmat[s, s', it] = probability to move from state s to s'
        during interval [W[it], W[it + 1])
    """
    nS = Q.shape[0]

    def uniformise(Q, iq):
        """
        Pt[s, s'] = prob. to move from state s to s' during interval
        [qtvec[iq], qtvec[iq+1])
        """
        exit_rate = - np.diag(Q[:, :, iq]).min() * jratio
        # assert iq == 0 or exit_rate > 0, "{} at {}".format(
        #     Q[:, :, iq].sum(), iq)
        if exit_rate == 0:
            Pt = np.eye(nS)
        else:
            Pt = Q[:, :, iq] / exit_rate + np.eye(nS)
        if not np.all(Pt >= 0):
            print "iq", iq
            print Q[:, :, iq]
            print Pt
            assert False
        return Pt, exit_rate
    ####
    iters = [zip(T[1:], np.zeros(len(T[1:]), dtype=int)),
             zip(qtvec[1:-1], np.ones(len(qtvec[1:-1]), dtype=int))]
    it, iq, tp, s = 0, 0, T[0], S[0]
    Pt, exit_rate = uniformise(Q, iq)
    T_q, S_q, transmat = [tp], [s], []
    kvec = [0]
    i = 0
    for (t, k) in heapq.merge(*iters):
        # sample virtual jumps in interval (tp, t)
        self_rate = Q[s, s, iq] + exit_rate
        assert t > tp, (t, k, tp)
        # print t, exit_rate, self_rate
        while tp < t:
            if self_rate == 0:
                break
            tau = nr.exponential(scale=1./self_rate)
            while tau == 0:
                print 'tau', 0
                tau = nr.exponential(scale=1./self_rate)
            tp += tau
            if tp < t:
                T_q += [tp]
                S_q += [s]
                transmat += [Pt]
                kvec += [k]
                i += 1
                # print "Add virtual", (tp, s), k
        # add the ending point
        T_q += [t]
        S_q += [s]
        tp = t
        kvec += [k]
        i += 1
        # print "Add point", (t, s), k
        if k == 0:
            transmat += [Pt]
            it += 1
            s = S[it]
        elif k == 1:
            # print i
            transmat += [np.eye(nS)]
            iq += 1
            Pt, exit_rate = uniformise(Q, iq)
    T_q, S_q = np.array(T_q), np.array(S_q)
    transmat = np.array(transmat)[:-1].transpose((1, 2, 0))
    kvec = np.array(kvec)
    assert len(kvec) == len(T_q), (len(kvec), len(T_q))
    # for i in xrange(len(kvec)):
    #    print i, kvec[i], transmat[:3, :3, i]
    assert transmat.shape == (nS, nS, len(T_q) - 2)
    return T_q, S_q, transmat


def drop_vjump(T, S):
    """
    Drop virtual jumps, keeping [t_start, t_end] and [init_state, last_state]
    """
    assert len(T) == len(S), "len(T, S) = ({}, {})".format(len(T), len(S))
    idx = np.array([True] + list(np.diff(S).astype(bool)))
    T, S = T[idx], S[idx]
    assert len(T) == len(S)
    return T, S


def make_evidence_matrix(prior, W, params):
    """
    E[s, it] = evidence that the node has state s during
    interval [W[it], W[it+1])
    """
    logE = np.zeros((prior.n_state + 1, len(W) - 1))
    logE[-1, :] = - np.inf  # Absorbing state
    # print logE.shape, prior.get_outlogp(params, W).shape
    logE[:-1, :] += prior.get_outlogp(params, W)
    return logE


def forward_filtering(logpi, logtransmat, logsoftev):
    """
    Forward recursion with logsumexp
    """
    assert logtransmat.ndim == 3
    nT = logsoftev.shape[1]
    logalpha = np.zeros((logtransmat.shape[0], nT))
    #
    logalpha[:, 0] = logpi + logsoftev[:, 0]
    # print "alpha0", logpi, logsoftev[:, 0], logalpha[:, 0]
    for it in xrange(1, nT):
        logalpha[:, it] = logsoftev[:, it] + logsumexp(
            logalpha[:, it-1] + logtransmat[:, :, it-1].T, axis=1)
        # print "alpha", it, logsoftev[:, it], logalpha[:, it]
        # print logtransmat[:5, :5, it-1]
        # print logsumexp(
        #     logalpha[:, it-1] + logtransmat[:, :, it-1].T, axis=1)
        if np.any(np.isnan(logalpha[:, it])):
            print "Failed at forward step {}".format(it)
            print logsoftev[:, it]
            print logsumexp(
                logalpha[:, it-1] + logtransmat[:, :, it-1].T, axis=1)
            print logalpha[:, it-1], logsoftev[:, it-1], logpi
            print logtransmat[:, :, it-1]
            raise exception.DataError
            return logalpha, -np.inf, False
    logl = logsumexp(logalpha[:, -1])
    return logalpha, logl, True


def ffbs(pi, transmat, logsoftev):
    """
    Forward-filtering backward-sampling
    pi[s] = Pr(S[0]=s),
    transmat[s, s', it] = Pr(S[it]=s'|S[it]=s)
    softev[s, it] = Pr(obs during [T[it], T[it+1])|S[it]=s)
    """
    nT = logsoftev.shape[1]
    S = np.zeros(nT, dtype=int)
    logtransmat = np.log(transmat)
    #
    logalpha, logl, success = forward_filtering(
        np.log(pi), logtransmat, logsoftev)
    if not success:
        return S, logl, False
    S[nT - 1] = sample_discrete(logalpha[:, nT - 1], log=True)
    # print "last", S[nT - 1], logalpha[:, nT - 1]
    # if S[nT - 1] == 0:
    #     assert False
    for it in xrange(nT - 2, -1, -1):
        sc = S[it + 1]
        it_filtered = logsoftev[sc, it + 1] - logalpha[sc, it + 1] +\
            logtransmat[:, sc, it] + logalpha[:, it]
        S[it] = sample_discrete(it_filtered, log=True)
        # print it, S[it], it_filtered
    return S, logl, True


def tune(scale, acc_rate):
    """
    Tunes the scaling parameter for the proposal distribution
    """
    if acc_rate is None:
        pass
    elif acc_rate < 0.001:
        scale *= 0.1
    elif acc_rate < 0.05:
        scale *= 0.5
    elif acc_rate < 0.2:
        scale *= 0.9
    elif acc_rate > 0.95:
        scale *= 10.
    elif acc_rate > 0.75:
        scale *= 2.
    elif acc_rate > 0.5:
        scale *= 1.1
    return scale
