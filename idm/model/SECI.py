from idm.model import struct, util, hyper

import numpy as np
import numpy.random as nr
from scipy.stats import poisson

name = "SECI"
n_comp, iI = 3, 2
compartments = ['Exposed', 'Cryptic', 'Symptomatic']
param_list = [
    'epsilon', 'beta', 'alpha', 'mu', 'varepsilon',  # transmission params
    'eta',    # control efficiency
    'gamma',  # rate of onset of infectiousness (E->C)
    'sigma',  # rate of emergence of symptoms (C->I)
    'dprob',  # detection probability per infected-unit-host
    'drate',  # reverse detection lag (NOT used for simulation)
]
rupdate = [
    np.array([1, 0, 0]),   # Exposed
    np.array([-1, 1, 0]),  # Infectious
    np.array([0, -1, 1]),  # Symptomatic
]


class PriorModel(struct.PopModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def get_rate(self, state, it, param_update):
        """
            E, C, I = INSTANT number of exposeds, cryptics, symptomatics,
            it = transition period index
        """
        E, C, I = state
        assert np.all(E + C + I <= self.pop.hidx), zip(
            E + C + I, self.pop.hidx)
        if param_update:
            self.eps = self.params['epsilon'].expand_by_space(it, self.pop)
            self.vareps = self.params['varepsilon'].expand_by_space(
                it, self.pop)
            self.beta = self.params['beta'].expand_by_space(it, self.pop)
            self.gamma = self.params['gamma'].expand_by_space(it, self.pop)
            self.sigma = self.params['sigma'].expand_by_space(it, self.pop)
        #
        Cf = self.pop.covs[1][:, it] * (C + I)
        erate = self.eps + self.pop.covs[0][:, it] * (
            self.vareps + self.beta * Cf * self.pop.Ksp)  # order matters!
        Erate = (self.pop.hidx - E - C - I) * erate
        self.cErate = Erate.cumsum()
        #
        Crate = E * self.gamma
        self.cCrate = Crate.cumsum()
        #
        Irate = C * self.sigma
        self.cIrate = Irate.cumsum()
        return np.cumsum([self.cErate[-1], self.cCrate[-1], self.cIrate[-1]])

    def update_state(self, state, crate, t):
        ei = np.argmax(crate > nr.rand() * crate[-1])
        if ei == 0:
            'Infection: S->E'
            ind = np.argmax(self.cErate > nr.rand() * self.cErate[-1])
        elif ei == 1:
            'Infectious: E->C'
            ind = np.argmax(self.cCrate > nr.rand() * self.cCrate[-1])
        elif ei == 2:
            'Symptomatic: C->I'
            ind = np.argmax(self.cIrate > nr.rand() * self.cIrate[-1])
        state[:, ind] += self.rupdate[ei]
        return state, [ind, ei]


class PosteriorModel(struct.PopPostModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def build_nodes(self, init_trajs):
        if len(init_trajs) == 1:
            Einit, Cinit = None, None
            Iinit = init_trajs[0]
        elif len(init_trajs) == 3:
            Einit, Cinit, Iinit = init_trajs
        else:
            assert False, len(init_trajs)
        #
        Enodes, Cnodes, Inodes = [], [], []
        Dnodes = []
        data_nodes = []
        for j in xrange(self.pop.n_pop):
            data = self.data.get_nodedata(j)  # list
            data_nodes += [data[0]]
            smax = Iinit[j][1][-1] if self.opts.fsize_known else None
            #
            Iprior = INode(self.pop, j, smax, data, None)
            Ipi = np.zeros(Iprior.n_state)
            Ipi[int(Iinit[j][1][0])] = 1
            Inodes += [hyper.Trajectory(Iinit[j], Iprior, Ipi)]
            #
            Cprior = CNode(self.pop, j, smax, None, None)
            Cpi = poisson.pmf(Cprior.space[:, 1], self.init.prior[j, 1])
            sigma = self.params['sigma'].expand_by_time(
                self.pop.base[0][j], self.pop)
            Cval = util.move_traj_back(Iinit[j], sigma, self.pop.tvec)\
                if Cinit is None else Cinit[j]
            Cnodes += [hyper.Trajectory(Cval, Cprior, Cpi)]
            #
            Eprior = ENode(self.pop, j, smax, None, None)
            Epi = poisson.pmf(Eprior.space[:, 1], self.init.prior[j, 0])
            gamma = self.params['gamma'].value
            gamma = gamma.mean() if self.params['gamma'].ncells > 1 else gamma
            Eval = util.move_traj_back(Cval, gamma, self.pop.tvec)\
                if Einit is None else Einit[j]
            Enodes += [hyper.Trajectory(Eval, Eprior, Epi)]
            #
            dnode = data_nodes[j]
            if (dnode.traj[1] > 0).sum() == 0:
                t1 = self.pop.tvec[-1] + 1
            else:
                t1 = dnode.traj[0][np.argmax(dnode.traj[1] > 0)]
            if t1 < self.pop.tvec[0]:
                Dt = self.pop.tvec[[0, -1]]
                Ds = np.ones(2)
            elif t1 < self.pop.tvec[-1]:
                Dt = np.array([self.pop.tvec[0], t1, self.pop.tvec[-1]])
                Ds = np.array([0, 1, 1])
            else:
                Dt = self.pop.tvec[[0, -1]]
                Ds = np.zeros(2)
            Dnodes += [hyper.Trajectory(np.vstack([Dt, Ds]), None, None)]
        self.nodes = {
            'Exposed': Enodes, 'Cryptic': Cnodes, 'Symptomatic': Inodes}
        self.Dnodes = Dnodes
        self.data_nodes = data_nodes
        for j in xrange(self.pop.n_pop):
            Enodes[j].prior.set_blanket(self.nodes)
            Cnodes[j].prior.set_blanket(self.nodes)
            Inodes[j].prior.set_blanket(self.nodes)

    def init_node_update(self, jratio=1.1, obs_dt=1):
        print "Inodes ..."
        for j in self.inds_pos:
            # print j,
            Enode, Cnode, Inode = [self.nodes[c][j] for c in compartments]
            t_pos = Inode.prior.data[0].t_1st_pos()
            assert t_pos is not None
            It, Is = Inode.value
            if t_pos < It[0]:
                continue
            '''print "Before",
            print "I", zip(It, Is),
            print "C", zip(Cnode.value[0, :], Cnode.value[1, :]),
            print "E", zip(Enode.value[0, :], Enode.value[1, :])'''
            if Is[-1] == 0 or It[np.argmax(Is > 0)] > t_pos:
                '''print "Node", j
                print "Before",
                print "I", zip(It, Is),
                print "C", zip(Cnode.value[0, :], Cnode.value[1, :]),
                print "E", zip(Enode.value[0, :], Enode.value[1, :])'''
                # adjust Inode to be consistent with the data
                t_pos -= obs_dt * nr.rand()
                if Is[-1] == 0:
                    It = np.hstack([It[0], t_pos, It[-1]])
                    Is = np.array([0, 1, 1])
                else:
                    # push the first symptomatic time back
                    It[np.argmax(Is > 0)] = t_pos
                Inode.value = np.vstack([It, Is])
                # print "After move back",
                # print "I", zip(It, Is),
                # adjust Cnode
                sigma = self.params['sigma'].expand_by_time(
                    self.pop.base[0][j], self.pop)
                Cnode.value = util.move_traj_back(
                    Inode.value, sigma, self.pop.tvec)
                # print "C", zip(Cnode.value[0, :], Cnode.value[1, :]),
                # adjust Enode
                gamma = self.params['gamma'].expand_by_time(
                    self.pop.base[0][j], self.pop)
                Enode.value = util.move_traj_back(
                    Cnode.value, gamma, self.pop.tvec)
                # print "E", zip(Enode.value[0, :], Enode.value[1, :])
            # posterior updates
            # print "After update",
            Cnode.update(self.params, jratio)
            # print "C", zip(Cnode.value[0, :], Cnode.value[1, :]),
            Enode.update(self.params, jratio)
            # print "E", zip(Enode.value[0, :], Enode.value[1, :]),
            Inode.update(self.params, jratio)
            # print "I", zip(Inode.value[0, :], Inode.value[1, :])
            assert Cnode.value[1, -1] > 0
            assert Enode.value[1, -1] > 0, Enode.pi
            assert Inode.value[1, -1] > 0

    def data_dlogl(self, dprob_q, Iname='Symptomatic'):
        # Data likelihood
        dlogl = 0
        # plant data
        n_node = 1
        for j in xrange(self.pop.n_pop):
            Inode = self.nodes[Iname][j]
            data_list = Inode.prior.data
            for k, data in enumerate(data_list):

                def logp_fn(inp, i, t, tp):
                    r, it = inp
                    ri = Inode.prior.encode_state(r)
                    if i == n_node:
                        # data observed
                        return data.segment_logp(it, ri, t, tp)
                    else:
                        return data.segment_logp(None, ri, t, tp)
                Dt = data.traj[0, :]
                data.set_probs(Inode.prior.space[:, 1:],
                               self.params['dprob'].value[k, :], self.pop)
                dlogl -= util.data_merge(
                    logp_fn, [Inode], self.pop.tvec[[0, -1]],
                    [Dt, np.arange(len(Dt)) + 1])[0]
                data.set_probs(Inode.prior.space[:, 1:], dprob_q[k, :],
                               self.pop)
                dlogl += util.data_merge(
                    logp_fn, [Inode], self.pop.tvec[[0, -1]],
                    [Dt, np.arange(len(Dt)) + 1])[0]
        return dlogl

    def joint_logl(self, alpha=None, mu=None, eps=None, vareps=None):
        # print delta
        if alpha is None and mu is None:
            Ksp, Kfull, Kloc = self.pop.Ksp, self.pop.Kfull, self.pop.Kloc
        elif mu is None:
            Ksp, Kfull, Kloc = self.pop.compute_kernel(
                alpha, self.params['mu'].value, self.opts.dkernel,
                self.opts.freq_depend, self.with_Kfull, self.opts.knorm)
        elif alpha is None:
            Ksp, Kfull, Kloc = self.pop.compute_kernel(
                None, mu, self.opts.dkernel,
                self.opts.freq_depend, self.with_Kfull, self.opts.knorm)
        if eps is None:
            eps = self.params['epsilon'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(eps):
            eps = eps[:, self.pop.base[1]][self.pop.base[0], :]
        if vareps is None:
            vareps = self.params['varepsilon'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(vareps):
            vareps = vareps[:, self.pop.base[1]][self.pop.base[0], :]
        #
        nodes = self.nodes['Exposed'] + self.nodes['Cryptic'] +\
            self.nodes['Symptomatic']
        n = self.pop.n_pop
        eps = np.ones((n, self.pop.n_dt)) * eps if np.isscalar(eps)\
            else eps
        vareps = np.ones((n, self.pop.n_dt)) * vareps if np.isscalar(vareps)\
            else vareps
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((n, self.pop.n_dt)) * beta if np.isscalar(beta)\
            else beta
        sigma = self.params['sigma'].expand_by_time(self.pop.base[0], self.pop)
        sigma = np.ones((n, self.pop.n_dt)) * sigma if np.isscalar(sigma)\
            else sigma
        gamma = self.params['gamma'].expand_by_time(self.pop.base[0], self.pop)
        gamma = np.ones((n, self.pop.n_dt)) * gamma if np.isscalar(gamma)\
            else gamma

        def rate_fn(inp, i, save):
            E, C, I = inp[:n], inp[n:2*n], inp[2*n:3*n]
            it = int(inp[-1])
            assert np.all(E >= C), (it, np.argwhere(E < C), E.sum(), C.sum())
            assert np.all(C >= I), (it, np.argwhere(C < I))
            assert np.all(self.pop.hidx >= E), [it, zip(self.pop.hidx, E)]

            def full_update():
                Cf = self.pop.covs[1][:, it] * C
                eprop = Cf * Ksp
                # per-capita infection rate at all sites
                erate = eps[:, it] + self.pop.covs[0][:, it] * (
                    vareps[:, it] + beta[:, it] * eprop)
                # per-capita E->C and C->I rates at all sites
                crate = gamma[:, it]
                irate = sigma[:, it]
                # sum of all rates
                rsum = ((self.pop.hidx - E) * erate + (E - C) * crate +
                        (C - I) * irate).sum()
                save = [erate, crate, irate, rsum]
                return save

            if i is None:
                rind = None
                save = full_update()
            elif i < n:
                'E[i]: e -> e+1'
                # rate at which an individual at site i gets infected
                rind = save[0][i]
                # change to i's population
                save[-1] += - rind + save[1][i]
            elif i < 2*n:
                j = i - n
                'C[j]: c -> c+1'
                # rate at which an individual at site j gets infectious
                rind = save[1][j]
                save[-1] += - save[1][j] + save[2][j]
                # change to j's population
                derate = 0
                save[-1] += (self.pop.hidx[j] - E[j]) * derate
                save[0][j] += derate
                # change to neighbouring populations
                inds = Kfull.neighbors[j]
                derate = self.pop.covs[0][inds, it] * beta[inds, it]\
                    * self.pop.covs[1][j, it] * Kfull.weights[j]
                save[-1] += ((self.pop.hidx[inds] - E[inds]) * derate).sum()
                save[0][inds] += derate
            elif i < 3*n:
                j = i - 2*n
                'I[j]: i -> i+1'
                # rate at which an individual at site j gets symptomatic
                rind = save[2][j]
                # change to j's population
                save[-1] += - save[2][j]
            elif i == 3*n:
                'Environment dynamics'
                rind = None
                save = full_update()
            else:
                assert False, "Unexpected event"
            return rind, save, save[-1]
        pop_dyns = [self.pop.tvec, np.arange(self.pop.n_dt)]
        logl = util.explogp_merge(rate_fn, nodes, pop_dyns)
        # print "Without data", logl
        # plant_data
        n_node = 1
        for j in xrange(self.pop.n_pop):
            Inode = self.nodes['Symptomatic'][j]
            data_list = Inode.prior.data
            for k, data in enumerate(data_list):

                def logp_fn(inp, i, t, tp):
                    r, it = inp
                    ri = Inode.prior.encode_state(r)
                    if i == n_node:
                        # data observed
                        return data.segment_logp(it, ri, t, tp)
                    else:
                        return data.segment_logp(None, ri, t, tp)
                Dt = data.traj[0, :]
                data.set_probs(Inode.prior.space[:, 1:],
                               self.params['dprob'].value[k, :], self.pop)
                logl += util.data_merge(
                    logp_fn, [Inode], self.pop.tvec[[0, -1]],
                    [Dt, np.arange(len(Dt)) + 1])[0]
        # print "With data", logl
        if alpha is not None or mu is not None:
            return logl, (Ksp, Kfull, Kloc)
        else:
            return logl

    def beta_gibbs(self):
        nodes = self.nodes['Exposed'] + self.nodes['Cryptic']
        n = self.pop.n_pop
        eps = self.params['epsilon'].expand_by_time(self.pop.base[0], self.pop)
        eps = np.ones((self.pop.n_pop, self.pop.n_dt)) * eps\
            if np.isscalar(eps) else eps
        vareps = self.params['varepsilon'].expand_by_time(
            self.pop.base[0], self.pop)
        vareps = np.ones((self.pop.n_pop, self.pop.n_dt)) * vareps\
            if np.isscalar(vareps) else vareps
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((self.pop.n_pop, self.pop.n_dt)) * beta\
            if np.isscalar(beta) else beta

        def rate_fn(inp, i, save):
            E, C, it = inp[:n], inp[n:2*n], int(inp[-1])
            assert np.all(E >= C), (it, np.argwhere(E < C))
            assert np.all(self.pop.hidx >= E), [it, zip(self.pop.hidx, E)]

            def full_update():
                Cf = self.pop.covs[1][:, it] * C
                # per-capita secondary infection propensity
                eprop = Cf * self.pop.Ksp
                # per-capita secondary infection hazard at all sites
                ehazd = self.pop.covs[0][:, it] * eprop
                # sum of secondary infection hazards by spatial zones
                hsum = self.params['beta'].collapse_by_space(
                    (self.pop.hidx - E) * ehazd, self.pop)
                save = [ehazd, hsum]
                return save

            if i is None:
                event = None
                save = full_update()
            elif i < n:
                'E[i]: e -> e+1'
                # rate at which an individual at site i gets infected
                rate2nd = beta[i, it] * save[0][i]
                rate1st = eps[i, it] + self.pop.covs[0][i, it] * vareps[i, it]
                iz = self.pop.base[0][i]
                # primary vs. secondary infection
                secondary = False
                if nr.rand() < rate2nd / (rate1st + rate2nd):
                    secondary = True
                event = [secondary, rate1st, save[0][i], iz]
                # changes due to the event
                if np.isscalar(save[-1]):
                    save[-1] -= save[0][i]
                else:
                    save[-1][iz] -= save[0][i]
            elif i < 2*n:
                j = i - n
                'C[j]: c -> c+1'
                event = None
                # changes due to an individual at site j gets infectious
                dhsum_all = np.zeros(self.pop.n_pop)
                # to j's population
                dehazd = self.pop.covs[0][j, it] * self.pop.covs[1][j, it]
                dhsum_all[j] = (self.pop.hidx[j] - E[j]) * dehazd
                save[0][j] += dehazd
                # to neighbouring populations
                inds = self.pop.Kfull.neighbors[j]
                dehazd = self.pop.covs[0][inds, it] * self.pop.covs[1][j, it]\
                    * self.pop.Kfull.weights[j]
                dhsum_all[inds] = (self.pop.hidx[inds] - E[inds]) * dehazd
                save[0][inds] += dehazd
                save[-1] += self.params['beta'].collapse_by_space(
                    dhsum_all, self.pop)
            elif i == 2*n:
                'Environment dynamics'
                event = None
                save = full_update()
            else:
                assert False, "Unexpected event"
            return event, save, save[-1]

        if self.params['beta'].bins is None:
            shape, hagg, r1, h2 = util.beta_merge_no_bins(
                rate_fn, nodes, self.pop)
        else:
            shape, hagg, r1, h2, bit, biz = util.beta_merge_with_bins(
                rate_fn, nodes, self.pop)

        def get_betaval():
            beta = self.params['beta'].value * 1.
            if np.isscalar(beta):
                beta *= np.ones_like(hagg)
            return beta
        beta_old = get_betaval()
        a, b = self.params['beta'].prior
        # print a + shape, b + hagg
        self.params['beta'].update([a + shape, b + hagg])
        beta_new = get_betaval()
        if self.params['beta'].bins is None:
            dlogl = - hagg * (beta_new - beta_old) +\
                (np.log(r1 + h2 * beta_new) -
                 np.log(r1 + h2 * beta_old)).sum()
        else:
            dlogl = - (hagg * (beta_new - beta_old)).sum() +\
                (np.log(r1 + h2 * beta_new[bit][biz]) -
                 np.log(r1 + h2 * beta_old[bit][biz])).sum()
        return dlogl

    def sigma_gibbs(self):
        nodes = self.nodes['Cryptic'] + self.nodes['Symptomatic']
        n = self.pop.n_pop

        def rate_fn(inp, i, hsum):
            C, I = inp[:n], inp[n:]
            assert np.all(C >= I), np.argwhere(C < I)

            if i is None:
                # first call
                event = None
                hsum = C.sum()
            elif i < n:
                # C[i]: c -> c + 1
                event = None
                hsum += 1
            else:
                j = i - n
                # I[j]: i -> i + 1
                event = 1
                hsum -= 1
            return event, hsum

        nevent, hagg = util.sigma_merge(rate_fn, nodes, self.pop)
        a, b = self.params['sigma'].prior
        sigma_old = self.params['sigma'].value * 1.
        self.params['sigma'].update([a + nevent, b + hagg])
        sigma_new = self.params['sigma'].value * 1.
        assert sigma_old != sigma_new, (sigma_old, sigma_new)
        dlogl = - hagg * (sigma_new - sigma_old) + nevent * (
            np.log(sigma_new) - np.log(sigma_old))
        return dlogl

    def drate_gibbs(self):
        nodes = self.nodes['Cryptic'] + self.Dnodes
        n = self.pop.n_pop

        def rate_fn(inp, i, hsum):
            C, D = inp[:n], inp[n:]
            assert np.all(C >= D), np.argwhere(C < D)

            if i is None:
                # first call
                event = None
                hsum = C.sum()
            elif i < n:
                # C[i]: c -> c + 1
                event = None
                hsum += 1
            else:
                j = i - n
                # D[j]: i -> i + 1
                event = 1
                hsum -= 1
            return event, hsum

        nevent, hagg = util.sigma_merge(rate_fn, nodes, self.pop)
        a, b = self.params['drate'].prior
        self.params['drate'].update([a + nevent, b + hagg])

    def epsilon_mh(self, logl, acc_rate):
        self.opts.tune['epsilon'] = util.tune(
            self.opts.tune['epsilon'], acc_rate)
        qa = self.opts.tune['epsilon'] * nr.randn()
        if self.params['epsilon'].ncells == 1:
            eps_q = self.params['epsilon'].value * np.exp(qa)
            logl_q = self.joint_logl(eps=eps_q)
            logp_q = logl_q + self.params['epsilon'].prior_logpdf(eps_q)
            logp = logl + self.params['epsilon'].prior_logpdf()
        else:
            je = nr.choice(2)
            eps_q = self.params['epsilon'].value * 1
            eps_q[je, :] = eps_q[je, :] * np.exp(qa)
            logl_q = self.joint_logl(eps=eps_q)
            logp_q = logl_q + self.params['epsilon'].prior_logpdf(eps_q)[je, 0]
            logp = logl + self.params['epsilon'].prior_logpdf()[je, 0]
        if logp_q - logp + qa > np.log(nr.rand()):
            self.params['epsilon'].set_val(eps_q)
            return True, logl_q, logp_q
        else:
            return False, logl, logp

    def varepsilon_mh(self, logl, acc_rate):
        self.opts.tune['varepsilon'] = util.tune(
            self.opts.tune['varepsilon'], acc_rate)
        qa = self.opts.tune['varepsilon'] * nr.randn()
        if self.params['varepsilon'].ncells == 1:
            eps_q = self.params['varepsilon'].value * np.exp(qa)
            logl_q = self.joint_logl(eps=eps_q)
            logp_q = logl_q + self.params['varepsilon'].prior_logpdf(eps_q)
            logp = logl + self.params['varepsilon'].prior_logpdf()
        else:
            je = nr.choice(2)
            eps_q = self.params['varepsilon'].value * 1
            eps_q[je, :] = eps_q[je, :] * np.exp(qa)
            logl_q = self.joint_logl(vareps=eps_q)
            logp_q = logl_q + self.params['varepsilon'].prior_logpdf(eps_q)[
                je, 0]
            logp = logl + self.params['varepsilon'].prior_logpdf()[je, 0]
        if logp_q - logp + qa > np.log(nr.rand()):
            self.params['varepsilon'].set_val(eps_q)
            return True, logl_q, logp_q
        else:
            return False, logl, logp


class ENode(struct.NodePostModel):
    """
    Cumulative Exposed (non-infectious, non-symptomatic)
    """
    def get_genrate(self, params):
        """
        Rate at which the node increases its state by 1
        Parents = Cnodes[neighbors + self.ind]
        """
        pinds = self.pop.Kfull.neighbors[self.ind]
        parents = [self.nodes['Cryptic'][pi] for pi in pinds]
        parents += [self.nodes['Cryptic'][self.ind]]
        eps = params['epsilon'].expand_by_time(self.iz, self.pop)
        eps = np.ones(self.pop.n_dt) * eps if np.isscalar(eps) else eps
        vareps = params['varepsilon'].expand_by_time(self.iz, self.pop)
        vareps = np.ones(self.pop.n_dt) * vareps if np.isscalar(vareps)\
            else vareps
        beta = params['beta'].expand_by_time(self.iz, self.pop)
        beta = np.ones(self.pop.n_dt) * beta if np.isscalar(beta) else beta

        def rate_fn(inp):
            C, c_this, it = inp[:-2], inp[-2], int(inp[-1])
            Cf = self.pop.covs[1][pinds, it] * C
            Cf_this = self.pop.covs[1][self.ind, it] * c_this
            # per-capita infection rate at the local site
            irate = eps[it] + self.pop.covs[0][self.ind, it] * (
                vareps[it] + beta[it] * (np.inner(
                    self.pop.Kfull.weights[self.ind], Cf))) 
            return (self.n_host - self.space[:, 1:]) * irate
        rates, tvec = util.rate_merge(
            rate_fn, parents, [self.pop.tvec, np.arange(self.pop.n_dt)])
        assert rates.shape == (self.n_state, len(tvec)-1)
        return [rates], tvec

    def get_outlogp(self, params, W):
        """
        Child = Cnodes[self.ind]
        Other Parents of Children = None
        """
        gamma = params['gamma'].expand_by_time(self.iz, self.pop)
        gamma = np.ones(self.pop.n_dt) * gamma if np.isscalar(gamma) else gamma
        Ct, Cs = self.nodes['Cryptic'][self.ind].value

        def logpi_fn(inp):
            ip, ic = inp.astype(int)
            logp = np.zeros(self.n_state)
            logp[self.space[:, 1] < Cs[ic]] = - np.inf
            return logp

        def rate_fn(inp, ni, di, save):
            """
            inp = [pop.tvec_i, Ct_i]
            ni = node index (\in\{None\} here)
            di = dyns index (\in\{0,1\} here)
            """
            ip, ic = inp.astype(int)

            def full_update():
                # rate at which a C event happens
                crate = gamma[ip] * (self.space[:, 1] > Cs[ic])
                # total rate of all C events
                rsum = (self.space[:, 1] - Cs[ic]) * crate
                return [crate, rsum]

            if di is None or di == 0:
                # at t0 or  pop.tvec_t
                rind = None
            else:
                rind = save[0]
            save = full_update()
            return rind, save, save[-1]
        logp = util.outlogp_merge(rate_fn, logpi_fn, [], W, self.pop.tvec, Ct)
        assert logp.shape == (self.n_state, len(W) - 1), (
            logp.shape, (self.n_state, len(W) - 1))
        return logp


class CNode(struct.NodePostModel):
    """
    Cumulative Cryptic (infectious, but not symptomatic)
    """

    def get_genrate(self, params):
        """
        Parent = Enodes[self.ind]
        """
        def rate_fn(inp):
            e, gam = inp
            return gam * (e - self.space[:, 1:]) * (e > self.space[:, 1:])
        parent = self.nodes['Exposed'][self.ind]
        gamma = params['gamma'].expand_by_time(self.iz, self.pop)
        if np.isscalar(gamma):
            rates = rate_fn([parent.value[1][:-1], gamma])
            tvec = parent.value[0]
        else:
            rates, tvec = util.rate_merge(
                rate_fn, [parent], [self.pop.tvec, gamma])
        assert rates.shape == (self.n_state, len(tvec)-1)
        return [rates], tvec

    def get_outlogp(self, params, W):
        """
        Children = Inodes[self.ind], Enodes[neighbors + self.ind],
        Other Parents of Children = Cnodes[neighbors of neighbors + neighbors]
        """
        ind_c = np.append(self.pop.Kfull.neighbors[self.ind], self.ind)
        Ec = [self.nodes['Exposed'][j] for j in ind_c[:-1]]
        ind_mb = []
        for j in ind_c:
            ind_mb = np.hstack([ind_mb, self.pop.Kfull.neighbors[j]])
        ind_mb = np.unique(ind_mb).astype(int)
        ind_mb = ind_mb[ind_mb != self.ind]
        Cmb = [self.nodes['Cryptic'][j] for j in ind_mb]
        #
        parT, parS = self.nodes['Exposed'][self.ind].value
        nodes = [self.nodes['Symptomatic'][self.ind]] + Ec + Cmb
        n = np.cumsum([1, len(ind_c) - 1, len(ind_mb)])
        eps = params['epsilon'].expand_by_time(
            self.pop.base[0][ind_c], self.pop)
        eps = np.ones((len(ind_c), self.pop.n_dt)) * eps\
            if np.isscalar(eps) else eps
        vareps = params['varepsilon'].expand_by_time(
            self.pop.base[0][ind_c], self.pop)
        vareps = np.ones((len(ind_c), self.pop.n_dt)) * vareps\
            if np.isscalar(vareps) else vareps
        beta = params['beta'].expand_by_time(
            self.pop.base[0][ind_c], self.pop)
        beta = np.ones((len(ind_c), self.pop.n_dt)) * beta\
            if np.isscalar(beta) else beta
        sigma = params['sigma'].expand_by_time(self.iz, self.pop)
        sigma = np.ones(self.pop.n_dt) * sigma if np.isscalar(sigma) else sigma
        #

        def logpi_fn(inp):
            logp = np.zeros(self.n_state)  # p=1 by default
            Ii = inp[0]
            logp[self.space[:, 1] < Ii] = -np.inf  # p=0 if state < sI
            ie = int(inp[-1])
            logp[self.space[:, 1] > parS[ie]] = -np.inf  # p=0 if state > sE
            return logp

        def rate_fn(inp, ni, di, save):
            """
            inp = [Inode_state, [Enode_state], [Cnode_state],
                   pop.tvec_i, thisEnode_i],
            ni = node index (\in\{0, ..., n_node - 1\} here)
            di = dyns index (\in\{0, 1\} here)
            """
            Ii, Ec0, Cmb = inp[0], inp[1:n[1]], inp[n[1]:n[2]]
            it, ie = int(inp[n[2]]), int(inp[-1])
            Ec = np.append(Ec0, parS[ie])
            assert np.all(self.pop.hidx[ind_c] >= Ec), [
                it, zip(self.pop.hidx[ind_c], Ec)]
            C = np.zeros(self.pop.n_pop)
            C[ind_mb] = Cmb

            def full_update():
                Cf = self.pop.covs[1][:, it] * C
                eprop = np.zeros((self.n_state, len(ind_c)))
                # infection pressure BY other sites
                eprop += (Cf * self.pop.Ksp)[ind_c][None, :]
                # infection pressure BY this site
                eprop[:, :-1] += self.pop.covs[1][self.ind, it] *\
                    self.space[:, 1:] * self.pop.Kfull.weights[self.ind]
                eprop[:, -1] += 0
                # per-capita infection pressure
                erate = eps[:, it] + self.pop.covs[0][ind_c, it] * (
                    vareps[:, it] + beta[:, it] * eprop)
                assert erate.shape == (self.n_state, len(ind_c))
                # infectious propensity of this site
                irate = (self.space[:, 1] > Ii) * sigma[it]
                # sum of all rates
                rsum = ((self.pop.hidx[ind_c] - Ec) * erate).sum(1) + (
                    self.space[:, 1] - Ii) * irate
                assert rsum.shape[0] == self.n_state
                return [erate, irate, rsum]

            if ni is None and di is None:
                # at t0
                rind = None
                save = full_update()
            elif ni is not None:
                # at node_t
                if ni == 0:
                    'I[self.ind]: i -> i + 1'
                    rind = save[1] * 1.
                    # changes to irate[:]
                    save[1] = (self.space[:, 1] > Ii) * sigma[it]
                    save[-1] += save[1] - rind
                elif ni < n[1]:
                    j = ni - 1
                    'Ec[j]: e -> e + 1'
                    rind = save[0][:, j] * 1.
                    # changes to erate[:, j]
                    save[-1] -= rind
                    save[0][:, j] = 0.
                elif ni < n[2]:
                    j = ind_mb[ni - n[1]]
                    'C[j]: c -> c + 1'
                    rind = None
                    # changes to erate[:, :]
                    deprop = np.zeros(self.pop.n_pop)
                    deprop[j] = 0
                    inds = self.pop.Kfull.neighbors[j]
                    deprop[inds] =\
                        self.pop.covs[1][j, it] * self.pop.Kfull.weights[j]
                    derate = self.pop.covs[0][ind_c, it] * beta[:, it] *\
                        deprop[ind_c]
                    save[0] += np.tile(derate, (self.n_state, 1))
                    save[-1] += ((self.pop.hidx[ind_c] - Ec) *
                                 derate).sum()
            elif di is not None:
                if di == 0:
                    # at pop.tvec_t
                    rind = None
                    save = full_update()
                elif di == 1:
                    # at thisEnode_t
                    'Ec[-1]: e -> e + 1'
                    rind = save[0][:, -1] * 1.
                    # changes to erate[:, -1]
                    save[-1] -= rind
                    save[0][:, -1] = 0.
            return rind, save, save[-1]
        logp = util.outlogp_merge(rate_fn, logpi_fn, nodes, W,
                                  self.pop.tvec, parT)
        assert logp.shape == (self.n_state, len(W) - 1), (
            logp.shape, (self.n_state, len(W) - 1))
        return logp


class INode(struct.NodePostModel):
    """
    Cumulative Infected (infectious, symptomatic)
    """
    def get_genrate(self, params):
        """
        Parent = Cnodes[self.ind]
        """
        def rate_fn(inp):
            c, sig = inp
            return sig * (c - self.space[:, 1:]) * (c > self.space[:, 1:])
        parent = self.nodes['Cryptic'][self.ind]
        sigma = params['sigma'].expand_by_time(self.iz, self.pop)
        if np.isscalar(sigma):
            rates = rate_fn([parent.value[1][:-1], sigma])
            tvec = parent.value[0]
        else:
            rates, tvec = util.rate_merge(
                rate_fn, [parent], [self.pop.tvec, sigma])
        assert rates.shape == (self.n_state, len(tvec)-1)
        return [rates], tvec

    def get_outlogp(self, params, W):
        """
        Children = data
        Parents of Children = None
        """
        n_node = 0
        data_list = self.data
        logp = 0
        for k, data in enumerate(data_list):
            data.set_probs(self.space[:, 1:], params['dprob'].value[k, :],
                           self.pop)
            Dt = data.traj[0, :]

            def logp_fn(inp, i, t, tp):
                it = inp[0]
                if i == n_node:
                    # data observed
                    return data.segment_logp_space(it, t, tp)
                else:
                    return data.segment_logp_space(None, t, tp)
            logp += util.data_merge(
                logp_fn, [], W, [Dt, np.arange(len(Dt)) + 1])
            assert logp.shape == (self.n_state, len(W) - 1)
        return logp
