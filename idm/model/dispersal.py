import numpy as np
import scipy.sparse as ssp


def exponential_fn(D, alpha, bpow=1, norm=False):
    'set bpow=2 to get Gaussian kernel'
    if norm:
        Z = 1. / (2 * np.pi * (alpha ** 2))
        return Z * np.exp(- (D / alpha))
    else:
        return np.exp(- (D / alpha) ** bpow)


"""
Power-law functions:
    1. Pareto: the basic power law
        f(d) = d ** (- beta)
    is not appropriate since it has a pole at x = 0.
    2. Pareto II / Lomax: shifted Pareto
        f(d) = (d + alpha) ** (- beta) \propto (1 + d / alpha) ** (- beta)
    beta could be set to 1 for simplification
    3. Student / Cauchy:
        f(d) = (d ** 2 + alpha ** 2) ** (- beta)
             \propto (1 + (d / alpha) ** 2) ** (- beta)
    again beta is set to 1 for simplification = 1 degree of freedom
    4. Adjusted Cauchy: as used in FMD by Chris Jewell
       f(d) = alpha * Cauchy(d)
"""


def cauchy_fn(D, alpha, bpow=1.5, norm=False):
    if norm:
        Z = 1 / (2 * np.pi * (alpha ** 2))
        return Z * ((1 + (D / alpha) ** 2) ** (- 1.5))
    else:
        return (1 + (D / alpha) ** 2) ** (- bpow)


def lomax_fn(D, alpha, bpow=1):
    return (1 + D / alpha) ** (- bpow)


def brand_test_fn(r, a, beta=1.):
    return beta / ((2 * np.pi * (1 + r**2)) ** (a/2))


"""
Dispersal functions for sparse distance matrix
"""


def exp_sfn(Dsp, alpha, norm=True):
    """
    K(r; \alpha) = \exp( - r / \alpha) / Z(\alpha)
    Z(\alpha) = \iint K(r; \alpha) dr dr = 2 * \pi * \alpha^2
    """
    em1 = ssp.csr_matrix.expm1(- (Dsp / alpha))
    vals = em1.data + 1
    Z = 1
    if norm:
        Z = 1. / (2 * np.pi * (alpha ** 2))
        vals = vals * Z
    out = ssp.csr_matrix((vals, Dsp.indices, Dsp.indptr),
                         shape=Dsp.shape)
    assert len(out.data) == len(Dsp.data)
    return out, Z


def cau_sfn(Dsp, alpha, bpow=1.5, norm=True):
    """
    K(r; \alpha) = (1 + (r / \alpha)^2)^(-b) / Z(\alpha)
    Z(\alpha) = \iint K(r; \alpha) dr dr = 2 * \pi * \alpha^2 if bpow = 1.5
    """
    pow = (Dsp / alpha).power(2)
    vals = 1 / (pow.data + 1) ** (- bpow)
    Z = 1
    if norm:
        assert bpow == 1.5
        Z = 1. / (2 * np.pi * (alpha ** 2))
        vals = vals * Z
    out = ssp.csr_matrix((vals, Dsp.indices, Dsp.indptr),
                         shape=Dsp.shape)
    assert len(out.data) == len(Dsp.data)
    return out, Z


def cau_adj_sfn(Dsp, alpha, bpow=1):
    """

    """
    Dpow = Dsp.power(2)
    out = ssp.csr_matrix((alpha / ((Dpow + alpha ** 2) ** bpow),
                          Dsp.indices, Dsp.indptr), shape=Dsp.shape)
    assert len(out.data) == len(Dsp.data)
    return out


def uni_sfn(Dsp, alpha, norm=True):
    ' i.e. set all entries to 1'
    vals = np.ones_like(Dsp.data)
    Z = 1
    if norm:
        Z = 1. / vals.sum()
        vals = vals * Z
    out = ssp.csr_matrix((vals, Dsp.indices, Dsp.indptr), shape=Dsp.shape)
    assert len(out.data) == len(Dsp.data)
    return out, Z
