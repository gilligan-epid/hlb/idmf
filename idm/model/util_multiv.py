import exception
import numpy as np
import numpy.random as nr
from scipy.misc import logsumexp
import copy
import heapq


def explogp_merge(rate_fn, nodes_list, *dyns):
    """
    Compute epidemic log-probability by merging over multiple
    trajectories and dynamics.
    """
    n_pop = len(nodes_list[0])
    ii = slice(1, -1)
    n_node, iters, inp_list = 0, [], []
    for nodes in nodes_list:
        iters += [zip(x.value[0][ii],
                      zip(np.ones_like(x.value[1][ii], dtype=int) * j,
                          x.value[1:, ii].T)) for j, x in enumerate(nodes)]
        n_node += len(nodes)
        inp_list += [np.vstack([x.value[1:, 0] for x in nodes])]
    n_traj = n_node + len(dyns)
    iters += [zip(x[0][ii], zip(np.ones_like(x[1][1:], dtype=int) * (j+n_node),
                  x[1][1:])) for j, x in enumerate(dyns)]
    inp_list += [[x[1][0] for x in dyns]]
    t0, tF = nodes[0].value[0][[0, -1]] if len(nodes) > 0 else dyns[0][[0, -1]]
    logp = 0.
    #
    tp = t0
    ignore, save, rsum = rate_fn(inp_list, None, 0)
    # print "init", rsum, save, inp
    for (t, (i, s)) in heapq.merge(*iters):
        # print (t, (i, s))
        assert t > tp, (t, tp)
        inp_list[int(i / n_pop)][i % n_pop] = s
        logp -= rsum * (t - tp)
        # print logp
        if not np.isfinite(logp):
            assert False
        if i < n_traj:
            rind, save, rsum = rate_fn(inp_list, i, save)
            if rind is not None:
                logp += np.log(rind)
        # print rind, save
        # print rsum, save, rind, logp
        tp = t
    logp -= rsum * (tF - tp)
    return logp


def rate_merge(rate_fn, nodes, *dyns):
    """
    Utility to compute transition rate of epidemic nodes
    """
    ii = slice(1, -1)
    n_node = len(nodes)
    iters = [zip(x.value[0][ii],
                 zip(np.ones_like(x.value[1][ii], dtype=int) * j,
                     x.value[1][ii])) for j, x in enumerate(nodes)]
    iters += [zip(x[0][ii], zip(np.ones_like(x[1][1:], dtype=int) * (j+n_node),
                  x[1][1:])) for j, x in enumerate(dyns)]
    rates, tvec = [], []
    #
    t0, tF = nodes[0].value[0][[0, -1]] if len(nodes) > 0 else dyns[0][[0, -1]]
    inp = np.hstack([[x.value[1][0] for x in nodes], [x[1][0] for x in dyns]])
    tvec += [t0]
    rates += [rate_fn(inp)]
    for (t, (i, s)) in heapq.merge(*iters):
        inp[i] = s
        tvec += [t]
        rates += [rate_fn(inp)]
    tvec += [tF]
    return np.hstack(rates), np.array(tvec)
