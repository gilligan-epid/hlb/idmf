import util
import exception

import numpy as np
import numpy.random as nr
import scipy.stats as sp
from scipy.sparse import csr_matrix
from shapely.geometry import box, Point


class Control(object):
    def __init__(self, value, pop, params):
        if not np.isscalar(value):
            assert np.all(value.shape == (pop.n_pop, pop.n_dt)), (
                value.shape, (pop.n_pop, pop.n_dt))
        self.value = value
        self.pop = pop
        self.params = params

    def get_wts(self, it):
        assert False, "get_wts(it) is not implemented."


class Quarantine(Control):
    def get_wts(self, it):
        if it > 0 and np.all(self.value[:, it] == self.value[:, it-1]):
            return None, None
        sw = self.pop.sw
        Qsp = csr_matrix((np.ones_like(sw.data), sw.indices, sw.indptr),
                         shape=sw.shape)
        Qz = np.ones(self.pop.n_pop)
        qjs = np.where(self.value[:, it] > 0)[0]
        # for each site j under quarantine
        for j in qjs:
            # get row j
            rsp = Qsp[j]
            # reduce rates to all neighbors
            rfull = np.ones(self.pop.n_pop) * (1 - self.params[0])
            # reset rates for under-quarantine neighbors
            rfull[qjs] = 1 - self.params[1]
            # set rates for row j
            rsp.data = rfull[rsp.indices]
            Qsp[j] = rsp
            Qz[j] = 1 - self.params[1]
        return Qsp, Qz

    def update_K(self, di, Ksp):
        """
        Given:
            di: index of newly detected site
            Ksp: current dispersal kernel
        The function puts the region surrounding di under quarantines.
        """
        assert np.isscalar(self.value)
        # 1. Construct a circle with di as the center
        grid = self.pop.grid
        xi, yi = grid.ixy[0][di], grid.ixy[1][di]
        x, y = grid.get_xy(xi, yi, scale=True)
        circle = Point(x, y).buffer(self.value)
        # 2. Get the list of all neighboring cells within or intersects with
        # the quarantine circle
        drow = self.pop.sw[di]
        nb_inds = drow.indices
        qjs = nb_inds[drow.data < self.value + 1./np.sqrt(grid.csize)]
        # 3. Compute the intersect portion for each neighboring cell
        qprops = np.ones(len(qjs))
        for i in xrange(len(qjs)):
            x, y = grid.get_xy(grid.ixy[0][qjs[i]], grid.ixy[1][qjs[i]],
                               scale=True)
            hs = .5 * grid.csize
            cell = box(x - hs, y - hs, x + hs, y + hs)
            qprops[i] = circle.intersection(cell).area
        # 4. Update the dispersal strengths from di and each neighboring cell
        # to cells outside the quarantine circle
        qjs = np.append(qjs, di)
        for i in xrange(len(qjs)):
            j = qjs[i]
            rsp = Ksp[j]
            # reduce spread from i to cells outside the quarantine region
            rfull = np.ones(self.pop.n_pop) * (1 - self.params[0])
            # reduce spread from i to cells within the quarantine region
            rfull[qjs] = 1 + qprops[i] * (
                (self.params[0] - self.params[1]) * qprops - self.params[0])
            rsp.data = rfull[rsp.indices]
            Ksp[j] = rsp.multiply(Ksp[j])
        return Ksp


class Treatment(Control):
    def __init__(self, value, pop, params, marks=None,
                 tstop=None, treset=None):
        super(Treatment, self).__init__(value, pop, params)
        self.marks = marks
        self.tstop = tstop  # time at which all treatments are halted
        self.treset = treset  # all treatment effect is diminished

    def update_wts(self, di, wts):
        """
        Given:
            di: index of newly detected site
            wts: current infectiousness
        The function puts the region surrounding di under treatment.
        """
        assert np.isscalar(self.value)
        # 1. Construct a circle with di as the center
        grid = self.pop.grid
        xi, yi = grid.ixy[0][di], grid.ixy[1][di]
        x, y = grid.get_xy(xi, yi, scale=True)
        circle = Point(x, y).buffer(self.value)
        # 2. Get the list of all neighboring cells within or intersects with
        # the treatment circle
        drow = self.pop.sw[di]
        nb_inds = drow.indices
        qjs = nb_inds[drow.data < self.value + 1./np.sqrt(grid.csize)]
        # 3. Compute the intersect portion for each of the treated cells
        qjs = np.append(qjs, di)
        qprops = np.ones(len(qjs))
        for i in xrange(len(qjs)):
            x, y = grid.get_xy(grid.ixy[0][qjs[i]], grid.ixy[1][qjs[i]],
                               scale=True)
            hs = .5 * grid.csize
            cell = box(x - hs, y - hs, x + hs, y + hs)
            qprops[i] = circle.intersection(cell).area
        # 4. Update the infectiousness weights for each of the treated cells
        rs = np.zeros_like(wts)
        rs[qjs] += self.params[0] * qprops
        if self.marks is not None:
            ms = np.unique(self.marks[qjs])
            # extend the treated area if it overlaps with commercial orchards
            for m in ms:
                idx = (self.marks == m)
                rs[idx] += self.params[0] * self.pop.cwts[1][idx]
                # print m, np.max(rs[idx])
        # assert False
        rs = rs.clip(max=self.params[0])
        # wts = (wts - rs).clip(min=1 - self.params[0])
        wts = np.minimum(wts, 1 - rs)
        return wts


class Parameter(object):
    def __init__(self, value, prior, bins=None, wc=1):
        self.value = value
        self.prior = prior
        self.bins = bins
        self.wc = wc
        if bins is not None:
            self.shape = np.array(map(len, bins))
            self.ncells = self.shape.prod()
            if value is not None:
                assert np.all(value.shape == self.shape), (
                    value.shape, self.shape)
        else:
            self.ncells = 1
        if value is None:
            self.update(prior)

    def update(self, hyper):
        assert False, "update(hyper) is not implemented."

    def prior_logpdf(self, value):
        assert False, "prior_logpdf(value) is not implemented."

    def set_val(self, value):
        self.value = value

    def get_val(self, iz, it):
        if self.ncells == 1:
            return self.value
        else:
            return self.value[iz, it]

    def expand_by_space(self, it, pop):
        'Expand a vector to M-length vector'
        if self.ncells == 1:
            return self.value
        else:
            return self.value[pop.base[0], pop.base[1][it]]

    def expand_by_time(self, iz, pop):
        'Expand a vector to K-length vector'
        if self.ncells == 1:
            return self.value
        else:
            return self.value[:, pop.base[1]][iz, :]

    def collapse_by_space(self, value, pop):
        if self.ncells == 1:
            return value.sum()
        else:
            out = np.zeros(self.shape[0])
            for i in xrange(self.shape[0]):
                out[i] += value[pop.bin_inds[i]].sum()
            return out


class GammaParameter(Parameter):
    """
    Parameter with a Gamma prior
    """
    uniform = [np.array([0.5]), np.array([1e-5])]  # shape, rate

    def update(self, hyper):
        self.value = sp.gamma(hyper[0], scale=1./hyper[1]).rvs()
        # print self.value

    def prior_logpdf(self, value=None):
        'expect scalar value here'
        value = self.value if value is None else value
        return sp.gamma(self.prior[0], scale=1./self.prior[1]).logpdf(value)


class ExponParameter(Parameter):
    def update(self, hyper):
        self.value = sp.expon(scale=hyper[0]).rvs()

    def prior_logpdf(self, value=None):
        value = self.value if value is None else value
        return sp.expon(scale=self.prior[0]).logpdf(value)


class ListParameter(Parameter):
    """
    Parameter with a prior = a list of samples
    """
    def update(self, hyper):
        idx = np.arange(len(hyper))
        self.value = hyper[nr.choice(idx)]
        if self.ncells > 1:
            assert np.all(self.value.shape == self.shape), (
                self.value.shape, self.shape)


class UniformParameter(Parameter):

    def prior_logpdf(self, value=None):
        if value is not None and not np.isscalar(value):
            return np.zeros_like(value)
        else:
            return 0.


class PopInit(Parameter):

    """
    Variable that represents the initial state of a population,
    init_eps = rate of primary infection pre-t0
    self.prior[j] = susceptibility to pre-t0 infection of site j
    """
    def update(self, hyper):
        " Update the first epidemic compartment only "
        newval = np.zeros_like(self.value)
        if hyper.ndim == 2:
            for j in xrange(hyper.shape[1] - 1, -1, -1):
                dist = hyper[:, j]
                idx = util.sample_discrete(
                    dist, size=nr.poisson(lam=dist.sum()))
                for i in idx:
                    newval[i, :j + 1] += 1
        elif hyper.ndim == 3:
            newval = hyper[nr.choice(np.arange(hyper.shape[0]))]
            assert np.all(newval.shape == self.value.shape), (
                newval.shape, self.value.shape)
        self.value = newval


class Trajectory(Parameter):
    """
    Trajectory of a node:
        + value = matrix of size 2 * nT, i.e.
            value[0, 0] = t_start; value[0, -1] = t_end;
            value[1, 0] = s0 (initial state); value[1, -1]==value[1, -2]
            value[0, i] = time of ith event
            value[1, i] = state after ith event
    """
    def __init__(self, value, prior, pi):
        self.pi = pi
        super(Trajectory, self).__init__(value, prior)

    def update(self, params, jratio=1.1):
        """
        """
        Q, qtvec = util.make_generator_matrix(self.prior, params)
        '''print "0", qtvec[0:2], "\n", Q[:, :, 0]
        iF = Q.shape[-1] - 1
        print iF, qtvec[iF:iF+2], "\n", Q[:, :, iF]'''
        T, S = self.value[0], self.value[1].astype(int)
        assert np.all(np.diff(T) > 0)
        # print "Traj before update:", self.value
        W, V, transmat = util.add_vjump(Q, qtvec, T, S, jratio)
        dw = np.diff(W)
        if not np.all(dw > 0):
            idx = np.argwhere(dw <= 0)
            for i in idx:
                i = i[0]
                print i, W[i:i+2]
            assert False, 'non-decreasing W expected'
        # print "qtvec", qtvec
        # print "W", zip(np.arange(len(W)), W)
        # print "lenW", len(W)
        # assert False
        logsoftev = util.make_evidence_matrix(self.prior, W, params)
        # print "ffbs quantites", transmat.shape, logsoftev.shape
        pi = np.append(self.pi, 0.)
        success = False
        while not success:
            V, logl_q, succ = util.ffbs(pi, transmat, logsoftev)
            if not succ:
                success = False
                raise exception.TraceUpdateError(
                    "FFBS failed, new parameters are needed!")
            if not np.all(V < self.prior.n_state):
                print "Got out of statespace, try again!"
            else:
                success = True
        T_q, S_q = util.drop_vjump(W[:-1], V)
        T_q, S_q = np.append(T_q, W[-1]), np.append(S_q, S_q[-1])
        self.value = np.vstack([T_q, S_q])
        # print "Traj after update:", self.value

    def mh_update(self, params):
        " update of single times "
