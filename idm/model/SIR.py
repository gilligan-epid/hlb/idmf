from idm.model import struct, util, hyper

import numpy as np
import numpy.random as nr
from scipy.stats import poisson

name = "SIR"
n_comp, iI = 2, 0
compartments = ['Infected', 'Removed']
param_list = [
    'epsilon', 'beta', 'alpha',  # transmission params (S->I)
    'mu',  # rate of removal of infections (I->R)
]
rupdate = [
    np.array([1, 0]),  # Infected
    np.array([-1, 1])  # Removed
]


class PriorModel(struct.PopModel):
    def setup(self, ):
        self.param_list = param_list
        self.rupdate = rupdate

    def get_rate(self, state, it):
        """
            I, R = INSTANT number of infectives, removals
            it = transition period index
        """
        I, R = state
        assert np.all(I + R <= self.pop.hidx), zip(I + R, self.pop.hidx)
        eps = self.params['epsilon'].expand_by_space(it, self.pop)
        beta = self.params['beta'].expand_by_space(it, self.pop)
        mu = self.params['mu'].expand_by_space(it, self.pop)
        #
        If = self.pop.covs[1][:, it] * I
        irate = self.pop.covs[0][:, it] * (eps + beta * (
            self.pop.Ksp * If + If) / self.pop.K0[:, it])
        Irate = (self.pop.hidx - I - R) * irate
        self.cIrate = Irate.cumsum()
        #
        Rrate = I * mu
        self.cRrate = Rrate.cumsum()
        return np.cumsum([self.cIrate[-1], self.cRrate[-1]])

    def update_state(self, state, crate):
        ei = np.argmax(crate > nr.rand() * crate[-1])
        if ei == 0:
            'Infection: S->I'
            ind = np.argmax(self.cIrate > nr.rand() * self.cIrate[-1])
        elif ei == 1:
            'Removal: I->R'
            ind = np.argmax(self.cRrate > nr.rand() * self.cRrate[-1])
        state[:, ind] += self.rupdate[ei]
        return state, [ind, ei]


class PosteriorModel(struct.PopPostModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def build_nodes(self, init_trajs):
        if len(init_trajs) == 1:
            Iinit = None
            Rinit = init_trajs[0]
        elif len(init_trajs) == 2:
            Iinit, Rinit = init_trajs
        else:
            assert False
        #
        Inodes, Rnodes = [], []
        for j in xrange(self.pop.n_pop):
            data = self.data.get_nodedata(j)
            smax = Rinit[j][1][-1] if self.opts.fsize_known else None
            #
            Rprior = RNode(self.pop, j, smax, data)
            Rpi = np.zeros(Rprior.n_state)
            Rpi[int(Rinit[j][1][0])] = 1
            Rnodes += [hyper.Trajectory(Rinit[j], Rprior, Rpi)]
            #
            Iprior = INode(self.pop, j, smax, None)
            Ipi = poisson.pmf(
                Iprior.space[:, 1], self.init.prior[j] * self.opts.init_eps)
            mu = self.params['mu'].expand_by_time(
                self.pop.base[0][j], self.pop)
            Ival = util.move_traj_back(Rinit[j], mu, self.pop.tvec)\
                if Iinit is None else Iinit[j]
            Inodes += [hyper.Trajectory(Ival, Iprior, Ipi)]
        self.nodes = {'Infected': Inodes, 'Removed': Rnodes}
        for j in xrange(self.pop.n_pop):
            Inodes[j].prior.set_blanket(self.nodes)
            Rnodes[j].prior.set_blanket(self.nodes)

    def joint_logl(self, alpha=None, eps=None):
        if alpha is None:
            Ksp, Kfull, K0 = self.pop.Ksp, self.pop.Kfull, self.pop.K0
        else:
            Ksp, Kfull, K0 = self.pop.compute_kernel(
                alpha, self.opts.dkernel, self.opts.freq_depend,
                self.with_Kfull)
        eps = self.params['epsilon'].value if eps is None else eps
        #
        nodes = self.nodes['Infected'] + self.nodes['Removed']
        n = self.pop.n_pop
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((n, self.pop.n_dt)) * beta if np.isscalar(beta)\
            else beta
        mu = self.params['mu'].expand_by_time(self.pop.base[0], self.pop)
        mu = np.ones((n, self.pop.n_dt)) * mu if np.isscalar(mu) else mu

        def rate_fn(inp, i, save):
            I, R, it = inp[:n], inp[n:2*n], int(inp[-1])
            assert np.all(I >= R), [it, zip(I, R)]
            assert np.all(self.pop.hidx >= I), [it, zip(self.pop.hidx, I)]

            def full_update():
                If = self.pop.covs[1][:, it] * (I - R)
                iprop = Ksp * If + If
                # per-capita infection rate at all sites
                irate = self.pop.covs[0][:, it] * (
                    eps + beta[:, it] * iprop / K0[:, it])
                # per-capita removal rate at all sites
                rrate = mu[:, it]
                # sum of all rates
                rsum = ((self.pop.hidx - I) * irate).sum() +\
                    ((I - R) * rrate).sum()
                save = [irate, rrate, rsum]
                return save

            def incr_irate_self(j, dI, dR):
                drate = self.pop.covs[0][j, it] * beta[j, it] *\
                    self.pop.covs[1][j, it] * (dI - dR) / K0[j, it]
                drsum = (self.pop.hidx[j] - I[j]) * drate - dI * save[0][j]
                return drate, drsum

            def incr_irate_nb(j, dI, dR, to_inds):
                drate = self.pop.covs[0][to_inds, it] * beta[to_inds, it] *\
                    self.pop.covs[1][j, it] * (dI - dR) * Kfull.weights[j]\
                    / K0[to_inds, it]
                drsum = ((self.pop.hidx[inds] - I[inds]) * drate).sum()
                return drate, drsum

            def incr_rrate(j, dI, dR):
                drsum = (dI - dR) * save[1][j]
                return drsum

            if i is None:
                rind = None
                save = full_update()
            elif i < n:
                'I[i]: i -> i+1'
                # rate at which an individual at site i gets infected
                rind = save[0][i]
                # changes due to the event
                # to i's population
                dirate, disum = incr_irate_self(i, 1, 0)
                drsum = incr_rrate(i, 1, 0)
                save[-1] += disum + drsum
                save[0][i] += dirate
                # to neighbouring populations
                inds = Kfull.neighbors[i]
                dirate, disum = incr_irate_nb(i, 1, 0, inds)
                save[-1] += disum
                save[0][inds] += dirate
            elif i < 2*n:
                j = i - n
                'R[j]: r -> r+1'
                # rate at which an individual at site j gets removed
                rind = save[1][j]
                # changes due to the event
                # to j's population
                dirate, disum = incr_irate_self(j, 0, 1)
                drsum = incr_rrate(j, 0, 1)
                save[-1] += disum + drsum
                save[0][j] += dirate
                # to neighbouring populations
                inds = Kfull.neighbors[j]
                dirate, disum = incr_irate_nb(j, 0, 1, inds)
                save[-1] += disum
                save[0][inds] += dirate
            elif i == 2*n:
                'Environment dynamics'
                rind = None
                save = full_update()
            else:
                assert False, "Unexpected event"
            return rind, save, save[-1]
        logl = util.explogp_merge(rate_fn, nodes,
                                  [self.pop.tvec, np.arange(self.pop.n_dt)])
        # print "Without data", logl
        # Data likelihood
        for i in xrange(self.pop.n_pop):
            Rnode = self.nodes['Removed'][i]
            data = Rnode.prior.data
            # print i
            # print Rnode.value
            # print zip(data.T, data.S)

            def logp_fn(inp, i, t, tp):
                r, it = inp
                if i == 1:
                    # data observed
                    logp = data.segment_logp(it, r, t, tp)
                else:
                    logp = data.segment_logp(None, r, t, tp)
                # print inp, i, t, tp, logp
                return logp
            logl += util.data_merge(
                logp_fn, [Rnode], self.pop.tvec[[0, -1]], [data.T,
                                                           np.arange(data.n)])
            # print logl
        if alpha is not None:
            return logl, (Ksp, Kfull, K0)
        else:
            return logl

    def beta_gibbs(self):
        nodes = self.nodes['Infected'] + self.nodes['Removed']
        n = self.pop.n_pop
        eps = self.params['epsilon'].value
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((self.pop.n_pop, self.pop.n_dt)) * beta\
            if np.isscalar(beta) else beta

        def rate_fn(inp, i, save):
            I, R, it = inp[:n], inp[n:2*n], int(inp[-1])
            assert np.all(I >= R), [it, zip(I, R)]
            assert np.all(self.pop.hidx >= I), [it, zip(self.pop.hidx, I)]

            def full_update():
                If = self.pop.covs[1][:, it] * (I - R)
                # per-capita secondary infection propensity
                iprop = self.pop.Ksp * If + If
                # per-capita secondary infection hazard at all sites
                ihazd = self.pop.covs[0][:, it] * iprop / self.pop.K0[:, it]
                # sum of secondary infection hazards by spatial zones
                hsum = self.params['beta'].collapse_by_space(
                    (self.pop.hidx - I) * ihazd, self.pop)
                save = [ihazd, hsum]
                return save

            def incr_ihazd_self(j, dI, dR):
                dhazd = self.pop.covs[0][j, it] * self.pop.covs[1][j, it]\
                        * (dI - dR) / self.pop.K0[j, it]
                dhsum = (self.pop.hidx[j] - I[j]) * dhazd - dI * save[0][j]
                return dhazd, dhsum

            def incr_ihazd_nb(j, dI, dR, to_inds):
                dhazd = self.pop.covs[0][to_inds, it] *\
                        self.pop.covs[1][j, it] * (dI - dR) *\
                        self.pop.Kfull.weights[j] / self.pop.K0[to_inds, it]
                dhsum = (self.pop.hidx[inds] - I[inds]) * dhazd
                return dhazd, dhsum

            if i is None:
                event = None
                save = full_update()
            elif i < n:
                'I[i]: i -> i+1'
                # rate at which an individual at site i gets infected
                rate2nd = beta[i, it] * save[0][i]
                rate1st = eps * self.pop.covs[0][i, it]
                # primary vs. secondary infection
                secondary = False
                if nr.rand() < rate2nd / (rate1st + rate2nd):
                    secondary = True
                event = [secondary, rate1st, save[0][i], self.pop.base[0][i]]
                #
                # changes due to the event
                ds_all = np.zeros(self.pop.n_pop)
                # to i's population
                dhazd, dsum = incr_ihazd_self(i, 1, 0)
                ds_all[i] = dsum
                save[0][i] += dhazd
                # to neighbouring populations
                inds = self.pop.Kfull.neighbors[i]
                dhazd, dsum = incr_ihazd_nb(i, 1, 0, inds)
                ds_all[inds] = dsum
                save[-1] += self.params['beta'].collapse_by_space(
                    ds_all, self.pop)
                save[0][inds] += dhazd
            elif i < 2*n:
                j = i - n
                'R[j]: r -> r+1'
                event = None
                # changes due to the removal of an individual at site j
                ds_all = np.zeros(self.pop.n_pop)
                # to j's population
                dhazd, dsum = incr_ihazd_self(j, 0, 1)
                ds_all[j] = dsum
                save[0][j] += dhazd
                # to neighbouring populations
                inds = self.pop.Kfull.neighbors[j]
                dhazd, dsum = incr_ihazd_nb(j, 0, 1, inds)
                ds_all[inds] = dsum
                save[-1] += self.params['beta'].collapse_by_space(
                    ds_all, self.pop)
                save[0][inds] += dhazd
            elif i == 2*n:
                'Environment dynamics'
                event = None
                save = full_update()
            else:
                assert False, "Unexpected event"
            return event, save, save[-1]
        shape, hagg, r1, h2, bit, biz = util.beta_merge(
            rate_fn, nodes, self.pop)

        def get_betaval():
            beta = self.params['beta'].value * 1.
            if np.isscalar(beta):
                beta *= np.ones_like(hagg)
            return beta
        beta_old = get_betaval()
        a, b = self.params['beta'].prior
        # print a + shape, b + hagg
        self.params['beta'].update([a + shape, b + hagg])
        beta_new = get_betaval()
        dlogl = - (hagg * (beta_new - beta_old)).sum() +\
            (np.log(r1 + h2 * beta_new[bit][biz]) -
             np.log(r1 + h2 * beta_old[bit][biz])).sum()
        return dlogl


class INode(struct.NodePostModel):
    """
    Cumulative Infected
    """
    def get_genrate(self, params):
        """
        Rate at which the node increases its state by 1
        Parents = Inodes, Rnodes
        """
        pinds = self.pop.Km1_full.neighbors[self.ind]
        n_pi = len(pinds)
        parI, parR = [], []
        for i in xrange(n_pi):
            parI += [self.nodes['Infected'][pinds[i]]]
            parR += [self.nodes['Removed'][pinds[i]]]
        parents = parI + parR + [self.nodes['Removed'][self.ind]]
        eps = params['epsilon'].expand_by_time(self.iz, self.pop)
        beta = params['beta'].expand_by_time(self.iz, self.pop)
        eps = np.ones(self.pop.n_dt) * eps if np.isscalar(eps) else eps
        beta = np.ones(self.pop.n_dt) * beta if np.isscalar(beta) else beta
        Kwts = self.pop.Km1_full.weights[self.ind]

        def rate_fn(inp):
            I, R, r_this, it = inp[:n_pi], inp[n_pi:2*n_pi], inp[-2], inp[-1]
            If = self.infs[it] * (I - R) * (I > R)
            If_this = self.infs[it] * (self.space[:, 1:] - r_this) * (
                self.space[:, 1:] > r_this)
            irate = self.susc[it] * (eps[it] + beta[it] * (
                np.inner(Kwts, If) + If.sum() + If_this) / self.pop.K0[it])
            return (self.n_host - self.space[:, 1:]) * irate
        rates, tvec = util.rate_merge(
            rate_fn, parents, [self.pop.tvec, np.arange(self.pop.n_dt)])
        assert rates.shape == (self.n_state, len(tvec) - 1),\
            (rates.shape, (self.n_state, len(tvec) - 1))
        return [rates], tvec

    def get_outlogp(self, params, W):
        """
        Child = Rnodes[self.ind]
        Other Parents of Children = None
        """
        mu = params['mu'].expand_by_time(self.iz, self.pop)
        assert np.isscalar(mu)
        T, R = self.nodes['Removed'][self.ind].value

        def rate_fn(inp, ni, di, save):
            ir = int(inp[0])

            def full_update():
                rrate = mu * (self.space[:, 1:] > R[ir])
                rsum = (self.space[:, 1:] - R[ir]) * rrate
                return [rrate, rsum]

            if ni is None and di is None:
                rind = None
            elif di is not None:
                rind = save[0] * 1.
            save = full_update()
            return rind, save, save[-1]
        logp = util.outlogp_merge(rate_fn, [], W, T)
        return logp


class RNode(struct.NodePostModel):
    """
    Cumulative Removed
    """
    def get_genrate(self, params):
        """
        Parents = Inodes[self.ind]
        """
        def rate_fn(inp):
            i, mu = inp
            return mu * (i - self.space[:, 1:]) * (i > self.space[:, 1:])
        parent = self.nodes['Infected'][self.ind]
        mu = params['mu'].expand_by_time(self.iz, self.pop)
        if np.isscalar(mu):
            rates = rate_fn([parent.value[1][:-1], mu])
            tvec = parent.value[0]
        else:
            rates, tvec = util.rate_merge(
                rate_fn, [parent], [self.pop.tvec, mu])
        assert rates.shape == (self.n_state, len(tvec)-1)
        return [rates], tvec

    def get_outlogp(self, params, W):
        """
        Child = data
        Other Parents of Children = None
        """
        def logp_fn(inp, i, t, tp):
            it = inp
            if i == 0:
                # data observed
                return self.data.segment_logp(it, self.space[:, 1:], t, tp)
            else:
                return self.data.segment_logp(None, self.space[:, 1:], t, tp)
        logp = util.data_merge(
            logp_fn, [], W, [self.data.T, np.arange(self.data.n)])
        assert logp.shape == (self.n_state, len(W) - 1)
        # print "Rdata", zip(self.data.T, self.data.S)
        # print "Rlogp", zip(W, logp.sum(0))
        return logp
