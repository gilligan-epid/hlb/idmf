from idm.model import struct, util, hyper, SECI

import numpy as np
import numpy.random as nr
from scipy.stats import poisson

name = "SECDS_SIS"
n_comp, iI = 4, 2
compartments = ['Exposed', 'Cryptic', 'Detected', 'Reservoir']
param_list = [
    'epsilon', 'beta', 'alpha', 'mu', 'varepsilon',  # transmission params
    'eta',    # control efficiency
    'gamma',  # rate of onset of infectiousness (E->C)
    'drate', 'dprob',  # detection rate & probability
    'rrate',  # infection to reservoir
]
rupdate = [
    np.array([1, 0, 0, 0]),   # main S->E
    np.array([-1, 1, 0, 0]),  # main E->C
    np.array([0, -1, 1, 0]),  # main C->D
    np.array([0, 0, -1, 0]),  # main D->S
    np.array([0, 0, 0, 1]),   # reservoir S->I
    np.array([0, 0, 0, -1]),  # reservoir I->S
]


class PriorModel(struct.PopModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.cov_wts = np.ones(self.pop.n_pop)

    def get_rate(self, state, it, param_update):
        """
            E, C, D, R = INSTANT number of exposeds, cryptics, infectious,
                          spore reservoirs
            it = transition period index
            self.pop.covs[2] = summer survival suitability, 0 other time
        """
        E, C, D, R = state
        assert np.all(E + C + D <= self.pop.hidx), zip(
            E + C + D, self.pop.hidx)
        assert np.all(R <= self.pop.hidx), (R, self.pop.hidx)
        if param_update:
            self.eps = self.params['epsilon'].expand_by_space(it, self.pop)
            self.vareps = self.params['varepsilon'].expand_by_space(
                it, self.pop)
            self.beta = self.params['beta'].expand_by_space(it, self.pop)
            self.gamma = self.params['gamma'].expand_by_space(it, self.pop)
            self.drate = self.params['drate'].expand_by_space(it, self.pop)
            self.dprob = self.params['dprob'].value
            self.rrate = self.params['rrate'].value
        #
        Cf = self.cov_wts * self.pop.covs[1][:, it] * (C + D * self.dprob)
        erate = self.pop.covs[0][:, it] * (
            self.eps * self.pop.covs[2][:, it] * R + self.cov_wts * (
                self.vareps + self.beta * Cf * self.pop.Ksp))  # order matters!
        Erate = (self.pop.hidx - E - C - D) * erate
        assert np.all(Erate >= 0)
        self.cErate = Erate.cumsum()
        #
        Crate = E * self.gamma
        assert np.all(Crate >= 0)
        self.cCrate = Crate.cumsum()
        #
        Irate = C * self.drate
        assert np.all(Irate >= 0)
        self.cIrate = Irate.cumsum()
        # main wheat back to Susceptible during summers
        Srate = D * (self.pop.covs[2][:, it] > 0)
        assert np.all(Srate >= 0)
        self.cSrate = Srate.cumsum()
        # infection to reservoir
        Rrate = (self.pop.hidx - R) * (C + D * self.dprob)
        assert np.all(Rrate >= 0)
        self.cRrate = Rrate.cumsum()
        # reservoir removal as main wheat stay healthy
        Sr_rate = self.rrate * R * (1 - self.pop.covs[2][:, it])
        assert np.all(Sr_rate >= 0)
        self.cSr_rate = Sr_rate.cumsum()
        #
        return np.cumsum([
            self.cErate[-1], self.cCrate[-1], self.cIrate[-1],
            self.cSrate[-1], self.cRrate[-1], self.cSr_rate[-1]])

    def update_state(self, state, crate, t):
        ei = np.argmax(crate > nr.rand() * crate[-1])
        if ei == 0:
            'main S->E'
            ind = np.argmax(self.cErate > nr.rand() * self.cErate[-1])
        elif ei == 1:
            'main E->C'
            ind = np.argmax(self.cCrate > nr.rand() * self.cCrate[-1])
        elif ei == 2:
            'main C->I'
            ind = np.argmax(self.cIrate > nr.rand() * self.cIrate[-1])
        elif ei == 3:
            'main I->S'
            ind = np.argmax(self.cSrate > nr.rand() * self.cSrate[-1])
        elif ei == 4:
            'reservoir S->I'
            ind = np.argmax(self.cRrate > nr.rand() * self.cRrate[-1])
        elif ei == 5:
            'reservoir I->S'
            ind = np.argmax(self.cSr_rate > nr.rand() * self.cSr_rate[-1])
        state[:, ind] += self.rupdate[ei]
        return state, [ind, ei]


