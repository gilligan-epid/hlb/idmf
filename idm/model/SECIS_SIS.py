from idm.model import struct, util, hyper, SECI

import numpy as np
import numpy.random as nr
from scipy.stats import poisson

name = "SEIS_SIS"
n_comp, iI = 4, 2
compartments = ['Exposed', 'Infectious', 'Reservoir']
param_list = [
    'epsilon', 'beta', 'alpha', 'mu', 'varepsilon',  # transmission params
    'eta',    # control efficiency
    'gamma',  # rate of onset of infectiousness (E->C)
    'dprob',  # detection probability
    'rrate',  # reservoir decay rate
]
rupdate = [
    np.array([1, 0, 0, 0]),   # main S->E
    np.array([-1, 1, 0, 0]),  # main E->C
    np.array([0, -1, 1, 0]),  # main C->I
    np.array([0, 0, -1, 0]),  # main I->S
    np.array([0, 0, 0, 1]),   # reservoir S->I
    np.array([0, 0, 0, -1]),  # reservoir I->S
]


class PriorModel(struct.PopModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.cov_wts = np.ones(self.pop.n_pop)

    def get_rate(self, state, it, param_update):
        """
            E, C, I, R = INSTANT number of exposeds, cryptics, infectious,
                          spore reservoirs
            it = transition period index
            self.pop.covs[2] = summer survival suitability, 0 other time
        """
        E, C, I, R = state
        assert np.all(E + C + I <= self.pop.hidx), zip(
            E + C + I, self.pop.hidx)
        assert np.all(R <= self.pop.hidx), (R, self.pop.hidx)
        if param_update:
            self.eps = self.params['epsilon'].expand_by_space(it, self.pop)
            self.vareps = self.params['varepsilon'].expand_by_space(
                it, self.pop)
            self.beta = self.params['beta'].expand_by_space(it, self.pop)
            self.gamma = self.params['gamma'].expand_by_space(it, self.pop)
            self.drate = self.params['drate'].expand_by_space(it, self.pop)
            self.dprob = self.params['dprob'].value
            self.rrate = self.params['rrate'].value
        #
        Cf = self.cov_wts * self.pop.covs[1][:, it] * (C + I * self.dprob)
        erate = self.pop.covs[0][:, it] * (
            self.eps * self.pop.covs[2][:, it] * R + self.cov_wts * (
                self.vareps + self.beta * Cf * self.pop.Ksp))  # order matters!
        Erate = (self.pop.hidx - E - C - I) * erate
        assert np.all(Erate >= 0)
        self.cErate = Erate.cumsum()
        #
        Crate = E * self.gamma
        assert np.all(Crate >= 0)
        self.cCrate = Crate.cumsum()
        #
        Irate = C * self.drate
        assert np.all(Irate >= 0)
        self.cIrate = Irate.cumsum()
        # main wheat back to Susceptible during summers
        Srate = I * (self.pop.covs[2][:, it] > 0)
        assert np.all(Srate >= 0)
        self.cSrate = Srate.cumsum()
        # infection to reservoir
        Rrate = (self.pop.hidx - R) * (C + I * self.dprob)
        assert np.all(Rrate >= 0)
        self.cRrate = Rrate.cumsum()
        # reservoir removal as main wheat stay healthy
        Sr_rate = self.rrate * R * (1 - self.pop.covs[2][:, it])
        assert np.all(Sr_rate >= 0)
        self.cSr_rate = Sr_rate.cumsum()
        #
        return np.cumsum([
            self.cErate[-1], self.cCrate[-1], self.cIrate[-1],
            self.cSrate[-1], self.cRrate[-1], self.cSr_rate[-1]])

    def update_state(self, state, crate, t):
        ei = np.argmax(crate > nr.rand() * crate[-1])
        if ei == 0:
            'main S->E'
            ind = np.argmax(self.cErate > nr.rand() * self.cErate[-1])
        elif ei == 1:
            'main E->C'
            ind = np.argmax(self.cCrate > nr.rand() * self.cCrate[-1])
        elif ei == 2:
            'main C->I'
            ind = np.argmax(self.cIrate > nr.rand() * self.cIrate[-1])
        elif ei == 3:
            'main I->S'
            ind = np.argmax(self.cSrate > nr.rand() * self.cSrate[-1])
        elif ei == 4:
            'reservoir S->I'
            ind = np.argmax(self.cRrate > nr.rand() * self.cRrate[-1])
        elif ei == 5:
            'reservoir I->S'
            ind = np.argmax(self.cSr_rate > nr.rand() * self.cSr_rate[-1])
        state[:, ind] += self.rupdate[ei]
        return state, [ind, ei]


class PosteriorModel(SECI.PosteriorModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def build_nodes(self, init_trajs):
        if len(init_trajs) == 2:
            Einit, Cinit = None, None
            Iinit, Rinit = init_trajs
            # Rinit to be simulated given Iinit
        elif len(init_trajs) == 4:
            Einit, Cinit, Iinit, Rinit = init_trajs
        else:
            assert False, len(init_trajs)
        #
        Enodes, Cnodes, Inodes, Rnodes = [], [], [], []
        Dnodes = []
        data_nodes = []
        for j in xrange(self.pop.n_pop):
            data = self.data.get_nodedata(j)  # list
            data_nodes += [data[0]]
            smax = Iinit[j][1][-1] if self.opts.fsize_known else None
            #
            Iprior = INode(self.pop, j, smax, data, None)
            Ipi = np.zeros(Iprior.n_state)
            Ipi[int(Iinit[j][1][0])] = 1
            Inodes += [hyper.Trajectory(Iinit[j], Iprior, Ipi)]
            #
            Cprior = CNode(self.pop, j, smax, None, None)
            Cpi = poisson.pmf(Cprior.space[:, 1], self.init.prior[j, 1])
            sigma = self.params['sigma'].expand_by_time(
                self.pop.base[0][j], self.pop)
            Cval = util.move_traj_back(
                Iinit[j], sigma, self.pop.tvec)\
                if Cinit is None else Cinit[j]
            Cnodes += [hyper.Trajectory(Cval, Cprior, Cpi)]
            #
            Eprior = ENode(self.pop, j, smax, None, None)
            Epi = poisson.pmf(Eprior.space[:, 1], self.init.prior[j, 0])
            gamma = self.params['gamma'].value
            gamma = gamma.mean() if self.params['gamma'].ncells > 1 else gamma
            Eval = util.move_traj_back(Cval, gamma, self.pop.tvec)\
                if Einit is None else Einit[j]
            Enodes += [hyper.Trajectory(Eval, Eprior, Epi)]
            #
            Rprior = RNode(self.pop, j, smax, None, None)
            Rpi = poisson.pmf(Rprior.space[:, 1], self.init.prior[j, 2])
            Rnodes += [hyper.Trajectory(Rinit[j], Rprior, Rpi)]
            #
            dnode = data_nodes[j]
            if (dnode.traj[1] > 0).sum() == 0:
                t1 = self.pop.tvec[-1] + 1
            else:
                t1 = dnode.traj[0][np.argmax(dnode.traj[1] > 0)]
            if t1 < self.pop.tvec[0]:
                Dt = self.pop.tvec[[0, -1]]
                Ds = np.ones(2)
            elif t1 < self.pop.tvec[-1]:
                Dt = np.array([self.pop.tvec[0], t1, self.pop.tvec[-1]])
                Ds = np.array([0, 1, 1])
            else:
                Dt = self.pop.tvec[[0, -1]]
                Ds = np.zeros(2)
            Dnodes += [hyper.Trajectory(np.vstack([Dt, Ds]), None, None)]
        self.nodes = {
            'Exposed': Enodes, 'Cryptic': Cnodes, 'Symptomatic': Inodes,
            'Reservoir': Rnodes}
        self.Dnodes = Dnodes
        self.data_nodes = data_nodes
        for j in xrange(self.pop.n_pop):
            Enodes[j].prior.set_blanket(self.nodes)
            Cnodes[j].prior.set_blanket(self.nodes)
            Inodes[j].prior.set_blanket(self.nodes)
            Rnodes[j].prior.set_blanket(self.nodes)

    def init_node_update(self, jratio=1.1, obs_dt=1):
        assert False, "not implemented yet"

    def joint_logl(self, alpha=None, mu=None, eps=None, vareps=None):
        n = self.pop.n_pop
        if alpha is None and mu is None:
            Ksp, Kfull, Kloc = self.pop.Ksp, self.pop.Kfull, self.pop.Kloc
        elif mu is None:
            Ksp, Kfull, Kloc = self.pop.compute_kernel(
                alpha, self.params['mu'].value, self.opts.dkernel,
                self.opts.freq_depend, self.with_Kfull, self.opts.knorm)
        elif alpha is None:
            Ksp, Kfull, Kloc = self.pop.compute_kernel(
                None, mu, self.opts.dkernel,
                self.opts.freq_depend, self.with_Kfull, self.opts.knorm)
        if eps is None:
            eps = self.params['epsilon'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(eps):
            eps = eps[:, self.pop.base[1]][self.pop.base[0], :]
        if vareps is None:
            vareps = self.params['varepsilon'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(vareps):
            vareps = vareps[:, self.pop.base[1]][self.pop.base[0], :]
        #
        nodes = self.nodes['Exposed'] + self.nodes['Cryptic'] +\
            self.nodes['Symptomatic'] + self.nodes['Reservoir']
        eps = np.ones((n, self.pop.n_dt)) * eps if np.isscalar(eps)\
            else eps
        vareps = np.ones((n, self.pop.n_dt)) * vareps if np.isscalar(vareps)\
            else vareps
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((n, self.pop.n_dt)) * beta if np.isscalar(beta)\
            else beta
        gamma = self.params['gamma'].expand_by_time(self.pop.base[0], self.pop)
        gamma = np.ones((n, self.pop.n_dt)) * gamma if np.isscalar(gamma)\
            else gamma
        drate = self.params['drate'].expand_by_time(self.pop.base[0], self.pop)
        drate = np.ones((n, self.pop.n_dt)) * drate if np.isscalar(drate)\
            else drate
        rrate = self.params['rrate'].expand_by_time(self.pop.base[0], self.pop)
        rrate = np.ones((n, self.pop.n_dt)) * drate if np.isscalar(rrate)\
            else rrate

        def rate_fn(inp, i, save):
            pass
        pop_dyns = [self.pop.tvec, np.arange(self.pop.n_dt)]
        logl = util.explogp_merge(rate_fn, nodes, pop_dyns)
        print "Without data", logl
        #
        n_node = 1
        for j in xrange(self.pop.n_pop):
            Inode = self.nodes['Symptomatic'][j]
            data_list = Inode.prior.data
            for k, data in enumerate(data_list):

                def logp_fn(inp, i, t, tp):
                    r, it = inp
                    ri = Inode.prior.encode_state(r)
                    if i == n_node:
                        # data observed
                        return data.segment_logp(it, ri, t, tp)
                    else:
                        return data.segment_logp(None, ri, t, tp)
                Dt = data.traj[0, :]
                data.set_probs(Inode.prior.space[:, 1:],
                               self.params['dprob'].value[k, :], self.pop)
                logl += util.data_merge(
                    logp_fn, [Inode], self.pop.tvec[[0, -1]],
                    [Dt, np.arange(len(Dt)) + 1])[0]

    def beta_gibbs(self):
        pass


class ENode(struct.NodePostModel):

    def get_genrate(self, params):
        pass

    def get_outlogp(self, params, W):
        pass


class CNode(struct.NodePostModel):

    def get_genrate(self, params):
        pass

    def get_outlogp(self, params, W):
        pass


class INode(struct.NodePostModel):

    def get_genrate(self, params):
        pass

    def get_outlogp(self, params, W):
        pass


class RNode(struct.NodePostModel):

    def get_genrate(self, params):
        pass

    def get_outlogp(self, params, W):
        pass
