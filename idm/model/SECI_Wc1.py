from idm.model import struct, util, hyper, SECI

import numpy as np
import numpy.random as nr
from scipy.stats import poisson

name = "SECI_W"
n_comp, iI, iW = 4, 2, 3
compartments = ['Exposed', 'Cryptic', 'Symptomatic', 'InfectedVector']
param_list = [
    'epsilon', 'beta', 'alpha', 'mu', 'varepsilon',  # transmission params (S->E)
    'eta',    # control efficiency
    'gamma',  # rate of onset of infectiousness (E->C)
    'sigma',  # rate of emergence of symptoms (C->I)
    'dprob',  # detection probability per infected-unit-host
    'delta',  # vector movement ratio
]
rupdate = [
    np.array([1, 0, 0, 0]),   # Exposed
    np.array([-1, 1, 0, 0]),  # Infectious
    np.array([0, -1, 1, 0]),  # Symptomatic
    np.array([0, 0, 0, 1]),   # InfectedVector
]


class PriorModel(struct.PopModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate

    def get_rate(self, state, it, param_update):
        """
            E, C, I, V = INSTANT number of exposeds, cryptics, symptomatics,
                         infected vectors
            it = transition period index
        """
        E, C, I, V = state
        assert np.all(E + C + I <= self.pop.hidx), zip(
            E + C + I, self.pop.hidx)
        if param_update:
            self.eps = self.params['epsilon'].expand_by_space(it, self.pop)
            self.vareps = self.params['varepsilon'].expand_by_space(
                it, self.pop)
            self.beta = self.params['beta'].expand_by_space(it, self.pop)
            self.gamma = self.params['gamma'].expand_by_space(it, self.pop)
            self.sigma = self.params['sigma'].expand_by_space(it, self.pop)
            self.delta = self.params['delta'].expand_by_space(it, self.pop)
            # quarantines
            Qsp, Qz = None, None
            for qr in self.quarantines:
                qk, qn = qr.get_wts(it)
                if qk is None:
                    break
                if Qsp is None:
                    Qsp, Qz = qk, qn
                else:
                    Qsp.data -= (Qsp.data - qk.data).clip(min=0)
            if Qsp is not None:
                assert np.all(Qsp.data >= 0) and np.all(Qsp.data <= 1)
                self.pop.Ksp, self.pop.Kfull, self.pop.Kloc = self.pop.compute_kernel(
                    None, self.opts.dkernel,
                    self.opts.freq_depend, False, self.opts.knorm, Qsp)
        #
	eprop = self.pop.covs[1][:, it] * (C + I) * self.pop.Ksp  # order matters!
        erate = self.eps + self.pop.covs[0][:, it] * V * (self.vareps + self.beta * eprop) 
        Erate = (self.pop.hidx - E - C - I) * erate
        self.cErate = Erate.cumsum()
        #
        Crate = E * self.gamma
        self.cCrate = Crate.cumsum()
        #
        Irate = C * self.sigma
        self.cIrate = Irate.cumsum()
        #
        vrate = self.delta * (
            erate - self.eps + self.pop.covs[0][:, it] * (C + I) * self.pop.Kloc)
        Vrate = (self.pop.hidx - V) * vrate
        self.cVrate = Vrate.cumsum()
        return np.cumsum([self.cErate[-1], self.cCrate[-1],
                          self.cIrate[-1], self.cVrate[-1]])

    def update_state(self, state, crate):
        ei = np.argmax(crate > nr.rand() * crate[-1])
        if ei == 0:
            'Infection: S->E'
            ind = np.argmax(self.cErate > nr.rand() * self.cErate[-1])
        elif ei == 1:
            'Infectious: E->C'
            ind = np.argmax(self.cCrate > nr.rand() * self.cCrate[-1])
        elif ei == 2:
            'Symptomatic: C->I'
            ind = np.argmax(self.cIrate > nr.rand() * self.cIrate[-1])
        elif ei == 3:
            ind = np.argmax(self.cVrate > nr.rand() * self.cVrate[-1])
        state[:, ind] += self.rupdate[ei]
        return state, [ind, ei]


class PosteriorModel(SECI.PosteriorModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def build_nodes(self, init_trajs):
        if len(init_trajs) == 2:
            Einit, Cinit = None, None
            Iinit, Vinit = init_trajs
        elif len(init_trajs) == 4:
            Einit, Cinit, Iinit, Vinit = init_trajs
            print len(Vinit), len(Iinit)
        else:
            assert False, len(init_trajs)
        #
        Enodes, Cnodes, Inodes, Vnodes = [], [], [], []
        nA = 0
        for j in xrange(self.pop.n_pop):
            data = self.data.get_nodedata(j)  # list
            dataV = self.dataV.get_nodedata(j)
            smax = None            
            #
            Iprior = INode(self.pop, j, smax, data, None)
            Ipi = np.zeros(Iprior.n_state)
            Ipi[int(Iinit[j][1][0])] = 1
            Inodes += [hyper.Trajectory(Iinit[j], Iprior, Ipi)]
            #
            Cprior = CNode(self.pop, j, smax, None, None)
            Cpi = poisson.pmf(Cprior.space[:, 1], self.init.prior[j, 1])
            sigma = self.params['sigma'].expand_by_time(
                self.pop.base[0][j], self.pop)
            Cval = util.move_traj_back(
                Iinit[j], sigma, self.pop.tvec)\
                if Cinit is None else Cinit[j]
            Cnodes += [hyper.Trajectory(Cval, Cprior, Cpi)]
            #
            Eprior = ENode(self.pop, j, smax, None, None)
            Epi = poisson.pmf(Eprior.space[:, 1], self.init.prior[j, 0])
            gamma = self.params['gamma'].value.mean()
            Eval = util.move_traj_back(Cval, gamma, self.pop.tvec)\
                if Einit is None else Einit[j]
            Enodes += [hyper.Trajectory(Eval, Eprior, Epi)]
            #
            Vprior = VNode(self.pop, j, smax, dataV, None)
            Vpi = np.zeros(Vprior.n_state)
            # TODO: change quick fix
            Vinit[j][1][0] = 1 if Vinit[j][1][0] > 1 else Vinit[j][1][0]
            if len(Eval[0]) > 2:
                if len(Vinit[j][0]) == 2 or Vinit[j][0][1] > Eval[0][1]:
                    Vinit[j] = Eval * 1.
                    Vinit[j][0][1] -= nr.rand()
                    nA += 1
            elif Eval[1][0] > 0:
                Vinit[j] = Eval * 1.
                nA += 1
            
            Vpi[int(Vinit[j][1][0])] = 1
            Vnodes += [hyper.Trajectory(Vinit[j], Vprior, Vpi)]
        print nA, "Vnodes adjusted"
        self.nodes = {
            'Exposed': Enodes, 'Cryptic': Cnodes, 'Symptomatic': Inodes,
            'InfectedVector': Vnodes}
        for j in xrange(self.pop.n_pop):
            Enodes[j].prior.set_blanket(self.nodes)
            Cnodes[j].prior.set_blanket(self.nodes)
            Inodes[j].prior.set_blanket(self.nodes)
            Vnodes[j].prior.set_blanket(self.nodes)
        #
        Dnodes = []
        for j in xrange(self.pop.n_pop):
            Dnodes += [self.data.get_nodedata(j)[0]]
        self.Dnodes = Dnodes

    def init_node_update(self, jratio=1.1, obs_dt=1):
        print "Vnodes ..."
        for j in self.dataV.pos_inds:
            Vnode = self.nodes['InfectedVector'][j]
            t_pos = Vnode.prior.data[0].t_1st_pos()
            assert t_pos is not None
            Vt, Vs = Vnode.value
            if t_pos < Vt[0]:
                continue
            if Vs[-1] == 0 or Vt[np.argmax(Vs > 0)] > t_pos:
                '''print "Node", j
                print "Before",
                print "V", zip(Vt, Vs),
                print "C", zip(Cnode.value[0, :], Cnode.value[1, :])'''
                # adjust Vnode to be consistent with the data
                t_pos -= obs_dt * nr.rand()
                if Vs[-1] == 0:
                    Vt = np.hstack([Vt[0], t_pos, Vt[-1]])
                    Vs = np.array([0, 1, 1])
                else:
                    # push the first symptomatic time back
                    Vt[np.argmax(Vs > 0)] = t_pos
                Vnode.value = np.vstack([Vt, Vs])
                # print "After move back",
                # print "V", zip(Vt, Vs),
        print "Inodes ..."
        for j in self.inds_pos:
            Enode, Cnode, Inode, Vnode = [self.nodes[c][j]
                                          for c in compartments]
            t_pos = Inode.prior.data[0].t_1st_pos()
            assert t_pos is not None
            It, Is = Inode.value
            if t_pos < It[0]:
                continue
            '''print "Before",
            print "I", zip(It, Is),
            print "C", zip(Cnode.value[0, :], Cnode.value[1, :]),
            print "E", zip(Enode.value[0, :], Enode.value[1, :]),
            print "V", zip(Vnode.value[0, :], Vnode.value[1, :])'''
            if Is[-1] == 0 or It[np.argmax(Is > 0)] > t_pos:
                print "Node", j
                '''print "Before",
                print "I", zip(It, Is),
                print "C", zip(Cnode.value[0, :], Cnode.value[1, :]),
                print "E", zip(Enode.value[0, :], Enode.value[1, :])'''
                # adjust Inode to be consistent with the data
                t_pos -= obs_dt * nr.rand()
                if Is[-1] == 0:
                    It = np.hstack([It[0], t_pos, It[-1]])
                    Is = np.array([0, 1, 1])
                else:
                    # push the first symptomatic time back
                    It[np.argmax(Is > 0)] = t_pos
                Inode.value = np.vstack([It, Is])
                # print "After move back",
                # print "I", zip(It, Is),
                # adjust Cnode
                sigma = self.params['sigma'].expand_by_time(
                    self.pop.base[0][j], self.pop)
                Cnode.value = util.move_traj_back(
                    Inode.value, sigma, self.pop.tvec)
                # print "C", zip(Cnode.value[0, :], Cnode.value[1, :]),
                # adjust Enode
                gamma = self.params['gamma'].value.mean()
                Enode.value = util.move_traj_back(
                    Cnode.value, gamma, self.pop.tvec)
                # print "E", zip(Enode.value[0, :], Enode.value[1, :])
            # posterior updates
            # print "After update",
            Cnode.update(self.params, jratio)
            # print "C", zip(Cnode.value[0, :], Cnode.value[1, :]),
            assert Cnode.value[1, -1] > 0, Cnode.value
            Enode.update(self.params, jratio)
            # print "E", zip(Enode.value[0, :], Enode.value[1, :]),
            assert Enode.value[1, -1] > 0
            Inode.update(self.params, jratio)
            # print "I", zip(Inode.value[0, :], Inode.value[1, :])
            assert Inode.value[1, -1] > 0
            Vnode.update(self.params, jratio)

    def data_dlogl(self, dprob_q):
        # Data likelihood
        dlogl = 0
        # plant data
        n_node = 1
        for j in xrange(self.pop.n_pop):
            Inode = self.nodes['Symptomatic'][j]
            data_list = Inode.prior.data
            for k, data in enumerate(data_list):

                def logp_fn(inp, i, t, tp):
                    r, it = inp
                    ri = Inode.prior.encode_state(r)
                    if i == n_node:
                        # data observed
                        return data.segment_logp(it, ri, t, tp)
                    else:
                        return data.segment_logp(None, ri, t, tp)
                Dt = data.traj[0, :]
                data.set_probs(Inode.prior.space[:, 1:],
                               self.params['dprob'].value[k, :], self.pop)
                dlogl -= util.data_merge(
                    logp_fn, [Inode], self.pop.tvec[[0, -1]],
                    [Dt, np.arange(len(Dt)) + 1])[0]
                data.set_probs(Inode.prior.space[:, 1:], dprob_q[k, :],
                               self.pop)
                dlogl += util.data_merge(
                    logp_fn, [Inode], self.pop.tvec[[0, -1]],
                    [Dt, np.arange(len(Dt)) + 1])[0]
        return dlogl

    def joint_logl(self, alpha=None, mu=None, eps=None, vareps=None, delta=None):
        # print delta
        n = self.pop.n_pop
        if alpha is None and mu is None:
            Ksp, Kfull, Kloc = self.pop.Ksp, self.pop.Kfull, self.pop.Kloc
        elif mu is None:
            Ksp, Kfull, Kloc = self.pop.compute_kernel(
                alpha, self.params['mu'].value, self.opts.dkernel,
                self.opts.freq_depend, self.with_Kfull, self.opts.knorm)
        elif alpha is None:
            Ksp, Kfull, Kloc = self.pop.compute_kernel(
                None, mu, self.opts.dkernel,
                self.opts.freq_depend, self.with_Kfull, self.opts.knorm)
        if delta is None:
            delta = self.params['delta'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(delta):
            delta = delta[:, self.pop.base[1]][self.pop.base[0], :]
        if eps is None:
            eps = self.params['epsilon'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(eps):
            eps = eps[:, self.pop.base[1]][self.pop.base[0], :]
        if vareps is None:
            vareps = self.params['varepsilon'].expand_by_time(
                self.pop.base[0], self.pop)
        elif not np.isscalar(vareps):
            vareps = vareps[:, self.pop.base[1]][self.pop.base[0], :]
        #
        nodes = self.nodes['Exposed'] + self.nodes['Cryptic'] +\
            self.nodes['Symptomatic'] + self.nodes['InfectedVector']
        eps = np.ones((n, self.pop.n_dt)) * eps if np.isscalar(eps)\
            else eps
        vareps = np.ones((n, self.pop.n_dt)) * vareps if np.isscalar(vareps)\
            else vareps
        delta = np.ones((n, self.pop.n_dt)) * delta if np.isscalar(delta)\
            else delta
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((n, self.pop.n_dt)) * beta if np.isscalar(beta)\
            else beta
        sigma = self.params['sigma'].expand_by_time(self.pop.base[0], self.pop)
        sigma = np.ones((n, self.pop.n_dt)) * sigma if np.isscalar(sigma)\
            else sigma
        gamma = self.params['gamma'].expand_by_time(self.pop.base[0], self.pop)
        gamma = np.ones((n, self.pop.n_dt)) * gamma if np.isscalar(gamma)\
            else gamma

        def rate_fn(inp, i, save):
            E, C, I, V = inp[:n], inp[n:2*n], inp[2*n:3*n], inp[3*n:4*n]
            it = int(inp[-1])
            assert np.all(E >= C), (it, np.argwhere(E < C), E.sum(), C.sum())
            assert np.all(C >= I), (it, np.argwhere(C < I))
            assert np.all(self.pop.hidx >= E), [it, zip(self.pop.hidx, E)]
            assert np.all(self.pop.hidx >= V), [it, zip(self.pop.hidx, V)]

            def full_update():
                Cf = self.pop.covs[1][:, it] * C
                # per-capita infection rate at all sites
                erate_sec = self.pop.covs[0][:, it] * (
                    vareps[:, it] + beta[:, it] * Cf * Ksp)
                # per-capita E->C and C->I rates at all sites
                crate = gamma[:, it]
                irate = sigma[:, it]
                # vector observation prob at all sites
                vrate = delta[:, it] * (
                    erate_sec + self.pop.covs[0][:, it] * C * Kloc)                
                # sum of all rates
                rsum = ((self.pop.hidx - E) * (V * erate_sec + eps[:, it])
                        + (E - C) * crate + (C - I) * irate +
                        (self.pop.hidx - V) * vrate).sum()
                save = [erate_sec, crate, irate, vrate, rsum]
                return save

            if i is None:
                rind = None
                save = full_update()
            elif i < n:
                'E[i]: e -> e+1'
                # rate at which an individual at site i gets infected
                rind = eps[i, it] + V[i] * save[0][i]
                # change to i's population
                save[-1] += - rind + save[1][i]
            elif i < 2*n:
                j = i - n
                'C[j]: c -> c+1'
                # rate at which an individual at site j gets infectious
                rind = save[1][j]
                save[-1] += - save[1][j] + save[2][j]
                # change to j's population
                derate = 0
                save[-1] += (self.pop.hidx[j] - E[j]) * V[j] * derate
                save[0][j] += derate
                dvrate = delta[j, it] * (derate + self.pop.covs[0][j, it] * Kloc[j])
                save[-1] += (self.pop.hidx[j] - V[j]) * dvrate
                save[3][j] += dvrate
                # change to neighbouring populations
                inds = Kfull.neighbors[j]
                derate = self.pop.covs[0][inds, it] * beta[inds, it]\
                    * self.pop.covs[1][j, it] * Kfull.weights[j]
                save[-1] += ((self.pop.hidx[inds] - E[inds]) * V[inds] * derate).sum()
                save[0][inds] += derate
                dvrate = delta[inds, it] * derate
                save[-1] += ((self.pop.hidx[inds] - V[inds]) * dvrate).sum()
                save[3][inds] += dvrate
            elif i < 3*n:
                j = i - 2*n
                'I[j]: i -> i+1'
                # rate at which an individual at site j gets symptomatic
                rind = save[2][j]
                # change to j's population
                save[-1] += - save[2][j]
            elif i < 4 * n:
                j = i - 3*n
                'V[j]: v -> v+1'
                rind = save[3][j]
                save[-1] += - rind + (self.pop.hidx[j] - E[j]) * save[0][j]
            elif i == 4*n:
                'Environment dynamics'
                rind = None
                save = full_update()
            else:
                assert False, "Unexpected event"
            return rind, save, save[-1]
        pop_dyns = [self.pop.tvec, np.arange(self.pop.n_dt)]
        logl = util.explogp_merge(rate_fn, nodes, pop_dyns)
        # print "Without data", logl.shape, logl
        # plant_data
        n_node = 1
        for j in xrange(self.pop.n_pop):
            Inode = self.nodes['Symptomatic'][j]
            data_list = Inode.prior.data
            for k, data in enumerate(data_list):

                def logp_fn(inp, i, t, tp):
                    r, it = inp
                    ri = Inode.prior.encode_state(r)
                    if i == n_node:
                        # data observed
                        return data.segment_logp(it, ri, t, tp)
                    else:
                        return data.segment_logp(None, ri, t, tp)
                Dt = data.traj[0, :]
                data.set_probs(Inode.prior.space[:, 1:],
                               self.params['dprob'].value[k, :], self.pop)
                logl += util.data_merge(
                    logp_fn, [Inode], self.pop.tvec[[0, -1]],
                    [Dt, np.arange(len(Dt)) + 1])[0]
        # print "With data", logl
        if alpha is not None or mu is not None:
            return logl, (Ksp, Kfull, Kloc)
        else:
            return logl

    def beta_gibbs(self):
        nodes = self.nodes['Exposed'] + self.nodes['Cryptic'] +\
                self.nodes['InfectedVector']
        n = self.pop.n_pop
        eps = self.params['epsilon'].expand_by_time(self.pop.base[0], self.pop)
        eps = np.ones((self.pop.n_pop, self.pop.n_dt)) * eps\
            if np.isscalar(eps) else eps
        vareps = self.params['varepsilon'].expand_by_time(
            self.pop.base[0], self.pop)
        vareps = np.ones((self.pop.n_pop, self.pop.n_dt)) * vareps\
            if np.isscalar(vareps) else vareps
        beta = self.params['beta'].expand_by_time(self.pop.base[0], self.pop)
        beta = np.ones((self.pop.n_pop, self.pop.n_dt)) * beta\
            if np.isscalar(beta) else beta
        delta = self.params['delta'].expand_by_time(self.pop.base[0], self.pop)
        delta = np.ones((self.pop.n_pop, self.pop.n_dt)) * delta\
            if np.isscalar(delta) else delta

        def rate_fn(inp, i, save):
            E, C, V, it = inp[:n], inp[n:2*n], inp[2*n:3*n], int(inp[-1])
            assert np.all(E >= C), (it, np.argwhere(E < C))
            assert np.all(self.pop.hidx >= E), [it, zip(self.pop.hidx, E)]
            assert np.all(self.pop.hidx >= V), [it, zip(self.pop.hidx, V)]

            def full_update():
                Cf = self.pop.covs[1][:, it] * C
                # per-capita secondary infection propensity
                eprop = Cf * self.pop.Ksp
                # per-capita secondary infection hazard at all sites
                ehazd = self.pop.covs[0][:, it] * eprop
                # hazard to invasion of infected vectors
                vhazd = delta[:, it] * ehazd
                # sum of secondary infection hazards by spatial zones
                hsum = self.params['beta'].collapse_by_space(
                    (self.pop.hidx - E) * V * ehazd + (self.pop.hidx - V) * vhazd,
                    self.pop)
                save = [ehazd, vhazd, hsum]
                return save

            if i is None:
                event = None
                save = full_update()
            elif i < n:
                'E[i]: e -> e+1'
                # rate at which an individual at site i gets infected
                rate2nd = beta[i, it] * V[i] * save[0][i]
                rate1st = eps[i, it] + self.pop.covs[0][i, it] * V[i] * vareps[i, it]
                iz = self.pop.base[0][i]
                # primary vs. secondary infection
                secondary = False
                if nr.rand() < rate2nd / (rate1st + rate2nd):
                    secondary = True
                event = [secondary, rate1st, save[0][i], iz]
                # changes due to the event
                if np.isscalar(save[-1]):
                    save[-1] -= save[0][i]
                else:
                    save[-1][iz] -= save[0][i]
            elif i < 2*n:
                j = i - n
                'C[j]: c -> c+1'
                event = None
                # changes due to an individual at site j gets infectious
                dhsum_all = np.zeros(self.pop.n_pop)
                # to j's population
                dehazd = 0
                dvhazd = delta[j, it] * dehazd
                dhsum_all[j] = (self.pop.hidx[j] - E[j]) * V[j] * dehazd +\
                               (self.pop.hidx[j] - V[j]) * dvhazd
                save[0][j] += dehazd
                save[1][j] += dvhazd
                # to neighbouring populations
                inds = self.pop.Kfull.neighbors[j]
                dehazd = self.pop.covs[0][inds, it] * self.pop.covs[1][j, it]\
                    * self.pop.Kfull.weights[j]
                dvhazd = delta[inds, it] * dehazd
                dhsum_all[inds] = (self.pop.hidx[inds] - E[inds]) * V[inds] * dehazd +\
                                  (self.pop.hidx[inds] - V[inds]) * dvhazd
                save[0][inds] += dehazd
                save[1][inds] += dvhazd
                save[-1] += self.params['beta'].collapse_by_space(
                    dhsum_all, self.pop)
            elif i < 3*n:
                j = i - 2 * n
                'W[j]: w -> w+1'
                # rate at which site j gets exposed to infected vectors
                rate2nd = beta[j, it] * save[1][j]
                rate1st = delta[j, it] * self.pop.covs[0][j, it] * (
                    vareps[j, it] + C[j] * self.pop.Kloc[j]) 
                iz = self.pop.base[0][j]
                # primary vs. secondary infection
                secondary = False
                if nr.rand() < rate2nd / (rate1st + rate2nd):
                    secondary = True
                event = [secondary, rate1st, save[1][j], iz]
                # changes due to the event
                if np.isscalar(save[-1]):
                    save[-1] += - save[1][j] + (self.pop.hidx[j] - E[j]) * save[0][j]
                else:
                    save[-1][iz] += - save[1][j] + (self.pop.hidx[j] - E[j]) * save[0][j]
            elif i == 3*n:
                'Environment dynamics'
                event = None
                save = full_update()
            else:
                assert False, "Unexpected event"
            return event, save, save[-1]

        if self.params['beta'].bins is None:
            shape, hagg, r1, h2 = util.beta_merge_no_bins(
                rate_fn, nodes, self.pop)
        else:
            shape, hagg, r1, h2, bit, biz = util.beta_merge_with_bins(
                rate_fn, nodes, self.pop)

        def get_betaval():
            beta = self.params['beta'].value * 1.
            if np.isscalar(beta):
                beta *= np.ones_like(hagg)
            return beta
        beta_old = get_betaval()
        a, b = self.params['beta'].prior
        # print a + shape, b + hagg
        self.params['beta'].update([a + shape, b + hagg])
        beta_new = get_betaval()
        if self.params['beta'].bins is None:
            dlogl = - hagg * (beta_new - beta_old) +\
                (np.log(r1 + h2 * beta_new) -
                 np.log(r1 + h2 * beta_old)).sum()
        else:
            dlogl = - (hagg * (beta_new - beta_old)).sum() +\
                (np.log(r1 + h2 * beta_new[bit][biz]) -
                 np.log(r1 + h2 * beta_old[bit][biz])).sum()
        return dlogl


class ENode(struct.NodePostModel):
    """
    Cumulative Exposed (non-infectious, non-symptomatic)
    """
    def get_genrate(self, params):
        """
        Rate at which the node increases its state by 1
        Parents = Cnodes[neighbors + self.ind], Vnodes[self.ind]
        """
        pinds = self.pop.Kfull.neighbors[self.ind]
        parents = [self.nodes['Cryptic'][pi] for pi in pinds]
        parents += [self.nodes['Cryptic'][self.ind]]
        parents += [self.nodes['InfectedVector'][self.ind]]
        eps = params['epsilon'].expand_by_time(self.iz, self.pop)
        eps = np.ones(self.pop.n_dt) * eps if np.isscalar(eps) else eps
        vareps = params['varepsilon'].expand_by_time(self.iz, self.pop)
        vareps = np.ones(self.pop.n_dt) * vareps if np.isscalar(vareps)\
            else vareps
        beta = params['beta'].expand_by_time(self.iz, self.pop)
        beta = np.ones(self.pop.n_dt) * beta if np.isscalar(beta) else beta

        def rate_fn(inp):
            C, c_this, v_this, it = inp[:-3], inp[-3], inp[-2], int(inp[-1])
            Cf = self.pop.covs[1][pinds, it] * C
            Cf_this = self.pop.covs[1][self.ind, it] * c_this
            # per-capita infection rate at the local site
            irate = eps[it] + self.pop.covs[0][self.ind, it] * v_this * (
                vareps[it] + beta[it] * np.inner(
                    self.pop.Kfull.weights[self.ind], Cf))
            return (self.n_host - self.space[:, 1:]) * irate
        rates, tvec = util.rate_merge(
            rate_fn, parents, [self.pop.tvec, np.arange(self.pop.n_dt)])
        assert rates.shape == (self.n_state, len(tvec)-1)
        return [rates], tvec

    def get_outlogp(self, params, W):
        """
        Child = Cnodes[self.ind]
        Other Parents of Children = None
        """
        gamma = params['gamma'].expand_by_time(self.iz, self.pop)
        gamma = np.ones(self.pop.n_dt) * gamma if np.isscalar(gamma) else gamma
        Ct, Cs = self.nodes['Cryptic'][self.ind].value

        def logpi_fn(inp):
            ip, ic = inp.astype(int)
            logp = np.zeros(self.n_state)
            logp[self.space[:, 1] < Cs[ic]] = - np.inf
            return logp

        def rate_fn(inp, ni, di, save):
            """
            inp = [pop.tvec_i, Ct_i]
            ni = node index (\in\{None\} here)
            di = dyns index (\in\{0,1\} here)
            """
            ip, ic = inp.astype(int)

            def full_update():
                # rate at which a C event happens
                crate = gamma[ip] * (self.space[:, 1] > Cs[ic])
                # total rate of all C events
                rsum = (self.space[:, 1] - Cs[ic]) * crate
                return [crate, rsum]

            if di is None or di == 0:
                # at t0 or  pop.tvec_t
                rind = None
            else:
                rind = save[0]
            save = full_update()
            return rind, save, save[-1]
        logp = util.outlogp_merge(rate_fn, logpi_fn, [], W, self.pop.tvec, Ct)
        assert logp.shape == (self.n_state, len(W) - 1), (
            logp.shape, (self.n_state, len(W) - 1))
        return logp


class CNode(struct.NodePostModel):
    """
    Cumulative Cryptic (infectious, but not symptomatic)
    """

    def get_genrate(self, params):
        """
        Parent = Enodes[self.ind]
        """
        def rate_fn(inp):
            e, gam = inp
            return gam * (e - self.space[:, 1:]) * (e > self.space[:, 1:])
        parent = self.nodes['Exposed'][self.ind]
        gamma = params['gamma'].expand_by_time(self.iz, self.pop)
        if np.isscalar(gamma):
            rates = rate_fn([parent.value[1][:-1], gamma])
            tvec = parent.value[0]
        else:
            rates, tvec = util.rate_merge(
                rate_fn, [parent], [self.pop.tvec, gamma])
        assert rates.shape == (self.n_state, len(tvec)-1)
        return [rates], tvec

    def get_outlogp(self, params, W):
        """
        Children = Inodes[self.ind], Enodes[neighbors + self.ind],
                   Vnodes[neighbors + self.ind]
        Other Parents of Children = Cnodes[neighbors of neighbors + neighbors]
        """
        ind_c = np.append(self.pop.Kfull.neighbors[self.ind], self.ind)
        Ec = [self.nodes['Exposed'][j] for j in ind_c[:-1]]
        ind_mb = []
        for j in ind_c:
            ind_mb = np.hstack([ind_mb, self.pop.Kfull.neighbors[j]])
        ind_mb = np.unique(ind_mb).astype(int)
        ind_mb = ind_mb[ind_mb != self.ind]
        Cmb = [self.nodes['Cryptic'][j] for j in ind_mb]
        Vc = [self.nodes['InfectedVector'][j] for j in ind_c]
        #
        parT, parS = self.nodes['Exposed'][self.ind].value
        nodes = [self.nodes['Symptomatic'][self.ind]] + Ec + Cmb + Vc
        n = np.cumsum([1, len(ind_c) - 1, len(ind_mb), len(ind_c)])
        eps = params['epsilon'].expand_by_time(
            self.pop.base[0][ind_c], self.pop)
        eps = np.ones((len(ind_c), self.pop.n_dt)) * eps\
            if np.isscalar(eps) else eps
        vareps = params['varepsilon'].expand_by_time(
            self.pop.base[0][ind_c], self.pop)
        vareps = np.ones((len(ind_c), self.pop.n_dt)) * vareps\
            if np.isscalar(vareps) else vareps
        beta = params['beta'].expand_by_time(
            self.pop.base[0][ind_c], self.pop)
        beta = np.ones((len(ind_c), self.pop.n_dt)) * beta\
            if np.isscalar(beta) else beta
        sigma = params['sigma'].expand_by_time(self.iz, self.pop)
        sigma = np.ones(self.pop.n_dt) * sigma if np.isscalar(sigma) else sigma
        delta = params['delta'].expand_by_time(
            self.pop.base[0][ind_c], self.pop)
        delta = np.ones((len(ind_c), self.pop.n_dt)) * delta\
            if np.isscalar(delta) else delta
        #

        def logpi_fn(inp):
            logp = np.zeros(self.n_state)
            Ii = inp[0]
            logp[self.space[:, 1] < Ii] = -np.inf
            ie = int(inp[-1])
            logp[self.space[:, 1] > parS[ie]] = -np.inf
            return logp

        def rate_fn(inp, ni, di, save):
            """
            inp = [Inode_state, [Enode_state], [Cnode_state],
                   pop.tvec_i, thisEnode_i],
            ni = node index (\in\{0, ..., n_node - 1\} here)
            di = dyns index (\in\{0, 1\} here)
            """
            Ii, Ec0, Cmb, Vc = inp[0], inp[1:n[1]], inp[n[1]:n[2]], inp[
                n[2]:n[3]]
            it, ie = int(inp[n[2]]), int(inp[-1])
            Ec = np.append(Ec0, parS[ie])
            assert np.all(self.pop.hidx[ind_c] >= Ec), [
                it, zip(self.pop.hidx[ind_c], Ec)]
            C = np.zeros(self.pop.n_pop)
            C[ind_mb] = Cmb

            def full_update():
                Cf = self.pop.covs[1][:, it] * C
                eprop = np.zeros((self.n_state, len(ind_c)))
                # infection pressure BY other sites
                eprop += (Cf * self.pop.Ksp)[ind_c][None, :]
                # infection pressure BY this site
                eprop[:, :-1] += self.pop.covs[1][self.ind, it] *\
                    self.space[:, 1:] * self.pop.Kfull.weights[self.ind]
                eprop[:, -1] += 0
                # per-capita infection pressure
                erate_sec = self.pop.covs[0][ind_c, it] * (
                    vareps[:, it] + beta[:, it] * eprop)
                assert erate_sec.shape == (self.n_state, len(ind_c))
                # infectious propensity of this site
                irate = (self.space[:, 1] > Ii) * sigma[it]
                # per-capita pressure for infected vector
                vrate = delta[:, it] * (
                    erate_sec + self.pop.covs[0][ind_c, it] * C[ind_c] * self.pop.Kloc[ind_c])                
                # sum of all rates
                rsum = (
                    (self.pop.hidx[ind_c] - Ec) * (Vc * erate_sec + eps[:, it]) +
                    (self.pop.hidx[ind_c] - Vc) * vrate).sum(1) + (
                        self.space[:, 1] - Ii) * irate
                assert rsum.shape[0] == self.n_state
                return [erate_sec, irate, vrate, rsum]

            if ni is None and di is None:
                # at t0
                rind = None
                save = full_update()
            elif ni is not None:
                # at node_t
                if ni == 0:
                    'I[self.ind]: i -> i + 1'
                    rind = save[1] * 1.
                    # changes to irate[:]
                    save[1] = (self.space[:, 1] > Ii) * sigma[it]
                    save[-1] += save[1] - rind
                elif ni < n[1]:
                    j = ni - 1
                    'Ec[j]: e -> e + 1'
                    rind = Vc[j] * save[0][:, j] + eps[j, it]
                    # changes to erate[:, j]
                    save[-1] -= rind
                    save[0][:, j] = 0.
                elif ni < n[2]:
                    j = ind_mb[ni - n[1]]
                    'C[j]: c -> c + 1'
                    rind = None
                    # changes to erate[:, :]
                    deprop = np.zeros(self.pop.n_pop)
                    deprop[j] = 0
                    inds = self.pop.Kfull.neighbors[j]
                    deprop[inds] =\
                        self.pop.covs[1][j, it] * self.pop.Kfull.weights[j]
                    derate = self.pop.covs[0][ind_c, it] * beta[:, it] *\
                        deprop[ind_c]
                    save[0] += np.tile(derate, (self.n_state, 1))
                    save[-1] += ((self.pop.hidx[ind_c] - Ec) * Vc *
                                 derate).sum()
                    dvprop = np.zeros(self.pop.n_pop)
                    dvprop[j] = self.pop.covs[0][j, it] * self.pop.Kloc[j]
                    dvrate = delta[:, it] * (derate + dvprop[ind_c])
                    save[2] += np.tile(dvrate, (self.n_state, 1))
                    save[-1] += ((self.pop.hidx[ind_c] - Vc) *
                                 dvrate).sum()
                elif ni < n[3]:
                    j = ni - n[2]
                    'Vc[j]: v -> v + 1'
                    rind = save[2][:, j] * 1.
                    # changes to vrate[:, j]
                    save[-1] += - rind + save[0][:, j]
                    save[2][:, j] = 0.
            elif di is not None:
                if di == 0:
                    # at pop.tvec_t
                    rind = None
                    save = full_update()
                elif di == 1:
                    # at thisEnode_t
                    'Ec[-1]: e -> e + 1'
                    rind = Vc[-1] * save[0][:, -1] + eps[-1, it]
                    # changes to erate[:, -1]
                    save[-1] -= rind
                    save[0][:, -1] = 0.
            return rind, save, save[-1]
        logp = util.outlogp_merge(rate_fn, logpi_fn, nodes, W,
                                  self.pop.tvec, parT)
        assert logp.shape == (self.n_state, len(W) - 1), (
            logp.shape, (self.n_state, len(W) - 1))
        return logp


class INode(struct.NodePostModel):
    """
    Cumulative Infected (infectious, symptomatic)
    """
    def get_genrate(self, params):
        """
        Parent = Cnodes[self.ind]
        """
        def rate_fn(inp):
            c, sig = inp
            return sig * (c - self.space[:, 1:]) * (c > self.space[:, 1:])
        parent = self.nodes['Cryptic'][self.ind]
        sigma = params['sigma'].expand_by_time(self.iz, self.pop)
        if np.isscalar(sigma):
            rates = rate_fn([parent.value[1][:-1], sigma])
            tvec = parent.value[0]
        else:
            rates, tvec = util.rate_merge(
                rate_fn, [parent], [self.pop.tvec, sigma])
        assert rates.shape == (self.n_state, len(tvec)-1)
        return [rates], tvec

    def get_outlogp(self, params, W):
        """
        Children = data
        Parents of Children = None
        """
        n_node = 0
        data_list = self.data
        logp = 0
        for k, data in enumerate(data_list):
            data.set_probs(self.space[:, 1:], params['dprob'].value[0, k, :],
                           self.pop)
            Dt = data.traj[0, :]

            def logp_fn(inp, i, t, tp):
                it = inp[0]
                if i == n_node:
                    # data observed
                    return data.segment_logp_space(it, t, tp)
                else:
                    return data.segment_logp_space(None, t, tp)
            logp += util.data_merge(
                logp_fn, [], W, [Dt, np.arange(len(Dt)) + 1])
            assert logp.shape == (self.n_state, len(W) - 1)
        return logp


class VNode(struct.NodePostModel):
    """
    Cumulative Infected Vectors
    """
    def get_genrate(self, params):
        """
        Rate at which the node increases its state by 1
        Parents = Cnodes[neighbors + self.ind]
        """
        pinds = self.pop.Kfull.neighbors[self.ind]
        parents = [self.nodes['Cryptic'][pi] for pi in pinds]
        parents += [self.nodes['Cryptic'][self.ind]]
        vareps = params['varepsilon'].expand_by_time(self.iz, self.pop)
        vareps = np.ones(self.pop.n_dt) * vareps if np.isscalar(vareps)\
            else vareps
        beta = params['beta'].expand_by_time(self.iz, self.pop)
        beta = np.ones(self.pop.n_dt) * beta if np.isscalar(beta) else beta
        delta = params['delta'].expand_by_time(self.iz, self.pop)
        delta = np.ones(self.pop.n_dt) * delta if np.isscalar(delta) else delta

        def rate_fn(inp):
            C, c_this, it = inp[:-2], inp[-2], int(inp[-1])
            Cf = self.pop.covs[1][pinds, it] * C
            Cf_this = self.pop.covs[1][self.ind, it] * c_this
            # per-capita infection rate at the local site
            irate = self.pop.covs[0][self.ind, it] * (
                vareps[it] + delta[it] * beta[it] * np.inner(
                    self.pop.Kfull.weights[self.ind], Cf))
            return (self.n_host - self.space[:, 1:]) * irate
        rates, tvec = util.rate_merge(
            rate_fn, parents, [self.pop.tvec, np.arange(self.pop.n_dt)])
        assert rates.shape == (self.n_state, len(tvec)-1)
        return [rates], tvec

    def get_outlogp(self, params, W):
        """
        Children = data, Enodes[self.ind]
        Parents of Children = Cnodes[neighbors of self.ind]
        """
        n_node = 0
        data_list = self.data
        logp = 0
        for k, data in enumerate(data_list):
            data.set_probs(self.space[:, 1:], params['dprob'].value[1, k, :],
                           self.pop)
            Dt = data.traj[0, :]

            def logp_fn(inp, i, t, tp):
                it = inp[0]
                if i == n_node:
                    # data observed
                    return data.segment_logp_space(it, t, tp)
                else:
                    return data.segment_logp_space(None, t, tp)
            logp += util.data_merge(
                logp_fn, [], W, [Dt, np.arange(len(Dt)) + 1])
            assert logp.shape == (self.n_state, len(W) - 1)
        #
        Et, Es = self.nodes['Exposed'][self.ind].value
        ind_nb = self.pop.Kfull.neighbors[self.ind]
        nodes = [self.nodes['Cryptic'][j] for j in ind_nb]
        n = len(ind_nb)
        eps = params['epsilon'].expand_by_time(self.pop.base[0][self.ind], self.pop)
        eps = np.ones(self.pop.n_dt) * eps if np.isscalar(eps) else eps
        vareps = params['varepsilon'].expand_by_time(self.pop.base[0][self.ind], self.pop)
        vareps = np.ones(self.pop.n_dt) * vareps if np.isscalar(vareps) else vareps
        beta = params['beta'].expand_by_time(self.pop.base[0][self.ind], self.pop)
        beta = np.ones(self.pop.n_dt) * beta if np.isscalar(beta) else beta

        def logpi_fn(inp):
            logp = np.zeros(self.n_state)
            ie = int(inp[-1])
            logp[self.space[:, 1] < Es[ie]] = -np.inf
            return logp

        def rate_fn(inp, ni, di, save):
            """
            inp = [Cnode_state, pop.tvec_i, Et_i]
            ni = node index (\in\{0, ..., n - 1\} here)
            di = dyns index (\in\{0,1\} here)
            """
            Cnb, it, ie = inp[:n], int(inp[-2]), int(inp[-1])

            def full_update():
                eprop = np.sum(self.pop.covs[1][ind_nb, it] * Cnb *
                               self.pop.Kfull.weights[self.ind])
                # rate at which an E event happens
                erate = eps[it] + self.pop.covs[0][self.ind, it] * self.space[:, 1] * (
                    vareps[it] + beta[it] * eprop)
                # total rate of all C events
                rsum = (self.pop.hidx[self.ind] - Es[ie]) * erate
                return [erate, rsum]

            if ni is None and di is None:
                # at t0
                rind = None
                save = full_update()
            elif ni is not None:
                'Cnb[ni]: c -> c + 1'
                rind = None
                # changes to erate[:, :]
                deprop = self.pop.covs[1][ind_nb[ni], it] * self.pop.Kfull.weights[self.ind][ni]
                derate = self.pop.covs[0][self.ind, it] * self.space[:, 1] * beta[it] * deprop
                save[0] += derate
                save[-1] += (self.pop.hidx[self.ind] - Es[ie]) * derate
            elif di is not None:
                if di == 0:
                    # at pop.tvec_t
                    rind = None
                    save = full_update()
                elif di == 1:
                    # at thisEnode_t
                    'e -> e + 1'
                    rind = save[0][:, -1] * 1.
                    save[-1] -= rind
                    save[0][:, -1] = 0.
            return rind, save, save[-1]
        logp += util.outlogp_merge(rate_fn, logpi_fn, nodes, W, self.pop.tvec, Et)
        assert logp.shape == (self.n_state, len(W) - 1)
        return logp
