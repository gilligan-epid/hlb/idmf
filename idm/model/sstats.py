import numpy as np
from scipy.sparse import csr_matrix


def make_moransi_correlogram(y, Dsp, binary, delta_r, N_r):
    # prepare
    if binary:
        sm = y.mean()
    else:
        z = y - y.mean()
        z2ss = (z * z).sum()
        ny = len(y)
    # make rings
    C_r = delta_r ** 2
    rs, mis, ws = [0, delta_r], [], []
    for i in xrange(1, N_r):
        # construct weight matrix
        W = Dsp.copy()
        wv = np.ones_like(W.data)
        wv[W.data <= rs[i-1]] = 0
        wv[W.data > rs[i]] = 0
        W = csr_matrix((wv, W.indices, W.indptr), shape=W.shape)
        W.eliminate_zeros()
        ws += [W.sum()]
        if binary:
            yl = W * y
            inum = (y * yl).sum()
            mis += [(inum - sm * sm) / (sm * (1 - sm) * ws[-1])]
        else:
            zl = W * z
            inum = (z * zl).sum()
            mis += [(ny * inum) / (z2ss * ws[-1])]
        rs += [C_r / rs[-1] + rs[-1]]
    return rs, mis, ws


def moransi(y, Dsp, bnds, binary=False):
    """
    y: length-n vector
    Dsp: sparse weight
    """
    w0 = Dsp.sum()
    if binary:
        sm = y.mean()
        yl = Dsp * y
        inum = (y * yl).sum()
        return (inum - sm * sm) / (sm * (1 - sm) * w0)
    else:
        n = len(y)
        z = y - y.mean()
        zl = Dsp * z
        inum = (z * zl).sum()
        z2ss = (z * z).sum()
        return (n * inum) / (z2ss * w0)
