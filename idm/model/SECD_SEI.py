from idm.model import struct

import numpy as np
import numpy.random as nr

name = "SECD_SEI"
n_comp, iI = 6, 2
compartments = ['Exposed_H', 'Cryptic_H', 'Detected_H',
                'Exposed_V', 'Cryptic_V', 'Detected_V']
param_list = [
    'epsilon', 'beta_H', 'alpha', 'mu', 'varepsilon',  # HLB transmission
    'eta',    # control efficiency
    'beta_V',  # ACP transmission
    'gamma', 'gamma_V',  # rate of onset of infectiousness (E->C/I)
    'drate_H', 'drate_V',  # detection rate (C->D)
    'dprob',  # detection probability
]
rupdate = [
    np.array([1, 0, 0, 0, 0, 0]),   # Exposed_H
    np.array([-1, 1, 0, 0, 0, 0]),  # Cryptic_H
    np.array([0, -1, 1, 0, 0, 0]),  # Detected_H
    np.array([0, 0, 0, 1, 0, 0]),   # Exposed_V
    np.array([0, 0, 0, -1, 1, 0]),  # Cryptic_V
    np.array([0, 0, 0, 0, -1, 1]),  # Detected_V
]

eps_V = 1e-5


class PriorModel(struct.PopModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.cov_wts = np.ones(self.pop.n_pop)

    def get_rate(self, state, it, param_update):
        """
            Eh, Ch, Dh = INSTANT number of HLB exposeds, cryptics, detecteds,
            Ev, Iv = number of ACP exposeds, infesteds,
            it = transition period index
        """
        Eh, Ch, Dh, Ev, Cv, Dv = state
        assert np.all(Eh + Ch + Dh <= self.pop.hidx), zip(
            Eh + Ch + Dh, self.pop.hidx)
        assert np.all(Ev + Cv + Dv <= self.pop.hidx), zip(
            Ev + Cv + Dv, self.pop.hidx)
        if param_update:
            self.eps = self.params['epsilon'].expand_by_space(it, self.pop)
            self.vareps = self.params['varepsilon'].expand_by_space(
                it, self.pop)
            self.beta_H = self.params['beta_H'].expand_by_space(it, self.pop)
            self.beta_V = self.params['beta_V'].expand_by_space(it, self.pop)
            self.gamma_H = self.params['gamma'].expand_by_space(it, self.pop)
            self.gamma_V = self.params['gamma_V'].expand_by_space(it, self.pop)
            self.drate_H = self.params['drate_H'].expand_by_space(it, self.pop)
            self.drate_V = self.params['drate_V'].expand_by_space(it, self.pop)
            self.dprob = self.params['dprob'].value
        # force of HLB exposure
        Cf = self.cov_wts * self.pop.covs[1][:, it] * (
            Ch + Dh * self.dprob) * (Cv + Dv)
        erate_H = self.eps + self.cov_wts * self.pop.covs[0][:, it] * (
            self.vareps + self.beta_H * Cf * self.pop.Ksp)  # order matters!
        Erate_H = (self.pop.hidx - Eh - Ch - Dh) * (Cv + Dv) * erate_H
        self.cErate_H = Erate_H.cumsum()
        # Eh -> Ch
        Crate_H = Eh * self.gamma_H
        self.cCrate_H = Crate_H.cumsum()
        # Ch -> Dh
        Drate_H = Ch * self.drate_H
        self.cDrate_H = Drate_H.cumsum()
        # force of ACP exposure
        If = eps_V + self.cov_wts * self.pop.covs[1][:, it] * (Cv + Dv)
        erate_V = self.cov_wts * self.pop.covs[0][:, it] * (
            self.vareps + self.beta_V * If * self.pop.Ksp)
        Erate_V = (self.pop.hidx - Ev - Cv - Dv) * erate_V
        self.cErate_V = Erate_V.cumsum()
        # Ev -> Cv
        Crate_V = Ev * self.gamma_V
        self.cCrate_V = Crate_V.cumsum()
        # Cv -> Dv
        Drate_V = Cv * self.drate_V
        self.cDrate_V = Drate_V.cumsum()
        #
        return np.cumsum([
            self.cErate_H[-1], self.cCrate_H[-1], self.cDrate_H[-1],
            self.cErate_V[-1], self.cCrate_V[-1], self.cDrate_V[-1]])

    def update_state(self, state, crate, t):
        ei = np.argmax(crate > nr.rand() * crate[-1])
        if ei == 0:
            'Infection: Sh->Eh'
            ind = np.argmax(self.cErate_H > nr.rand() * self.cErate_H[-1])
        elif ei == 1:
            'Infectious: Eh->Ch'
            ind = np.argmax(self.cCrate_H > nr.rand() * self.cCrate_H[-1])
        elif ei == 2:
            'Detected: Ch->Dh'
            ind = np.argmax(self.cDrate_H > nr.rand() * self.cDrate_H[-1])
            # quarantine
            if len(self.quarantines) > 0 and self.quarantines[0] is not None:
                self.pop.Ksp = self.quarantines[0].update_K(ind, self.pop.Ksp)
            # treatment of pathogen-detecteds
            if len(self.treatments) > 0 and self.treatments[0] is not None:
                tr = self.treatments[0]
                if tr.treset is not None and t > tr.treset:
                    # full infection pressure after treatment stopped
                    self.cov_wts = np.ones(self.pop.n_pop)
                elif tr.tstop is not None and t > tr.tstop:
                    # no treatment
                    pass
                else:
                    # treat around pathogen-positive site
                    self.cov_wts = tr.update_wts(ind, self.cov_wts)
        elif ei == 3:
            'Vector Exposed: Sv->Ev'
            ind = np.argmax(self.cErate_V > nr.rand() * self.cErate_V[-1])
        elif ei == 4:
            'Vector Infested: Ev->Cv'
            ind = np.argmax(self.cCrate_V > nr.rand() * self.cCrate_V[-1])
        elif ei == 5:
            'Vector Detected: Cv->Dv'
            ind = np.argmax(self.cDrate_V > nr.rand() * self.cDrate_V[-1])
            # quarantine
            if len(self.quarantines) > 1 and self.quarantines[1] is not None:
                self.pop.Ksp = self.quarantines[1].update_K(ind, self.pop.Ksp)
            # treatment of vector-detecteds
            if len(self.treatments) > 1 and self.treatments[1] is not None:
                tr = self.treatments[1]
                if tr.treset is not None and t > tr.treset:
                    self.cov_wts = np.ones(self.pop.n_pop)
                elif tr.tstop is not None and t > tr.tstop:
                    pass
                else:
                    self.cov_wts = tr.update_wts(ind, self.cov_wts)
        state[:, ind] += self.rupdate[ei]
        return state, [ind, ei]


class PosteriorModel(struct.PopPostModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate
        self.n_comp = n_comp
        self.compartments = compartments

    def build_nodes(self, init_trajs):
        pass
