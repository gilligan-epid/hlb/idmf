from idm.model import struct

import numpy as np
import numpy.random as nr

name = "SECI_V"
n_comp, iI, iV = 4, 2, 3
compartments = ['Exposed', 'Cryptic', 'Symptomatic', 'Vector']
param_list = [
    'epsilon', 'beta', 'alpha',  # transmission params (S->E)
    'gamma',  # rate of onset of infectiousness (E->C)
    'sigma',  # rate of emergence of symptoms (C->I)
    'dprob',  # detection probability per infected-unit-host
    'delta',  # vector movement rate
    'kappa',  #
]
rupdate = [
    np.array([1, 0, 0, 0]),   # Exposed
    np.array([-1, 1, 0, 0]),  # Infectious
    np.array([0, -1, 1, 0]),  # Symptomatic
    np.array([0, 0, 0, 1]),   # Vector
]
eps_vec = 1e-5


class PriorModel(struct.PopModel):
    def setup(self):
        self.param_list = param_list
        self.rupdate = rupdate

    def get_rate(self, state, it):
        """
            E, C, I, V = INSTANT number of exposeds, cryptics, symptomatics,
                         infected vectors
            it = transition period index
        """
        E, C, I, V = state
        assert np.all(E + C + I <= self.pop.hidx), zip(
            E + C + I, self.pop.hidx)
        assert np.all(V <= self.pop.hidx)
        eps = self.params['epsilon'].expand_by_space(it, self.pop)
        beta = self.params['beta'].expand_by_space(it, self.pop)
        gamma = self.params['gamma'].expand_by_space(it, self.pop)
        sigma = self.params['sigma'].expand_by_space(it, self.pop)
        delta = self.params['delta'].expand_by_space(it, self.pop)
        #
        Cf = self.pop.covs[1][:, it] * V * (C + I)
        erate = self.pop.covs[0][:, it] * V * (eps + beta * (
            self.pop.Ksp * Cf + self.pop.Kz * Cf) / self.pop.K0[:, it])
        Erate = (self.pop.hidx - E - C - I) * erate
        self.cErate = Erate.cumsum()
        #
        Crate = E * gamma
        self.cCrate = Crate.cumsum()
        #
        Irate = C * sigma
        self.cIrate = Irate.cumsum()
        #
        Vf = self.pop.covs[1][:, it] * V
        vrate = self.pop.covs[0][:, it] * (eps_vec + delta * beta * (
            self.pop.Ksp * Vf + self.pop.Kz * Vf) / self.pop.K0[:, it])
        Vrate = (self.pop.hidx - V) * vrate
        self.cVrate = Vrate.cumsum()
        return np.cumsum([self.cErate[-1], self.cCrate[-1],
                          self.cIrate[-1], self.cVrate[-1]])

    def update_state(self, state, crate):
        ei = np.argmax(crate > nr.rand() * crate[-1])
        if ei == 0:
            'Infection: S->E'
            ind = np.argmax(self.cErate > nr.rand() * self.cErate[-1])
        elif ei == 1:
            'Infectious: E->C'
            ind = np.argmax(self.cCrate > nr.rand() * self.cCrate[-1])
        elif ei == 2:
            'Symptomatic: C->I'
            ind = np.argmax(self.cIrate > nr.rand() * self.cIrate[-1])
        elif ei == 3:
            'Vector: V->V+1'
            ind = np.argmax(self.cVrate > nr.rand() * self.cVrate[-1])
        state[:, ind] += self.rupdate[ei]
        return state, [ind, ei]
