from idm.const import fkernel
import exception
import hyper
import util

import numpy as np
import numpy.random as nr
import copy
import pysal
import pyproj
import heapq
import cPickle as pickle
from rasterio.transform import rowcol


class Grid(object):
    def __init__(self, tf, shape, ixy, epsg=None, cscale=1, csize=None):
        self.tf = tf  # Affine transformation object
        self.shape = shape  # host.shape = (n_col, n_row)
        self.ixy = ixy  # list of non-zero cells [[xi], [yi]]
        self.epsg = epsg
        self.cscale = cscale  # scaling factor for cell-size
        #
        self.n_pos = len(self.ixy[0])
        #
        if self.tf is not None:
            x0, y0 = self.tf * (0, 0)
            xF, yF = self.tf * self.shape
            xedges = np.linspace(x0, xF, self.shape[0] + 1)
            yedges = np.linspace(y0, yF, self.shape[1] + 1)
        else:
            assert csize is not None
            xedges = np.arange(0, self.shape[0] * csize + 1, csize)
            assert len(xedges) == self.shape[0] + 1
            yedges = np.arange(0, self.shape[1] * csize + 1, csize)
        self.csize = (xedges[1] - xedges[0]) * self.cscale
        self.edges = (xedges, yedges)
        xbins = (xedges[:-1] + xedges[1:]) * .5
        ybins = (yedges[:-1] + yedges[1:]) * .5
        self.bins = (xbins, ybins)

    def get_index(self, x, y):
        yi, xi = rowcol(self.tf, x, y)
        gi = np.where((
            self.ixy[0] == xi) & (self.ixy[1] == yi))[0]
        if len(gi) == 0:
            return None, (int(xi), int(yi))
        elif len(gi) == 1:
            return gi[0], (int(xi), int(yi))
        else:
            assert False, "duplicated index found"

    def get_xy(self, xi, yi, scale=False):
        x, y = self.bins[0][xi], self.bins[1][yi]
        if scale:
            x = x * self.cscale
            y = y * self.cscale
        return x, y

    def get_grid(self, value):
        assert len(value) == self.n_pos, (len(value), self.n_pos)
        H = np.zeros(self.shape)
        H[self.ixy] = value
        return np.ma.masked_array(H, mask=H == 0).T

    def add_pos_cells(self, ixy):
        self.ixy = np.vstack([self.ixy, ixy])
        self.n_pos = len(self.ixy)

    def project_edges(self, outProj, inProj=None):
        """
        Convert raster coordinates to another projection for plotting purpose.
        """
        if inProj is None:
            assert self.epsg is not None, "No native projection was provided"
            inProj = pyproj.Proj(init='epsg:{}'.format(self.epsg))
        x0, y0 = self.edges[0][0], self.edges[1][-1]
        xedges = np.array([
            pyproj.transform(inProj, outProj, x, y0)
            for x in self.edges[0]])[:, 0]
        yedges = np.array([
            pyproj.transform(inProj, outProj, x0, y)
            for y in self.edges[1]])[:, 1]
        return [xedges, yedges]

    def project_points(self, out_epsg, xy):
        """
        Convert raster coordinates to another projection for plotting purpose.
        """
        assert self.epsg is not None, "No native projection was provided"
        inProj = pyproj.Proj(init='epsg:{}'.format(self.epsg))
        outProj = pyproj.Proj(init='epsg:{}'.format(out_epsg))
        out = [[], []]
        for i in xrange(len(xy)):
            x, y = pyproj.transform(inProj, outProj, xy[0], xy[1])
            out[0] += [x]
            out[1] += [y]
        return out


class Population(object):
    """
    A metapopulation of hosts. Fields:
        + tvec: (K+1)-length vector of time points,
             e.g. [t_start, t_c1, t_c2, t_end]
        + hdens: M-length vector of host density
        + base: space-time system of interest
            base[0] = M-length vector of spatial bin indices,
            base[1] = K-length vector of temporal bin indices,
             e.g. np.arange(K)
        + covs: covariates to be applied onto the system
            covs[0] = M-by-K matrix of susceptibility
            covs[1] = M-by-K matrix of infectivity
        + grid: spatial grid, for plotting purpose
        + dband: distance threshold
        + sw: spatial weight object
        + n_maxhost: maximum number of hosts per grid cell
    """
    def __init__(self, tvec, hdens, base, covs, grid, dband, n_maxhost,
                 cwts=None):
        self.tvec = tvec
        self.hdens = hdens
        self.base = base
        self.incovs = covs if covs is not None else [
            np.ones_like(hdens), np.ones_like(hdens)]
        self.grid = grid
        self.dband = dband
        self.n_maxhost = n_maxhost
        self.cwts = [np.ones_like(hdens), np.zeros_like(hdens)]\
            if cwts is None else cwts
        self.sw = None
        self.Ksp = None
        self.covs = [None, None]
        # host index
        self.hidx = np.ceil(self.hdens * self.n_maxhost).astype(int)
        self.hdens = self.hdens / self.hidx
        #
        self.n_pop = len(base[0])
        assert self.n_pop == self.grid.n_pos, (self.n_pop, self.grid.n_pos)
        self.n_dt = len(base[1])
        self.inds = np.arange(self.n_pop)
        self.izs = np.unique(self.base[0])
        self.n_iz = len(self.izs)
        assert np.all(self.izs == np.arange(self.n_iz))
        self.its = np.unique(self.base[1])
        self.n_it = len(self.its)
        assert np.all(self.its == np.arange(self.n_it))
        #
        self.bin_inds = []
        for i in xrange(self.n_iz):
            self.bin_inds += [np.argwhere(self.base[0] == self.izs[i])]

    def update_covs(self, eta, eta_fn):
        if eta_fn is None:
            wt = self.cwts[0] + self.cwts[1]
        elif eta_fn == 'linear':
            wt = self.cwts[0] + eta * self.cwts[1]
        elif eta_fn == 'sigmoid':
            wt = eta * (self.cwts[0] + self.cwts[1]) +\
                (1 - eta) / (1 + np.exp(-10 * (self.cwts[0] - .5)))
        self.covs = [self.hdens[:, None] * self.incovs[i] * wt[:, None]
                     for i in xrange(2)]
        if len(self.incovs) > 2:
            self.covs += self.incovs[2:]
        # for extra control options
        self.covs += [np.ones_like(self.incovs[0]) * wt[:, None]]

    def get_continuous_weight(self, sw_file, dband=None):
        if sw_file is None:
            ixy, csize = np.array(self.grid.ixy).T, self.grid.csize
            points = ixy * csize
            dband = dband if dband is not None else self.dband
            W = pysal.threshold_continuousW_from_array(points, dband, alpha=1)
            sw = W.sparse
            print self.n_pop, W.nonzero, W.pct_nonzero
        else:
            sw = pickle.load(open(sw_file, "rb"))
            print "sw loaded."
        return sw

    def get_binary_weight(self, dband=None):
        ixy, csize = np.array(self.grid.ixy).T, self.grid.csize
        points = ixy * csize
        dband = dband if dband is not None else self.dband
        return pysal.threshold_binaryW_from_array(points, dband)

    def update_kernel(self, alpha, mu, kernel, freq_depend, with_Kfull,
                      sw_file, knorm):
        if self.sw is None:
            self.sw = self.get_continuous_weight(sw_file)
        Ksp, Kfull, Kloc = self.compute_kernel(
            alpha, mu, kernel, freq_depend, with_Kfull, knorm)
        self.Ksp, self.Kfull, self.Kloc = Ksp, Kfull, Kloc

    def compute_kernel(self, alpha, mu, kernel, freq_depend, with_Kfull,
                       knorm, Qsp=None):
        '''
        Ksp[r,c]: \propto movement rate from patch r to patch c
        Kfull[r].weights: \propto movement rates from patch r to
            Kfull[r].neighbors
        '''
        if alpha is not None:
            self.Ksp0, Kloc = fkernel[kernel](self.sw, alpha, norm=knorm)
        Ksp = self.Ksp0
        if Qsp is not None:
            Ksp = Ksp.multiply(Qsp)
        if freq_depend:
            # adjust kernel directly
            if alpha is not None or Qsp is not None:
                self.R = Ksp * self.hdens  # leaving rate (order matters!)
                assert self.R.shape == (self.n_pop,)
            Kloc = mu * 1. / (self.R + mu)  # checked!
            Z = Ksp.copy().transpose()
            Z.data = np.ones_like(Z.data)
            Z = Z.multiply(1. / (self.R + mu))
            Z = Z.transpose()
            Ksp = Ksp.multiply(Z)
        else:
            Kloc = np.ones_like(self.hdens) * Kloc
        Ksp = pysal.weights.WSP(Ksp)
        Kfull = pysal.weights.WSP2W(Ksp) if with_Kfull else None
        return Ksp.sparse, Kfull, Kloc


class ModelOptions(object):
    def __init__(self, **kwargs):
        self.param_dist = kwargs.pop('param_dist', {})
        self.param0 = kwargs.pop('param0', {})
        self.param_wbins = kwargs.pop('param_wbins', {})
        self.param_wconst = kwargs.pop('param_wconst', {})
        self.beta_update = kwargs.pop('beta_update', True)
        self.alpha_update = kwargs.pop('alpha_update', True)
        self.epsilon_update = kwargs.pop('epsilon_update', True)
        self.varepsilon_update = kwargs.pop('varepsilon_update', False)
        self.dprob_update = kwargs.pop('dprob_update', False)
        self.delta_update = kwargs.pop('delta_update', False)
        self.sigma_update = kwargs.pop('sigma_update', False)
        self.eta_update = kwargs.pop('eta_update', False)
        self.drate_update = kwargs.pop('drate_update', False)
        self.xi_update = kwargs.pop('xi_update', False)
        self.node_update = kwargs.pop('node_update', True)
        self.param_wt = kwargs.pop('param_wt', 1)
        self.n_par = 8  # hard coded
        #
        self.dkernel = kwargs.pop('dkernel', 'uni')
        self.knorm = kwargs.pop('knorm', True)
        self.freq_depend = kwargs.pop('freq_depend', False)
        self.fsize_known = kwargs.pop('fsize_known', False)
        self.sw_file = kwargs.pop('sw_file', None)
        self.eta_fn = kwargs.pop('eta_fn', 'linear')
        #
        self.tune = kwargs.pop('tune', {})
        self.node_shuffle = kwargs.pop('node_shuffle', True)
        self.n_burnin = kwargs.pop('n_burnin', 2000)
        self.n_tune = kwargs.pop('n_tune', 100)
        self.update_method = kwargs.pop('update_method', 'uniformised')
        #
        if len(kwargs) > 0:
            assert False, "Unknown model options: {}".format(kwargs)


class PopModel(object):
    """
    A prior epidemic model over the whole population. Fields:
        + pop: the population under consideration
        + opts: model options
        + init: population state before t0
        + init_trajs: initalization of latent trajectories
    """
    def __init__(self, pop, opts, init, dataP=None, with_Kfull=False,
                 init_trajs=None, quarantines=[], treatments=[]):
        assert pop.n_maxhost == 1, 'To adjust kernel computation in models'
        self.pop = pop
        self.opts = opts
        self.init = init
        self.dataP = dataP
        self.with_Kfull = with_Kfull
        self.quarantines = quarantines
        self.treatments = treatments
        #
        if self.pop.n_it * self.pop.n_iz == 1:
            self.bins = None
        else:
            self.bins = [self.pop.izs, self.pop.its]
        #
        self.setup()
        self.build_params()
        self.build_nodes(init_trajs)

    def setup(self):
        assert False, "setup() method is not implemented"

    def get_rate(self, state):
        assert False, "get_rate(state) method is not implemented"

    def update_state(self, state, crate, t):
        assert False, "update_state(state, crate) is not implemented"

    def build_params(self):
        params = {}
        for par_name in self.param_list:
            print par_name
            p_dist = self.opts.param_dist.pop(par_name, None)
            p_val = self.opts.param0.get(par_name, None)
            p_wbin = self.opts.param_wbins.pop(par_name, False)
            p_bins = self.bins if p_wbin else None
            p_wc = self.opts.param_wconst.pop(par_name, 1)
            # print par_name, p_dist, p_val
            if p_dist is None:
                if 'epsilon' in par_name:
                    assert p_val is not None
                    params[par_name] = hyper.ExponParameter(
                        p_val, [p_val], p_bins, p_wc)
                elif par_name == 'beta' or par_name == 'sigma' or par_name == 'drate':
                    params[par_name] = hyper.GammaParameter(
                        p_val, hyper.GammaParameter.uniform, p_bins, p_wc)
                else:
                    p_prior = [p_val] if p_val is not None else [1]
                    params[par_name] = hyper.UniformParameter(
                        p_val, p_prior, p_bins, p_wc)
            else:
                params[par_name] = hyper.ListParameter(p_val, p_dist, p_bins)

        self.params = params
        self.update_covs()
        self.update_K()

    def build_nodes(self, init_trajs):
        self.data = self.dataP  # dirty fix
        if self.data is not None:
            data_nodes = []
            for j in xrange(self.pop.n_pop):
                data_nodes += [self.data.get_nodedata(j)[0]]
            self.data_nodes = data_nodes

    def update_K(self):
        """
            K_ij = K_ji = coupling strength between sites i & j
        """
        self.pop.update_kernel(
            self.params['alpha'].value, self.params['mu'].value,
            self.opts.dkernel, self.opts.freq_depend, self.with_Kfull,
            self.opts.sw_file, self.opts.knorm)

    def update_covs(self):
        self.pop.update_covs(self.params['eta'].value, self.opts.eta_fn)

    def update_params(self):
        alpha_old = self.params['alpha'].value
        mu_old = self.params['mu'].value
        eta_old = self.params['eta'].value
        for par_name in self.param_list:
            self.params[par_name].update(self.params[par_name].prior)
            # print par_name, self.params[par_name].value
        # assert False
        if self.params['eta'].value != eta_old:
            self.update_covs()
        if self.params['alpha'].value != alpha_old or\
           self.params['mu'].value != mu_old:
            self.update_K()

    def sample(self, Ctvec=[], Csvec=[], init_update=True, param_update=True,
               with_econtrib=True):
        """
        Input: tvec, svec = list of forced primary cryptics
        Output:
        S.shape = [len(T), n_compartments, n_pop] -- CANCELLED
        S[0, 0] = first event's spatial location
        S[0, 1] = first event's type index
        """
        #
        if init_update:
            self.init.update(self.init.prior)
        if param_update:
            self.update_params()
        t, s = self.pop.tvec[0], copy.copy(self.init.value.T)
        T, S = [t], []
        n_year = int((self.pop.tvec[-1] - t) / 365) + 1
        econtrib = np.zeros((n_year, 3))
        #
        tau_EC = 1. / np.asarray(self.params['gamma'].value).mean()
        Etvec, Esvec = [], []
        for i in xrange(len(Ctvec)):
            Et = Ctvec[i] - tau_EC
            Es = Csvec[i]
            if Et > t:
                Etvec += [Et]
                Esvec += [Es]
            else:
                s[0, Es] = 1
        #
        iters = [zip(self.pop.tvec[1:], zip(np.zeros(self.pop.n_dt, dtype=int),
                                            np.arange(self.pop.n_dt))),
                 zip(Etvec, zip(np.ones_like(Esvec, dtype=int), Esvec)),
                 zip(Ctvec, zip(np.ones_like(Csvec, dtype=int) * 2, Csvec))]
        inp = np.zeros(3, dtype=int)
        param_update = True
        econtrib_update = False
        for (t_next, (i, j)) in heapq.merge(*iters):
            inp[i] = j
            param_update = ((i == 0) or param_update)
            while True:
                crate = self.get_rate(s, inp[0], param_update)
                if isinstance(crate, tuple):
                    erate = crate[1]
                    crate = crate[0]
                    econtrib_update = True
                param_update = False
                if crate[-1] == 0:
                    break
                tau = nr.exponential(scale=1/crate[-1])
                if t + tau < t_next:
                    t += tau
                    s, event = self.update_state(s, crate, t)
                    T += [t]
                    S += [event]
                    if econtrib_update:
                        econtrib[int((t - self.pop.tvec[0]) / 365), :] +=\
                            erate * tau
                else:
                    if econtrib_update:
                        econtrib[int((t - self.pop.tvec[0]) / 365), :] +=\
                            erate * (t_next - t)
                    t = t_next
                    if i == 1 and s[:, j].sum() == 0:
                        # set E
                        s[0, j] = 1
                        T += [t]
                        S += [[j, 0]]
                    elif i == 2 and s[1:, j].sum() == 0:
                        # set C
                        assert s[0, j] == 1
                        s[0, j] = 0
                        s[1, j] = 1
                        T += [t]
                        S += [[j, 1]]
                    break
        T += [t_next]
        T, S = np.array(T), np.array(S)
        s0 = copy.copy(self.init.value)
        # econtrib = econtrib / np.tile(econtrib.sum(1), (3, 1)).T
        return T, S, s0, econtrib

    def sample_data(self, Imat, tt, snap_times):
        n_snaps = len(snap_times) + 1
        Dmat = np.zeros_like(Imat)
        dcounts = [np.zeros(Imat.shape[1]) for i in xrange(n_snaps)]
        pos_inds = np.argwhere(Imat[-1, :] > 0)[:, 0]
        for gi in pos_inds:
            D, dcount_list = self.data_nodes[gi].sample(
                Imat[:, gi], tt, self.params['dprob'].value[0, :],
                snap_times=snap_times)
            Dmat[:, gi] = D
            for k in xrange(n_snaps):
                dcounts[k][gi] = dcount_list[k]
        return Dmat, dcounts


class PopPostModel(PopModel):
    """
    Posterior population model for a population. Fields:
        + data: observed data
        + init_trajs: instanti
    """
    def __init__(self, pop, opts, init, data, init_trajs):
        assert pop.n_maxhost == 1, 'To adjust kernel computation in models'
        self.data, self.dataV = data
        super(PopPostModel, self).__init__(pop, opts, init, with_Kfull=True,
                                           init_trajs=init_trajs)
        print type(self.data)
        #
        self.inds = np.arange(self.pop.n_pop)
        self.inds_pos = self.data.pos_inds
        self.inds_update = self.inds_pos\
            if self.opts.fsize_known else self.inds
        self.n_inds = len(self.inds_update) * self.opts.node_update
        self.n_vars = int(self.n_inds + self.opts.param_wt * self.n_inds + 1)
        print "variables", self.n_inds, self.n_vars
        #
        # assert self.params['epsilon'].ncells == 1
        assert self.params['alpha'].ncells == 1
        #
        if self.opts.update_method == 'exponentiate':
            assert self.pop.n_pop == 1 and len(self.compartments) == 1
            assert self.opts.dkernel == 'uni'
        elif self.opts.update_method == 'MH':
            assert self.pop.n_maxhost == 1

    def build_nodes(self, init_trajs):
        assert False, "build_nodes(init_trajs) is not implemented yet"

    def joint_logl(self):
        assert False, "joint_logl() is not implemented yet"

    def set_state(self, pfile):
        params, trajs = pickle.load(open(pfile, "rb"))
        for c in self.compartments:
            for i in xrange(self.pop.n_pop):
                self.nodes[c][i] = trajs[c][i]
        for pname in self.model.param_list:
            self.params[pname].value = params[pname]

    def pop_traj(self, tt):
        """
        S.shape = [len(T), n_compartments, n_pop]
        """
        return util.traj_merge(self.nodes, self.compartments, tt)

    def sample_params(self, logl, acc_rate):
        """
        Estimate alpha, beta, epsilon, & dprob only.
        Keep all other parameters fixed.
        """
        if logl is None:
            logl = self.joint_logl()
        if self.opts.sigma_update:
            dlogl = self.sigma_gibbs()
            logl += dlogl
            # logl_true = self.joint_logl()
            # assert np.allclose(logl, logl_true), (logl, logl_true)
        elif 'sigma' in self.params and type(
                self.params['sigma']) == hyper.ListParameter:
            self.params['sigma'].update(self.params['sigma'].prior)
        if self.opts.beta_update:
            dlogl = self.beta_gibbs()
            logl += dlogl
            # logl_true = self.joint_logl()
            # assert np.allclose(logl, logl_true), (logl, logl_true)
        elif 'beta' in self.params and type(
                self.params['beta']) == hyper.ListParameter:
            self.params['beta'].update(self.params['beta'].prior)
        logp = None
        acc = np.zeros(self.opts.n_par)
        if self.opts.varepsilon_update:
            vareps_acc, logl, logp = self.varepsilon_mh(logl, acc_rate[6])
            acc[6] += vareps_acc
        elif 'varepsilon' in self.params and type(
                self.params['varepsilon']) == hyper.ListParameter:
            self.params['varepsilon'].update(self.params['varepsilon'].prior)
        if self.opts.epsilon_update:
            eps_acc, logl, logp = self.epsilon_mh(logl, acc_rate[0])
            acc[0] += eps_acc
        elif 'epsilon' in self.params and type(
                self.params['epsilon']) == hyper.ListParameter:
            self.params['epsilon'].update(self.params['epsilon'].prior)
        if self.opts.alpha_update:
            alpha_acc, logl, logp = self.alpha_mh(logl, acc_rate[1])
            acc[1] += alpha_acc
            if self.opts.freq_depend:
                mu_acc, logl, logp = self.mu_mh(logl, acc_rate[2])
                acc[2] += mu_acc
        elif 'alpha' in self.params and type(
                self.params['alpha']) == hyper.ListParameter:
            self.params['alpha'].update(self.params['alpha'].prior)
            if self.opts.freq_depend:
                self.params['mu'].update(self.params['mu'].prior)
        if self.opts.dprob_update:
            dp_acc, logl, logp = self.dprob_mh(logl, acc_rate[3])
            acc[3] += dp_acc
        elif 'dprob' in self.params and type(
                self.params['dprob']) == hyper.ListParameter:
            self.params['dprob'].update(self.params['dprob'].prior)
        if self.opts.delta_update:
            vr_acc, logl, logp = self.delta_mh(logl, acc_rate[4])
            acc[4] += vr_acc
        elif 'delta' in self.params and type(
                self.params['delta']) == hyper.ListParameter:
            self.params['delta'].update(self.params['delta'].prior)
        if self.opts.eta_update:
            kp_acc, logl, logp = self.eta_mh(logl, acc_rate[5])
            acc[5] += kp_acc
        elif 'eta' in self.params and type(
                self.params['eta']) == hyper.ListParameter:
            self.params['eta'].update(self.params['eta'].prior)
        if self.opts.xi_update:
            kp_acc, logl, logp = self.xi_mh(logl, acc_rate[7])
            acc[7] += kp_acc
        elif 'xi' in self.params and type(
                self.params['xi']) == hyper.ListParameter:
            self.params['xi'].update(self.params['xi'].prior)
        #
        if self.opts.drate_update:
            self.drate_gibbs()
        elif 'drate' in self.params and type(
                self.params['drate']) == hyper.ListParameter:
            self.params['drate'].update(self.params['drate'].prior)
        return acc, logl, logp

    def update_beta_with_expon(self, logl, acc_rate):
        """
        Estimate beta for temporal model with marginal likelihood computed by
        matrix exponentiation.
        """
        self.opts.tune['beta'] = util.tune(self.opts.tune['beta'], acc_rate)
        qa = self.opts.tune['beta'] * nr.randn()
        beta_q = self.params['beta'].value * np.exp(qa)
        logl_q = self.marginal_logl(beta=beta_q)
        logp_q = logl_q + self.params['beta'].prior_logpdf(beta_q)
        logp = logl + self.params['beta'].prior_logpdf()
        if logp_q - logp + qa > np.log(nr.rand()):
            self.params['beta'].set_val(beta_q)
            return True, logl_q, logp_q
        else:
            return False, logl, logp

    def alpha_mh(self, logl, acc_rate):
        self.opts.tune['alpha'] = util.tune(self.opts.tune['alpha'], acc_rate)
        qa = self.opts.tune['alpha'] * nr.randn()
        alpha_q = self.params['alpha'].value * np.exp(qa)
        logl_q, Ksuit_q = self.joint_logl(alpha=alpha_q)
        logp_q = logl_q + self.params['alpha'].prior_logpdf(alpha_q)
        logp = logl + self.params['alpha'].prior_logpdf()
        if logp_q - logp + qa > np.log(nr.rand()):
            self.params['alpha'].set_val(alpha_q)
            self.pop.Ksp, self.pop.Kfull, self.pop.Kloc = Ksuit_q
            return True, logl_q, logp_q
        else:
            return False, logl, logp

    def mu_mh(self, logl, acc_rate):
        self.opts.tune['mu'] = util.tune(self.opts.tune['mu'], acc_rate)
        qa = self.opts.tune['mu'] * nr.randn()
        mu_q = self.params['mu'].value * np.exp(qa)
        logl_q, Ksuit_q = self.joint_logl(mu=mu_q)
        logp_q = logl_q + self.params['mu'].prior_logpdf(mu_q)
        logp = logl + self.params['mu'].prior_logpdf()
        if logp_q - logp + qa > np.log(nr.rand()):
            self.params['mu'].set_val(mu_q)
            self.pop.Ksp, self.pop.Kfull, self.pop.Kloc = Ksuit_q
            return True, logl_q, logp_q
        else:
            return False, logl, logp

    def epsilon_mh(self, logl, acc_rate):
        assert self.params['epsilon'].ncells == 1
        self.opts.tune['epsilon'] = util.tune(
            self.opts.tune['epsilon'], acc_rate)
        qa = self.opts.tune['epsilon'] * nr.randn()
        eps_q = self.params['epsilon'].value * np.exp(qa)
        logl_q = self.joint_logl(eps=eps_q)
        logp_q = logl_q + self.params['epsilon'].prior_logpdf(eps_q)
        logp = logl + self.params['epsilon'].prior_logpdf()
        if logp_q - logp + qa > np.log(nr.rand()):
            self.params['epsilon'].set_val(eps_q)
            return True, logl_q, logp_q
        else:
            return False, logl, logp

    def varepsilon_mh(self, logl, acc_rate):
        assert self.params['varepsilon'].ncells == 1
        self.opts.tune['varepsilon'] = util.tune(
            self.opts.tune['varepsilon'], acc_rate)
        qa = self.opts.tune['varepsilon'] * nr.randn()
        eps_q = self.params['varepsilon'].value * np.exp(qa)
        logl_q = self.joint_logl(vareps=eps_q)
        logp_q = logl_q + self.params['varepsilon'].prior_logpdf(eps_q)
        logp = logl + self.params['varepsilon'].prior_logpdf()
        if logp_q - logp + qa > np.log(nr.rand()):
            self.params['varepsilon'].set_val(eps_q)
            return True, logl_q, logp_q
        else:
            return False, logl, logp

    def dprob_mh(self, logl, acc_rate):
        self.opts.tune['dprob'] = util.tune(
            self.opts.tune['dprob'], acc_rate)
        dp_q = self.params['dprob'].value.flatten()
        ju = nr.choice(len(dp_q))
        qa = self.opts.tune['dprob'] * nr.randn()
        dp_q[ju] = dp_q[ju] * np.exp(qa)
        if dp_q[ju] > 1.:
            return False, logl, logl
        dp_q.shape = self.params['dprob'].value.shape
        dlogl = self.data_dlogl(dprob_q=dp_q)
        if dlogl + qa > np.log(nr.rand()):
            self.params['dprob'].set_val(dp_q)
            logl_q = logl + dlogl
            return True, logl_q, logl_q
        else:
            return False, logl, logl

    def delta_mh(self, logl, acc_rate):
        self.opts.tune['delta'] = util.tune(
            self.opts.tune['delta'], acc_rate)
        qa = self.opts.tune['delta'] * nr.randn()
        vr_q = self.params['delta'].value * np.exp(qa)
        logl_q = self.joint_logl(delta=vr_q)
        if logl_q - logl + qa > np.log(nr.rand()):
            self.params['delta'].set_val(vr_q)
            return True, logl_q, logl_q
        else:
            return False, logl, logl

    def eta_mh(self, logl, acc_rate):
        self.opts.tune['eta'] = util.tune(
            self.opts.tune['eta'], acc_rate)
        kp_q = self.params['eta'].value
        qa = self.opts.tune['eta'] * nr.randn()
        kp_q *= np.exp(qa)
        if kp_q > 1:
            return False, logl, logl
        self.pop.update_covs(kp_q, self.opts.eta_fn)
        logl_q = self.joint_logl()
        if logl_q - logl + qa > np.log(nr.rand()):
            self.params['eta'].set_val(kp_q)
            return True, logl_q, logl_q
        else:
            self.pop.update_covs(self.params['eta'].value, self.opts.eta_fn)
            return False, logl, logl

    def init_node_update(self, niter=1):
        pass

    def sample(self):
        logl = self.joint_logl()
        print "Method:", self.opts.update_method
        print "Initial log-likelihood: {}".format(logl)
        assert np.isfinite(logl)
        if self.opts.update_method == 'uniformised':
            accepted, total, niter = np.zeros(self.opts.n_par), 0., 0
            while True:
                vc = nr.choice(self.n_vars, self.n_vars)
                for i in xrange(len(vc)):
                    if vc[i] < self.n_inds:
                        j = self.inds_update[vc[i]]
                        # print "Node", j
                        try:
                            for c in self.compartments:
                                node = self.nodes[c][j]
                                # print c
                                # print "Before",
                                # print zip(node.value[0, :], node.value[1, :])
                                node.update(self.params)
                                logl = None
                                # print "After",
                                # print zip(node.value[0, :], node.value[1, :])
                        except exception.TraceUpdateError:
                            print "Node {} skipped updating!".format(j)
                    else:
                        if niter < self.opts.n_burnin and\
                           niter % self.opts.n_tune == 0:
                            acc, logl, logp = self.sample_params(
                                logl, accepted/total)
                            print niter,
                            if self.opts.epsilon_update:
                                print 'eps', self.opts.tune['epsilon'],
                            if self.opts.alpha_update:
                                print 'alpha', self.opts.tune['alpha'],
                            if self.opts.dprob_update:
                                print 'dprob', self.opts.tune['dprob'],
                            if self.opts.delta_update:
                                print 'delta', self.opts.tune['delta'],
                            if self.opts.eta_update:
                                print 'eta', self.opts.tune['eta'],
                            if self.opts.varepsilon_update:
                                print 'vareps', self.opts.tune['varepsilon'],
                            print '.'
                            accepted, total = np.zeros(self.opts.n_par), 0.
                        else:
                            acc, logl, logp = self.sample_params(
                                logl, [None for i in xrange(self.opts.n_par)])
                        accepted += acc
                        total += 1
                        niter += 1
                        yield logl, logp, accepted/total
        elif self.opts.update_method == 'exponentiate':
            beta_accepted = 0.
            while True:
                if niter < self.opts.n_burnin and\
                   niter % self.opts.n_tune == 0:
                    self.update_beta_with_expon(logl, beta_accepted/total)
                    print niter, self.opts.tune['beta']
                    beta_accepted, total = 0., 0.
                else:
                    acc, logl, logp = self.update_beta_with_expon(logl, None)
        elif self.opts.update_method == 'MH':
            accepted, total, niter = np.zeros(2), 0., 0
            while True:
                # adaptive burn-in
                if niter < self.opts.n_burnin and\
                   niter % self.opts.n_tune == 0:
                    acc, logl, logp = self.update_params(logl, accepted/total)
                    print niter, self.opts.tune['epsilon'],
                    print self.opts.tune['alpha']
                    accepted, total = np.zeros(2), 0.
                else:
                    acc, logl, logp = self.update_params(logl, [None, None])
                accepted += acc
                total += 1
                niter += 1
                #
                for j in self.inds_update:
                    for c in self.compartments:
                        self.nodes[self.compartments[c]][j].mh_update(
                            self.params)

    def update_trajs(self, crate=0.1):
        idx = nr.choice(self.n_inds, int(crate * self.n_inds))
        for i in idx:
            j = self.inds_update[i]
            try:
                for c in self.compartments:
                    node = self.nodes[c][j]
                    node.update(self.params)
            except exception.TraceUpdateError:
                print "Node {} skipped updating!".format(j)


class NodePostModel(object):
    """
    Posterior model for a spatial node in a population. Fields:
        + pop: the population under consideration
        + ind: spatial index of the node
        + smax: the node's maximal state
        + iz: index of the spatial bin
    """
    def __init__(self, population, ind, smax=None, data=None, dataV=None):
        self.pop = population
        self.ind = ind
        self.n_host = self.pop.hidx[self.ind]
        self.smax = smax if smax is not None else self.n_host
        self.data = data
        self.dataV = dataV
        #
        self.iz = self.pop.base[0][self.ind]
        #
        self.space, self.rupdate = self.make_state_space()
        self.n_state = len(self.space)
        self.n_react = len(self.rupdate)

    def make_state_space(self):
        """
        State space values = {0, 1, ..., smax}
        """
        states = np.tile(np.arange(self.smax + 1), (2, 1)).T
        rupdate = [1]
        return states, rupdate

    def set_blanket(self, nodes):
        """
        Reference to all other nodes
        """
        self.nodes = nodes
        self.init_setup()

    def init_setup(self):
        pass

    def get_genrate(self, params):
        """
        R[s, it] = rate at which node might move away from state s during
        interval [tvec[it], tvec[it+1])
            + s \in self.space
        """
        assert False, "get_genrate(params) is not implemented yet"

    def get_outlogp(self, params, W):
        assert False, "get_outlogp(params, W) is not implemented yet"

    def encode_state(self, state):
        " Return the index of the given state "
        ind = self.space[(self.space[:, 1:] == state).prod(1, dtype=bool), 0]
        if len(ind) == 0:
            return -1
        elif len(ind) == 1:
            return int(ind[0])
        else:
            raise exception.StateSpaceError(
                'Duplicated state of {}'.format(state))

    def decode_state(self, sc):
        " Return the state value at index sc "
        return self.space[sc, 1:]


class PopAgentModel(PopModel):
    """
    """
    def __init__(self, pop, opts, init):
        self.pop = pop
        self.opts = opts
        self.init = init

    def sample(self):
        pass
